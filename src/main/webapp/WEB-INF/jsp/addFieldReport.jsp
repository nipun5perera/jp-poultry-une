<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>

<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
 
<title>Add Field Report - JP Poultry</title>
 <script type="text/javascript">
$(function () {
	
	 var options = {
			 global: {
					useGTM: true
				},
		        chart: {
		            renderTo: 'chart',
		            type: 'line'
		        },
		        title: {
		            text: 'FCR History',
		            x: -20 //center
		        },
		        yAxis: {
		            title: {
		                text: 'FCR'
		            },
		            plotLines: [{
		                value: 0,
		                width: 1,
		                color: '#808080'
		            }]
		        },
		        xAxis: {
		        	 
		                type: 'date',
		                labels: {

		                    },
		                 
		            title: {
		                text: ' '
		            },
		            plotLines: [{
		                value: 0,
		                width: 1,
		                color: '#808080'
		            }]
		        },
		        series: [{tickInterval: 24 * 3600 * 1000}]
		    };
		    
	 		var myParam = location.search.split('bid=')[1]
		    var url =  "./getFieldReport.json?bid="+myParam+"";
		    $.getJSON(url,  function(data) {
		        options.series[0].data = data;
		        var chart = new Highcharts.Chart(options);
		    });
	 
	
	 
});
        </script>
</head>
<body>
 <script src="resources/js/highcharts.js"></script>
 
<script src="resources/js/exporting.js"></script>

<div class="container-fluid">
   
    <div class="row">
        <%@include file="layouts/admin.jsp"%>
        
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            
            <h1 class="page-header">Add Field Report</h1>
            
                <form:form commandName="fieldReport" class="form-horizontal">
                        <div class="form-group">
                            <label for="inputDate" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-5">
                                
                        
                                <div class="container">
                                    <div class="row">
                                        <div class='col-sm-3'>
                                            <div class="form-group">
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(function () {$('#datetimepicker1').datetimepicker({
                                                    format: 'YYYY-MM-DD'
                                                });
                                            });
                                        </script>
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    <div class="form-group">
                        <label for="fieldOfficer" class="col-sm-2 control-label">Field Officer</label>
                        <div class="col-sm-3">
                          <form:select path="fieldOfficer.fieldOfficerId" id="fieldOfficer" class="form-control">
                            <form:options items="${fieldOfficerReport}" itemLabel="fieldOfficerFullName"
                                itemValue="fieldOfficerId" />
                        </form:select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputBatch" class="col-sm-2 control-label">Distribute Batch ID</label>
                        <div class="col-sm-3">
                          <form:input path="supplyBatch.supplyBatchId" value="${param.bid }" type="text" class="form-control" id="inputBatch" readonly="true" />
                        </div>
                    </div>          
                    
                    <div class="form-group">
                        <label for="inputAmountAte" class="col-sm-2 control-label">Amount Ate</label>
                        <div class="col-sm-3">
                          <form:input path="amountAte" type="text" class="form-control" id="inputAmountAte"  />
                        </div>
                    </div>  
                    
                    <div class="form-group">
                        <label for="inputAverageWeight" class="col-sm-2 control-label">Average Weight</label>
                        <div class="col-sm-3">
                          <form:input path="averageWeight" type="text" class="form-control" id="inputAverageWeight"  />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputFCR" class="col-sm-2 control-label">FCR</label>
                        <div class="col-sm-3">
                          <form:input path="FCR" type="text" class="form-control" id="inputFCR"  />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputNumOfChicksDied" class="col-sm-2 control-label">Number Of Chicks Died</label>
                        <div class="col-sm-3">
                          <form:input path="numOfChicksDied" type="text" class="form-control" id="inputNumOfChicksDied"  />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAge" class="col-sm-2 control-label">Age</label>
                        <div class="col-sm-3">
                          <form:input path="age" type="text" class="form-control" id="inputAge"  />
                        </div>
                    </div>
                    
                     <div class="form-group">
                        <div class="col-sm-4 control-label">
                            <input type="submit" value="Add Field Report" class="btn btn-primary">
                        </div>
                     </div>
                            
                </form:form>
                
             
               <div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div> 
        </div>
        
    </div>
     
</div>


    <%@include file="layouts/footer.jsp" %>
  
</body>
</html>