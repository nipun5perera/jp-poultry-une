<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Update Distributed Batch - JP Poultry</title>

<script type="text/javascript" src="../resources/js/jquery.min.js"></script>

</head>
<body>
<div class="container-fluid">
	<div class="row">
	
	<%@include file="layouts/admin.jsp"%>	
		
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		
			<h1 class="page-header">Update Distributed Batch</h1>
			
			<form:form commandName="supplyBatch" class="form-horizontal">
			
				<div class="form-group">
				    <label for="supplyBatchId" class="col-sm-2 control-label">Batch ID</label>
				    <div class="col-sm-3">
				      <form:input path="supplyBatchId" type="text" class="form-control" disabled="true" id="supplyBatchId"  value="${supplyBatch.supplyBatchId}"/>
				    </div>
			  	</div>
		
				<div class="form-group">
					<label for="farmerId" class="col-sm-2 control-label">Farmer</label>
					<div class="col-sm-3">
						<form:select path="farmer.farmerId" class="form-control"
							id="farmerId" disabled="true">
							<form:options items="${farmerReport}" itemLabel="name"
								itemValue="farmerId" />
						</form:select>
					</div>
				</div>
		
				<div class="form-group">
					<label for="fieldOfficerId" class="col-sm-2 control-label">Field
						Officer</label>
					<div class="col-sm-3">
						<form:select path="fieldOfficer.fieldOfficerId"
							class="form-control" id="fieldOfficer" disabled="true">
							<form:options items="${fieldOfficerReport}" itemLabel="fname"
								itemValue="fieldOfficerId" />
						</form:select>
					</div>
				</div>
				
				<div class="form-group">
				    <label for="inputDate" class="col-sm-2 control-label">Date</label>
				    <div class="col-sm-3">
				      
				      	<div class="container">
						    <div class="row">
						        <div class='col-sm-3'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker1'>
						                    <form:input path="date" type='text' class="form-control" id="inputDate" value="${supplyBatch.date}"/>
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
						            $(function () {
						                $('#datetimepicker1').datetimepicker({
						                	format: 'YYYY-MM-DD'
						                });
						            });
						        </script>
						    </div>
						</div>
						
				    </div>
			  	</div>		
			  	<div class="form-group">
				    <label for="quantity" class="col-sm-2 control-label">Quantity</label>
				    <div class="col-sm-3">
				      <form:input path="quantity" type="text" class="form-control" id="quantity" value="${supplyBatch.quantity}" />
				    </div>
			  	</div>
			  	
			  <div class="form-group">
				  <div class="col-sm-5 control-label">
				  	<input type="submit" value="Update" class="btn btn-primary">
				  </div>
			 </div>	
				
			</form:form>
		</div>
	</div>
</div>

	<%@include file="layouts/footer.jsp" %>
</body>
</html>