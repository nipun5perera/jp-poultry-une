<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp"%>
<title>Update Feed Order- JP Poultry</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>

<script language="javascript">
	function addRow(tableID) {
		var table = document.getElementById(tableID);
		
		 var showVar = '<c:out value="${supplierReport}"/>';
		    alert("The variable show is "+showVar.length);
		var att = '${supplierReport}';
		
		//alert(att.length);
		
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var counts = rowCount - 1;

		var cell1 = row.insertCell(0);
		var feed = document.createElement("select");
		
		var op = new Option();
	 	op = att;
		feed.type = "option";
		 
		
		
		feed.name = "feedOrderDetail[" + counts + "].feed.feedId";
		cell1.appendChild(feed);

		var cell2 = row.insertCell(1);
		var unitPrice = document.createElement("input");
		unitPrice.type = "text";
		unitPrice.name = "feedOrderDetail[" + counts + "].unitPrice";
		cell2.appendChild(unitPrice);

		var cell3 = row.insertCell(2);
		var quantity = document.createElement("input");
		quantity.type = "text";
		quantity.name = "feedOrderDetail[" +counts + "].quantity";
		cell3.appendChild(quantity);

	}
</script>


<script type="text/javascript"> 
function addRowToTable()
{
	var rowCount = document.getElementById('feedTable').rows.length;
//	alert(rowCount-1);
	var pid = rowCount-1;
//	alert(rowCount);
	var feedClone =  $('#feed').clone();
	var unitPriceClone = $('#unitPrice').clone();
	var quantityClone = $('#quantity').clone();
	feedClone.attr("name", "feedOrderDetail["+pid+"].feed.feedId");
	unitPriceClone.attr("name", "feedOrderDetail["+pid+"].unitPrice");
	quantityClone.attr("name", "feedOrderDetail["+pid+"].quantity");
	
	$('#feedTable').find('tbody').
	append($('<tr>')
			.append($('<td>').append(feedClone))
			.append($('<td>').append(unitPriceClone))
			.append($('<td>').append(quantityClone))
			.append($('<td>').append('<input type="button" id="del" class="delRowBtn" value="Delete"/>'))
			
	);
	
	//append("<tr><td>").append(dropClone).append("</td><td>2</td><td>3</td></tr>")
	
	rowCount = document.getElementById('feedTable').rows.length;
//	alert(rowCount-1);
	 
	 
	 $(document.body).delegate(".delRowBtn", "click", function(){
	        $(this).closest("tr").remove();        
	    });  
	
	
}

</script> 
 
 


<script type="text/javascript">

$(document).ready(function() {
    $('#paymentRequest').on('change', function() {
     // alert( this.value ); // or $(this).val()
      
      var id = this.value;
      
      console.log(id);
      
      $.ajax({
    	  url: 'http://localhost:8080/jp/getPaymentRequestObject.json?pid='+id+'',
    	  success: function(data){
    		  console.log(data);
    		  $('#paymentAmount').val(data.amount);
    		  $('#paymentDate').val(new Date(data.date));
    		  $('#description').val(data.description);
    	  
    	  }
      
    	});
      return false;
      
      
    });
});
</script>


<script type="text/javascript">

$(document).ready(function() {
    $('#supplierId').on('change', function() {
     // alert( this.value ); // or $(this).val()
      
      var sid = this.value;
      
      console.log(sid);
      
      $.ajax({
    	  url: 'http://localhost:8080/jp/getSupplierObject.json?sid='+sid+'',
    	  success: function(data){
    		  console.log(data);
    		  $('#sname').val(data.name);
    		  $('#address1').val(data.address1);
    		  $('#address2').val(data.address2);
    		  $('#city').val(data.city);
    		  $('#email').val(data.email);
    		  $('#contact').val(data.contact);
    		  
    	  }
      
    	});
      return false;
      
      
    });
});
</script>




</head>
<body>

	<div class="container-fluid">
		<div class="row">
			
			<%@include file="layouts/admin.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Approve Feed Order</h1>
			<!-- left side of the page -->
				<div class=".col-md-6">
					<form:form method="POST" commandName="feedOrder" class="form-horizontal">
					
						<div class="form-group">
							<label for="inputDate" class="col-sm-2 control-label">Payment Request Date</label>
							<div class="col-sm-5">
								
						
								<div class="container">
								    <div class="row">
								        <div class='col-sm-3'>
								            <div class="form-group">
								                <div class='input-group date' id='datetimepicker2'>
								                    <form:input path="paymentRequest.date" type='text' value="${feedOrder.paymentRequest.date}" class="form-control" id="inputDate" readonly="true" />
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>
								        </div>
								        <script type="text/javascript">
								            $(function () {
								                $('#datetimepicker2').datetimepicker({
								                	format: 'YYYY-MM-DD'
								                });
								            });
								        </script>
								    </div>
								</div>
						
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputTotal" class="col-sm-2 control-label">Payment request amount</label>
							<div class="col-sm-5">
								<form:input path="paymentRequest.amount" type="text" value="${paymentReqeust.amount}" class="form-control" id="inputTotal" readonly="true" />
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputTotal" class="col-sm-2 control-label">Payment request ID</label>
							<div class="col-sm-5">
								<form:input path="paymentRequest.paymentId" type="text" value="${paymentReqeust.paymentId}" class="form-control" id="inputTotal" readonly="true" />
							</div>
						</div>

						<div class="form-group">
							<label for="inputTotal" class="col-sm-2 control-label">Payment
								description</label>
							<div class="col-sm-5">
								<form:input path="paymentRequest.description" type="text"
									value="${paymentReqeust.description}" class="form-control"
									id="inputTotal" readonly="true"  />
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputDate" class="col-sm-2 control-label">Date</label>
							<div class="col-sm-5">
								
						
								<div class="container">
								    <div class="row">
								        <div class='col-sm-3'>
								            <div class="form-group">
								                <div class='input-group date' id='datetimepicker1'>
								                    <form:input path="date" type='text' class="form-control" id="inputDate" readonly="true" />
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>
								        </div>
								        <script type="text/javascript">
								            $(function () {
								                $('#datetimepicker1').datetimepicker({
								                	format: 'YYYY-MM-DD'
								                });
								            });
								        </script>
								    </div>
								</div>
						
							</div>
						</div>

						 
						
						<div class="form-group">
							<label for="inputTotal" class="col-sm-2 control-label">ID</label>
							<div class="col-sm-5">
								<form:input path="feedOrderId" type="text" value="${feedOrder.feedOrderId}"
									class="form-control" id="inputTotal" readonly="true" />
							</div>
						</div>
						<div class="form-group">
							<label for="inputTotal" class="col-sm-2 control-label">Total</label>
							<div class="col-sm-5">
								<form:input path="total" type="text" value="${feedOrder.total}" class="form-control" id="inputTotal" readonly="true" />
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputDiscount" class="col-sm-2 control-label">Discount
							</label>
							<div class="col-sm-5">
								<form:input path="discount" type="text" value="${feedOrder.discount}" class="form-control" id="inputDiscount" readonly="true" />
							</div>
						</div>	
						
						
						<div class="form-group">
							<label for="inputDiscount" class="col-sm-2 control-label">Fulfill Status
							</label>
							<div class="col-sm-5">
								<form:checkbox path="FulFilled"  />
								 
							</div>
						</div>	
						
						<div class="table-responsive">
							<table id="feedTable" class="table table-bordered table-hover">
								<thead>
									<tr>
										<td  style="width:200px;">Feed ID</td>
										<td>Unit Price</td>
										<td>Quantity</td>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${feedOrder.feedOrderDetail}" var="feedOrdeerDetails" varStatus="loop">
								
									<tr>
										<td>
										
										<form:select path="feedOrderDetail[${loop.index}].feed.feedId"  id="feed" class="form-control">
											<form:options items="${feedReport}" itemLabel="feedNameManufacturer" itemValue="feedId" />
										</form:select>
										</td>
										<td><form:input path="feedOrderDetail[${loop.index}].unitPrice" value="${feedOrdeerDetails.unitPrice}" id="unitPrice" class="form-control"/></td>
										<td><form:input path="feedOrderDetail[${loop.index}].quantity" value="${feedOrdeerDetails.quantity}" id="quantity" class="form-control"/></td>
										<td><form:input path="feedOrderDetail[${loop.index}].filledQuantity" value="${feedOrdeerDetails.filledQuantity}" id="quantity" class="form-control"/></td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>	
						
						 

						<input type="submit" value="Approve Feed Order" class="btn btn-primary">																				
												
					</form:form>
					
				</div>	
				
				<!--  right side of the page -->
				<%-- <div class="col-md-5">
				<form action="#" class="form-horizontal">

					<div class="form-group">
						<label for="paymentDate" class="col-sm-3 control-label">Payment Date</label>
						<div class="col-sm-5">
							<input id="paymentDate" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="paymentAmount" class="col-sm-3 control-label">Payment Amount</label>
						<div class="col-sm-5">
							<input id="paymentAmount" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="description" class="col-sm-3 control-label">Description</label>
						<div class="col-sm-5">
							<textarea id="description" class="form-control" rows="3" disabled></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label for="sname" class="col-sm-3 control-label">Supplier Name</label>
						<div class="col-sm-5">
							<input id="sname" class="form-control" disabled>
						</div>
					</div>	
					
					<div class="form-group">
						<label for="address1" class="col-sm-3 control-label">Address Line 1</label>
						<div class="col-sm-5">
							<input id="address1" class="form-control" disabled>
						</div>
					</div>						
	
					<div class="form-group">
						<label for="address2" class="col-sm-3 control-label">Address Line 2</label>
						<div class="col-sm-5">
							<input id="address2" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="city" class="col-sm-3 control-label">City</label>
						<div class="col-sm-5">
							<input id="city" class="form-control" disabled>
						</div>
					</div>	
					
					<div class="form-group">
						<label for="email" class="col-sm-3 control-label">Email</label>
						<div class="col-sm-5">
							<input id="email" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="contact" class="col-sm-3 control-label">Contact Number</label>
						<div class="col-sm-5">
							<input id="contact" class="form-control" disabled>
						</div>
					</div>				  	
		
			 	</form> 				
				</div> --%>
									
			</div>
			
			 
			
		</div>
	</div>
	<%@include file="layouts/footer.jsp"%>


</body>
</html>