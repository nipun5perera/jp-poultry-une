<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
</script>
<title>Perform Catching - JP Poultry</title>
</head>
<body>
<div class="container-fluid">
	<div class="row">
	
			<%@include file="layouts/admin.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Perform Catching</h1>
			<!-- left side of the page -->
				<div class="col-md-6">
				
					<form:form commandName="catching" class="form-horizontal">
					 
					
					  	<%-- <div class="form-group">
						    <label for="inputAverageWeight" class="col-sm-4 control-label">Date</label>
						    <div class="col-sm-6">
						      <form:input path="date" type="text" class="form-control" id="inputAverageWeight"  />
						    </div>
					  	</div>
					 --%>
					 <div class="form-group">
						    <label for="inputBatch" class="col-sm-4 control-label">Task ID</label>
						    <div class="col-sm-6">
						      <input   value="${param.tid}" type="text" class="form-control" id="task" name="task" readonly />
						    </div>
					  	</div>
						<div class="form-group">
							<label for="inputDate" class="col-sm-4 control-label">Date</label>
							<div class="col-sm-5">
								
						
								<div class="container">
								    <div class="row">
								        <div class='col-sm-3'>
								            <div class="form-group">
								                <div class='input-group date' id='datetimepicker1'>
								                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>
								        </div>
								        <script type="text/javascript">
								            $(function () {
								                $('#datetimepicker1').datetimepicker({
								                	format: 'YYYY-MM-DD'
								                });
								            });
								        </script>
								    </div>
								</div>
						
							</div>
						</div>
					
					
					
					  	<div class="form-group">
						    <label for="inputAverageWeight" class="col-sm-4 control-label">Average Weight</label>
						    <div class="col-sm-6">
						      <form:input path="averageWeight" type="text" class="form-control" id="inputAverageWeight"  />
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label for="inputBatch" class="col-sm-4 control-label">Distribute Batch ID</label>
						    <div class="col-sm-6">
						      <form:input path="supplyBatch.supplyBatchId" value="${param.bid}" type="text" class="form-control" id="inputBatch" readonly="true"  />
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label for="inputLiveWeight" class="col-sm-4 control-label">Live Weight</label>
						    <div class="col-sm-6">
						      <form:input path="liveWeight" type="text" class="form-control" id="inputLiveWeight"  />
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label for="inputNumOfChicks" class="col-sm-4 control-label">Number of Chicks</label>
						    <div class="col-sm-6">
						      <form:input path="numOfChicks" type="text" class="form-control" id="inputNumOfChicks"  />
						    </div>
					  	</div>
							<div class="form-group">
						    <label for="inputNumOfChicks" class="col-sm-4 control-label">FCR</label>
						    <div class="col-sm-6">
						      <form:input path="FCR" type="text" class="form-control" id="inputFCR"  />
						    </div>
					  	</div>

						<div class="form-group">
							<label for="inputNumOfChicksDied" class="col-sm-4 control-label">Number
								of Chicks Died</label>
							<div class="col-sm-6">
								<input value="${numOfChicksDied}" type="text"
									class="form-control" id="inputNumOfChicksDied" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6 control-label">
								<input type="submit" value="Add Catching"
									class="btn btn-primary">
							</div>
						</div>
					</form:form>					
				</div>
				
			 	
				
			</div>
	</div>
</div>

	<%@include file="layouts/footer.jsp" %>
</body>
</html>