<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp"%>
<title>Add Feed Return - JP Poultry</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
</script>
<script type="text/javascript">

$(document).ready(function() {
    
      var id = location.search.split('bid=')[1];
      
      console.log(id);
      
      $.ajax({
    	  url: './getBatchObject.json?bid='+id+'',
    	  success: function(data){
    		  console.log(data);
    	//	  alert(data.farmer.name);
    		  
    		  $('#farmer').val(data.farmer.name);
    		  $('#batch').val(data.batchId);
    		  $('#chickQuantity').val(data.quantity);
    		  
    		  $('#fieldOfficer').val(data.fieldOfficer.fname+" "+data.fieldOfficer.lname);
    		  
    		  var tr =[] ;
    		  var tr2=[];
    		  
    		  for(var i=0; i < data.batchFeed.length;i++)
    		  {
    			 tr = $('<tr/>');
    			 tr.append("<td>"+data.batchFeed[i].feed.feedId+"</td>");
    			 tr.append("<td>"+data.batchFeed[i].feed.name+"</td>");
    			 tr.append("<td>"+data.batchFeed[i].quantity+"</td>");
    			 //alert(data.batchFeed[i].feed.feedId)
    			 $('#feedTable').append(tr);
    		  }
    		  
    		  for(var i=0; i < data.batchMedicine.length;i++)
    		  {
    			 tr2 = $('<tr/>');
    			 tr2.append("<td>"+data.batchMedicine[i].medicine.medicineId+"</td>");
    			 tr2.append("<td>"+data.batchMedicine[i].medicine.name+"</td>");
    			 tr2.append("<td>"+data.batchMedicine[i].quantity+"</td>");
    			 $('#medTable').append(tr2);
    		  }
    		  
    		  
    		  
    		  /*$('#description').val(data.description);
    		  */
    	  }
      
    	});
      return false;
      
});
</script>
</head>
<body>

	<div class="container-fluid">
		<div class="row">

			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Add Feed Return</h1>
				<!-- left side of the page -->
				<div class="col-md-6">
					<form:form commandName="feedReturn" class="form-horizontal">

						<div class="form-group">
						<label for="inputFeed" class="col-sm-2 control-label">Feed</label>
						<div class="col-sm-5">
							<%--  <form:input path="feed.feedId" type="text" class="form-control" id="inputFeed"/>--%>

							<form:select path="feed.feedId" id="inputFeed"
								class="form-control">
								<form:options items="${batchFeedReport}" itemLabel="feed.feedNameManufacturer"
									itemValue="feed.feedId" />
							</form:select>
						</div>
					</div>	

						<div class="form-group">
							<label for="inputBatchId" class="col-sm-2 control-label">Distribute Batch
								ID</label>
							<div class="col-sm-5">
								<form:input path="supplyBatch.supplyBatchId" type="text"
									class="form-control" id="inputBatchId" value="${param.bid}" readonly="true"/>
							</div>
						</div>
						<div class="form-group">
							<label for="inputQuantity" class="col-sm-2 control-label">Quantity</label>
							<div class="col-sm-5">
								<form:input path="quantity" type="text" class="form-control"
									id="inputQuantity" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-7 control-label">
								<input type="submit" value="Add Feed Return" class="btn btn-primary">
							</div>
						</div>

					</form:form>
				</div>

				<!--  right side of the page -->
				<%-- <div class="col-md-6">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="inputTotal" class="col-sm-3 control-label">Farmer</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" id="farmer" />
							</div>
						</div>

						<div class="form-group">
							<label for="inputTotal" class="col-sm-3 control-label">Number of Chicks</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" id="chickQuantity" />
							</div>
						</div>

						<div class="form-group">
							<label for="inputTotal" class="col-sm-3 control-label">Field
								Officer</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" id="fieldOfficer" />
							</div>
						</div>
					</form>
				</div> --%>

			</div>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Issued Feeds</h1>
				<%@include file="./getBatchFeedReport.jsp"%>
			</div>
		</div>
	</div>

	<%@include file="layouts/footer.jsp"%>
</body>
</html>