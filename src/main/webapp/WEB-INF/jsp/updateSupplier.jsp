<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Update Supplier - JP Poultry</title>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@include file="layouts/admin.jsp"%>	
		
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		
			<h1 class="page-header">Update Supplier</h1>
		
			<form:form commandName="supplier" class="form-horizontal">
			
			<div class="form-group">
				    <label for="inputName" class="col-sm-1 control-label">ID</label>
				    <div class="col-sm-3">
				      <form:input path="supplierId" type="text" class="form-control" value="${param.sid}" id="inputName" placeholder="Enter Supplier Name" />
				    </div>
			  	</div>
			
				<div class="form-group">
				    <label for="inputName" class="col-sm-1 control-label">Name</label>
				    <div class="col-sm-3">
				      <form:input path="name"   type="text" class="form-control" id="inputName" placeholder="Enter Supplier Name" />
				    </div>
			  	</div>
			  	
				<div class="form-group">
				    <label for="inputAddress1" class="col-sm-1 control-label">Address</label>
				    <div class="col-sm-3">
				      <form:input path="address1" type="text" class="form-control" id="inputAddress1" placeholder="Enter Address" />
				    </div>
			  	</div>		
			  	
				<div class="form-group">
				    <label for="inputAddress2" class="col-sm-1 control-label"></label>
				    <div class="col-sm-3">
				      <form:input path="address2" type="text" class="form-control" id="inputAddress2" placeholder="Enter Address" />
				    </div>
			  	</div>	
			  	
				<div class="form-group">
				    <label for="inputCity" class="col-sm-1 control-label">City</label>
				    <div class="col-sm-3">
				      <form:input path="city" type="text" class="form-control" id="inputCity" placeholder="Enter City" />
				    </div>
			  	</div>
			  	
				<div class="form-group">
				    <label for="inputContact" class="col-sm-1 control-label">Contact</label>
				    <div class="col-sm-3">
				      <form:input path="contact" type="text" class="form-control" id="inputContact" placeholder="Enter Contact Number" />
				    </div>
			  	</div>	
			  	
				<div class="form-group">
				    <label for="inputEmail" class="col-sm-1 control-label">Email</label>
				    <div class="col-sm-3">
				      <form:input path="email" type="text" class="form-control" id="inputEmail" placeholder="Enter Email Address" />
				    </div>
			  	</div>				  			  				  		  	
			  	
			  <div class="form-group">
				  <div class="col-sm-4 control-label">
				  	<input type="submit" value="Update Supplier" class="btn btn-primary">
				  </div>
			 </div>
			 
			</form:form>
			
			 
			
		</div>	
	</div>
</div>
	

	<%@include file="layouts/footer.jsp" %>
</body>
</html>