<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Payment Request ID</th>
							<th>Date</th>
							<th>Description</th>
							<th>Amount</th>
							<th>Status</th>
							<th>Delete</th>
							<th>Update</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${paymentRequestReport}" var="paymentRequest">
							<tr>

								<td>${paymentRequest.paymentId}</td>
								<td>${paymentRequest.date}</td>
								<td>${paymentRequest.description}</td>
								<td>${paymentRequest.amount}</td>
								<td>${paymentRequest.status}</td>
								<td><a
									href="http://localhost:8080/jp/admin/deletePaymentRequest.html?pid=${paymentRequest.paymentId}"
									class="btn btn-danger">Delete</a></td>
								<td><a
									href="http://localhost:8080/jp/admin/updatePaymentRequest.html?pid=${paymentRequest.paymentId}"
									class="btn btn-warning">Update</a></td>
								<c:choose>
									<c:when test="${paymentRequest.status == 'Pending'}">
									<security:authorize ifAnyGranted="ROLE_ADMIN">
										<td><a
											href="http://localhost:8080/jp/admin/approvePaymentRequest.html?pid=${paymentRequest.paymentId}"
											class="btn btn-warning">Approve</a></td>
											</security:authorize>
									</c:when>

								</c:choose>


							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<%@include file="layouts/footer.jsp"%>

</body>
</html>