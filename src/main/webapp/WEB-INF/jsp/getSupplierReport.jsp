<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Get Supplier Report - JP Poultry</title>
</head>

<body>
<div class="table-responsive">
		<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Supplier ID</th>
				<th>Name</th>
				<th>Address Line 1</th>
				<th>Address Line 2</th>
				<th>City</th>
				<th>Email</th>
				<th>Contact Number</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${supplierReport}" var="supplier">
				<tr>
					<td>${supplier.supplierId}</td>
					<td>${supplier.name}</td>
					<td>${supplier.address1}</td>
					<td>${supplier.address2}</td>
					<td>${supplier.city}</td>
					<td>${supplier.email}</td>
					<td>${supplier.contact}</td>
					<td><a href="http://localhost:8080/jp/admin/updateSupplier.html?sid=${supplier.supplierId}" class="btn btn-warning">Update</a> </td>
					<td><a href="http://localhost:8080/jp/admin/deleteSupplier.html?sid=${supplier.supplierId}" class="btn btn-danger">Delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<%@include file="layouts/footer.jsp" %>
</body>
</html>