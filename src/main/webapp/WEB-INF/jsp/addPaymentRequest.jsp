<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Add Supplier - JP Poultry</title>

<script type="text/javascript" src="jquery-2.1.4.min.js">
</script>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@include file="layouts/admin.jsp" %>
		
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Add Payment Request</h1>
			
			<form:form commandName="paymentRequest" class="form-horizontal">
				
				<div class="form-group">
				    <label for="inputDate" class="col-sm-1 control-label">Date</label>
				    <div class="col-sm-3">
				      
				      	<div class="container">
						    <div class="row">
						        <div class='col-sm-4'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker1'>
						                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
						            $(function () {
						                $('#datetimepicker1').datetimepicker({
						                	format: 'YYYY-MM-DD'
						                });
						            });
						        </script>
						    </div>
						</div>
						
				    </div>
			  	</div>
			  	
				<div class="form-group">
				    <label for="inputDescription" class="col-sm-1 control-label">Description</label>
				    <div class="col-sm-3">
				      <form:textarea path="description" type="text" class="form-control" id="inputDescription" placeholder="Enter Short Description" rows="3"/>
				    </div>
			  	</div>		
			  	
				<div class="form-group">
				    <label for="inputAmount" class="col-sm-1 control-label">Amount</label>
				    <div class="col-sm-3">
				      <form:input path="amount" type="text" class="form-control" id="inputAmount" placeholder="Enter Amount" />
				    </div>
			  	</div>
			  	
			  <div class="form-group">
				  <div class="col-sm-4 control-label">
				  	<input type="submit" value="Add Payment Request" class="btn btn-primary">
				  </div>
			 </div>			  					  		  	
			  					
			</form:form>
		</div>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Payment Requests</h1>


				<%@include file="getPaymentRequestReport.jsp"%>
			</div>


		</div>
</div>

	<%@include file="layouts/footer.jsp" %>

</body>
</html>