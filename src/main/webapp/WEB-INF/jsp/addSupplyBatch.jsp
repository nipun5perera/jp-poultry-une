<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Issue Chicks - JP Poultry</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>


<script type="text/javascript">

$(document).ready(function() {
    $('#batchId').on('change', function() {
     // alert( this.value ); // or $(this).val()
      
      var id = this.value;
      
      console.log(id);
      
      $.ajax({
    	  url: './getBatchObject.json?bid='+id+'',
    	  success: function(data){
    		  console.log(data);
    		  $('#quantity').val(data.quantity);
    		  $('#paymentDate').val(new Date(data.date));
    		  $('#supplier').val(data.supplier.name);
    		  $('#issuedAmount').val(data.issuedAmount);
    		  $('#stock').val(data.quantity-data.issuedAmount);
    	  
    	  }
      
    	});
      return false;
      
      
    });
});
</script>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>
			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Distribute Chicks</h1>	
			
				<!-- left side of the page -->
				<div class="col-md-6">
					<form:form commandName="supplyBatch" class="form-horizontal">
					<form:errors path="*" Class="alert alert-danger" element="div" />
					
						<div class="form-group">
							<label for="inputDate" class="col-sm-3 control-label">Date</label>
							<div class="col-sm-5">
								
						
								<div class="container">
								    <div class="row">
								        <div class='col-sm-3'>
								            <div class="form-group">
								                <div class='input-group date' id='datetimepicker1'>
								                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>
								        </div>
								        <script type="text/javascript">
								            $(function () {
								                $('#datetimepicker1').datetimepicker({
								                	format: 'YYYY-MM-DD'
								                });
								            });
								        </script>
								    </div>
								</div>
						
							</div>
						</div>
						<div class="form-group">
							<label for="paymentAmount" class="col-sm-3 control-label">Bill Number</label>
							<div class="col-sm-5">
								<form:input path="billNumber" id="inputBillNumber" class="form-control"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="paymentAmount" class="col-sm-3 control-label">Chick Price</label>
							<div class="col-sm-5">
								<form:input path="chickPrice" id="inputChickPrice" class="form-control"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="farmerId" class="col-sm-3 control-label">Chick Purchase Batch</label>
							<div class="col-sm-5">
								<form:select path="batch.batchId" class="form-control" id="batchId">
								    <form:option value="" label="Select Bird purchase" />
									<form:options items="${batchReport}" itemLabel="purchaseBatchID" itemValue="batchId" />
								</form:select>
							</div>
						</div>
				
				
						<div class="form-group">
							<label for="farmerId" class="col-sm-3 control-label">Farmer</label>
							<div class="col-sm-5">
								<form:select path="farmer.farmerId" class="form-control" id="farmerId">
									<form:options items="${farmerReport}" itemLabel="name" itemValue="farmerId" />
								</form:select>
							</div>
						</div>
									
						<div class="form-group">
							<label for="fieldOfficerId" class="col-sm-3 control-label">Field Officer</label>
							<div class="col-sm-5">
								<form:select path="fieldOfficer.fieldOfficerId" class="form-control" id="fieldOfficer">
									<form:options items="${fieldOfficerReport}" itemLabel="fieldOfficerFullName" itemValue="fieldOfficerId"/>
								</form:select>
							</div>
						</div>

						 

						<div class="form-group">
							<label for="inputQuantity" class="col-sm-3 control-label">Quantity</label>
							<div class="col-sm-5">
								<form:input path="quantity" type="text" class="form-control" id="inputQuantity"/>
							</div>
						</div>

						     
						
						
						
						
						
				  <div class="col-sm-8 control-label">
				  	<input type="submit" value="Distribute Batch" class="btn btn-primary">
				  </div>						
																																						
					</form:form>
				</div>
				
				<!-- right side of the page -->
				  <div class="col-md-6">
				<form class="form-horizontal">
				
						<div class="form-group">
							<label for="paymentDate" class="col-sm-3 control-label">Batch Purchase Date</label>
							<div class="col-sm-5">
								<input id="paymentDate" class="form-control"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="paymentAmount" class="col-sm-3 control-label">Quantity</label>
							<div class="col-sm-5">
								<input id="quantity" class="form-control"/>
							</div>
						</div>

						<div class="form-group">
							<label for="description" class="col-sm-3 control-label">Supplier</label>
							<div class="col-sm-5">
								<input id="supplier" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="col-sm-3 control-label">Issued Chicks</label>
							<div class="col-sm-5">
								<input id="issuedAmount" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="col-sm-3 control-label">Stock</label>
							<div class="col-sm-5">
								<input id="stock" class="form-control"/>
							</div>
						</div>
				</form>
				</div>	 	
			
			</div>		
		</div>
	</div>
	
	<%@include file="layouts/footer.jsp" %>
</body>
</html>