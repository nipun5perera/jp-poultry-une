<html>
<div class="col-sm-2 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
	 
		<li>
			<a href="addSupplier.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Supplier</a>
		</li>
	 
		<li>
			<a href="addFarmer.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Farmer</a>
		</li>
		<li>
			<a href="addFeed.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Feed</a>
		</li>
		<li>
			<a href="addFieldOfficer.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Field Officer</a>
		</li>
		<li>
			<a href="addMedicine.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Medicine</a>
		</li>
		<li>
			<a href="getPaymentRequestReport.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Get Payment Requests</a>
		</li>
		<li>
			<a href="addMedicineOrder.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Medicine Order</a>
		</li>
		<li>
			<a href="addFeedOrder.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Feed Order</a>
		</li>
		<li>
			<a href="addDamagedFeed.html.html">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
			Add Damaged Feed</a>
		</li>
		<li>
			<a href="addBatch.html">
			<span class="glyphicon glyphicon-usd" aria-hidden="true"></span> 
			Purchase Chicks</a>
		</li>
		<li>
			<a href="addSupplyBatch.html">
			<span class="glyphicon glyphicon-usd" aria-hidden="true"></span> 
			Distribute Chicks</a>
		</li>
		<li>
			<a href="getBatchReport.html">
			<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> 
			View Purchased Batches</a>
		</li>
		<li>
			<a href="getSupplyBatches.html">
			<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> 
			View Distributed Batches</a>
		</li>
		<li>
			<a href="getTaskReport.html">
			<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> 
			View Feed Issue Tasks</a>
		</li>
		<li>
			<a href="getCatchingTaskReport.html">
			<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> 
			View Catching Tasks</a>
		</li>
	</ul>
</div>
</html>