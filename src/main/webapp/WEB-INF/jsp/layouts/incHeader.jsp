<%@taglib prefix="bootstrap" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<bootstrap:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<bootstrap:url value="/resources/css/bootstrap-datetimepicker.css" />" rel="stylesheet">
    <link href="<bootstrap:url value="/resources/css/dashboard.css" />" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</html>