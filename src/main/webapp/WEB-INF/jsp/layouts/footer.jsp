<%@taglib prefix="bootstrap" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->

	<script src="<bootstrap:url value="/resources/js/jquery.min.js" />"></script>
    <script src="<bootstrap:url value="/resources/js/bootstrap.min.js" />"></script>
    <script src="<bootstrap:url value="/resources/js/moment.js" />"></script>
    <script src="<bootstrap:url value="/resources/js/bootstrap-datetimepicker.js" />"></script>
</html>