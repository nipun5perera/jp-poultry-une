<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<td>ID</td>
					<td>First Name</td>
					<td>Middle Name</td>
					<td>Last Name</td>
					<td>Contact</td>
					<td>NIC</td>
					<td>Update</td>
					<td>Delete</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${fieldOfficerReport}" var="fieldOfficer">
					<tr>
						<td>${fieldOfficer.fieldOfficerId}</td>
						<td>${fieldOfficer.fname}</td>
						<td>${fieldOfficer.mname}</td>
						<td>${fieldOfficer.lname}</td>
						<td>${fieldOfficer.contact}</td>
						<td>${fieldOfficer.NIC}</td>
						<td><a href="http://localhost:8080/jp/admin/updateFieldOfficer.html?foid=${fieldOfficer.fieldOfficerId}" class="btn btn-warning">Update</a></td>
						<td><a href="http://localhost:8080/jp/admin/deleteFieldOfficer.html?foid=${fieldOfficer.fieldOfficerId}" class="btn btn-danger">Delete</a></td>
					
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>