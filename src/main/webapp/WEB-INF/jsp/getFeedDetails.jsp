<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="table-responsive">
		<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Feed ID</th>
				<th>Name</th>
				<th>Manufacturer</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${feedDetails}" var="feed">
				<tr>
					<td>${feed.feedId}</td>
					<td>${feed.name}</td>
					<td>${feed.manufacturer}</td>
					<td>${feed.quantity}</td>
					<td>${feed.price}</td>
					<td><a href="http://localhost:8080/jp/admin/updateFeed.html?fid=${feed.feedId}" class="btn btn-warning">Update</a></td>
					<td><a href="http://localhost:8080/jp/admin/deleteFeed.html?fid=${feed.feedId}" class="btn btn-danger">Delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<%@include file="layouts/footer.jsp" %>
</body>
</html>