<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp"%>
<title>Add Farmer - JP Poultry</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

				<h1 class="page-header">Add Farmer</h1>

				<form:form commandName="farmer" class="form-horizontal">
				<form:errors path="*" Class="alert alert-danger" element="div" />

					<div class="form-group">
						<label for="inputName" class="col-sm-1 control-label">Farmer ID</label>
						<div class="col-sm-3">
							<form:input path="fid" type="text" class="form-control"
								id="inputName" placeholder="Enter Farmer Name" />
						</div>
					</div>
					<div class="form-group">
						<label for="inputName" class="col-sm-1 control-label">Name</label>
						<div class="col-sm-3">
							<form:input path="name" type="text" class="form-control"
								id="inputName" placeholder="Enter Farmer Name" />
						</div>
					</div>

					<div class="form-group">
						<label for="inputAddress" class="col-sm-1 control-label">Address</label>
						<div class="col-sm-3">
							<form:input path="address" type="text" class="form-control"
								id="inputAddress" placeholder="Enter Address" />
						</div>
					</div>

					<div class="form-group">
						<label for="inputContact" class="col-sm-1 control-label">Contact</label>
						<div class="col-sm-3">
							<form:input path="contact" type="text" class="form-control"
								id="inputContact" placeholder="Enter Contact Number" />
						</div>
					</div>

					<div class="form-group">
						<label for="inputCapacity" class="col-sm-1 control-label">Capacity</label>
						<div class="col-sm-3">
							<form:input path="capacity" type="text" class="form-control"
								id="inputCapacity" placeholder="Enter Capacity" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-4 control-label">
							<input type="submit" value="Add Farmer" class="btn btn-primary">
						</div>
					</div>

				</form:form>

				<h1 class="page-header">Farmers</h1>

				<%@include file="getFarmerDetails.jsp"%>
			</div>
		</div>
	</div>

	<%@include file="layouts/footer.jsp"%>
</body>
</html>