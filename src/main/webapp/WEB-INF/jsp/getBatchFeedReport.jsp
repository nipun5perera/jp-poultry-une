<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div class="table-responsive">
		<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Distribute Batch ID</th>
				<th>Feed</th>
				<th>Date</th>
				<th>Quantity</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${batchFeedReport}" var="batchFeed">
				<tr>
					<td>${batchFeed.feedIssueID}</td>
					<td>${batchFeed.supplyBatch.supplyBatchId}</td>
					<td>${batchFeed.feed.name}</td>
					<td>${batchFeed.date}</td>
					<td>${batchFeed.quantity}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<%@include file="layouts/footer.jsp" %>



</body>
</html>