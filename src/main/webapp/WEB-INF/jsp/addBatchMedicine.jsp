<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Issue Medicine - JP Poultry</title>

<script type="text/javascript" src="../resources/js/jquery.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
    $('#inputMedicineId').on('change', function() {
    //  alert( this.value ); // or $(this).val()
      
      var id = this.value;
      
      console.log(id);
      
      $.ajax({
    	  url: './getMedicineObject.json?mid='+id+'',
    	  success: function(data){
    		  console.log(data);
    		  $('#inputUnitPrice').val(data.price);
    	  }
    	});
      return false;
    });
});
</script>

<script>
$(document).ready(function() {  
    $('#inputMedicineId').change();
});
</script>

<script type="text/javascript">
function sum() {
    var  unitPrice = document.getElementById('inputUnitPrice').value;
    var quantity = document.getElementById('inputQuantity').value;
    var amount = parseFloat(unitPrice) * parseFloat(quantity);
    if (!isNaN(amount)) {
    	var tot =  parseFloat(amount).toFixed(2);
        document.getElementById('inputAmount').value = tot;
    }
}
</script>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@include file="layouts/admin.jsp"%>	
			
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		
			<h1 class="page-header">Issue Medicine</h1>
			
			<form:form commandName="batchMedicine" class="form-horizontal">
			
				 <div class="form-group">
					<label for="inputDate" class="col-sm-2 control-label">Date</label>
					<div class="col-sm-3">
						
				
						<div class="container">
						    <div class="row">
						        <div class='col-sm-3'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker1'>
						                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
						            $(function () {
						                $('#datetimepicker1').datetimepicker({
						                	format: 'YYYY-MM-DD'
						                });
						            });
						        </script>
						    </div>
						</div>
				
					</div>
				</div> 
			  	
			  	<div class="form-group">
				    <label for="inputBatchId" class="col-sm-2 control-label">Distribute Batch ID</label>
				    <div class="col-sm-3">
				      <form:input path="supplyBatch.supplyBatchId" value="${param.id}" class="form-control" id="inputBatchId"  readonly="true"/>
				    </div>
			  	</div>
			  	
			  		<div class="form-group">
						<label for="inputQuantity" class="col-sm-2 control-label">Bill Number</label>
						<div class="col-sm-2">
							<form:input path="billNumber" type="text" class="form-control" id="inputBillNumber"/>
						</div>
					</div>
			  	
			  	<div class="form-group">
				    <label for="inputMedicineId" class="col-sm-2 control-label">Medicine</label>
				    
				    
				    <div class="col-sm-3">
				      <form:select path="medicine.medicineId"
								id="inputMedicineId" class="form-control">
								<form:options items="${medicineReport}" itemLabel="name" itemValue="medicineId" />
							</form:select>
				    </div>
			  	</div>

			  	
			  		<div class="form-group">
						<label for="inputQuantity" class="col-sm-2 control-label">Unit Price</label>
						<div class="col-sm-2">
							<form:input path="unitPrice" type="text" class="form-control" id="inputUnitPrice"   onkeyup="sum()"/>
						</div>
					</div>
					
					<%-- <div class="form-group">
						<label for="inputQuantity" class="col-sm-2 control-label">Actual Price</label>
						<div class="col-sm-2">
							<form:input path="actualPrice" type="text" class="form-control" id="inputActualPrice" readonly="true" onkeyup="sum()"/>
						</div>
					</div>
					 --%>
					<div class="form-group">
				    <label for="inputQuantity" class="col-sm-2 control-label">Quantity</label>
				    <div class="col-sm-3">
				      <form:input path="quantity" type="text" class="form-control" id="inputQuantity" onkeyup="sum()"  />
				    </div>
			  	</div>	
					<div class="form-group">
						<label for="inputQuantity" class="col-sm-2 control-label">Amount</label>
						<div class="col-sm-2">
							<form:input path="amount" type="text" class="form-control" id="inputAmount" />
						</div>
					</div>
				  <div class="form-group">
				  <div class="col-sm-4 control-label">
				  	<input type="submit" value="Add Batch Medicine" class="btn btn-primary">
				  </div>
			 </div>	
			</form:form>
		</div>
	</div>
</div>

	 
	
		<%@include file="layouts/footer.jsp"%>
</body>
</html>