<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Purchase Chicks - JP Poultry</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#paymentRequest')
								.on(
										'change',
										function() {
											// alert( this.value ); // or $(this).val()

											var id = this.value;

											console.log(id);

											$
													.ajax({
														url : 'http://localhost:8080/jp/getPaymentRequestObject.json?pid='
																+ id + '',
														success : function(data) {
															console.log(data);
															$('#paymentAmount')
																	.val(
																			data.amount);
															$('#paymentDate')
																	.val(
																			new Date(
																					data.date));
															$('#description')
																	.val(
																			data.description);

														}

													});
											return false;

										});
					});
</script>

<script>
jQuery(function($) {
    $('#inputChickPrice, #inputQuantity, #inputDiscount').on('keyup', function() {
       //alert('key up');
       
       var chickPrice = document.getElementById('inputChickPrice').value;
       
    var quantity = document.getElementById('inputQuantity').value;
  
    var purchaseOrderValue = parseFloat(chickPrice) * parseFloat(quantity);
   
    
    if (!isNaN(purchaseOrderValue)) {
       document.getElementById('inputPurchaseOrderValue').value = purchaseOrderValue.toFixed(2);
    }
    
    var discount = document.getElementById('inputDiscount').value;
    var actualOrderValue = parseFloat(purchaseOrderValue) - parseFloat(discount);
    
    if(!isNaN(actualOrderValue))
    {
    	document.getElementById('inputActualOrderValue').value = actualOrderValue.toFixed(2);
    }
    
       
       
    });
});
</script>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>
			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Purchase Chicks</h1>	
			
				<!-- left side of the page -->
				<div class="col-md-6">
					<form:form commandName="batch" class="form-horizontal" autocomplete="off">
					<form:errors path="*" Class="alert alert-danger" element="div" />	
						 <div class="form-group">
							<label for="inputDate" class="col-sm-3 control-label">Payment Request Date</label>
							<div class="col-sm-5">
								
						
								<div class="container">
								    <div class="row">
								        <div class='col-sm-3'>
								            <div class="form-group">
								                <div class='input-group date' id='datetimepicker1'>
								                    <form:input path="paymentRequest.date" type='text' class="form-control" id="inputDate"/>
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>
								        </div>
								        <script type="text/javascript">
								            $(function () {
								                $('#datetimepicker1').datetimepicker({
								                	format: 'YYYY-MM-DD'
								                });
								            });
								        </script>
								    </div>
								</div>
						
							</div>
						</div>



						<div class="form-group">
							<label for="inputDescription" class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<form:textarea path="paymentRequest.description" type="text"
									class="form-control" id="inputDescription"
									placeholder="Enter Short Description" rows="3" />
							</div>
						</div>

						<div class="form-group">
							<label for="inputAmount" class="col-sm-3 control-label">Amount</label>
							<div class="col-sm-5">
								<form:input path="paymentRequest.amount" type="text" class="form-control"
									id="inputAmount" placeholder="Enter Amount" />
							</div>
						</div>
					
						<div class="form-group">
							<label for="inputDate" class="col-sm-3 control-label">Date</label>
							<div class="col-sm-5">
								
						
								<div class="container">
								    <div class="row">
								        <div class='col-sm-3'>
								            <div class="form-group">
								                <div class='input-group date' id='datetimepicker2'>
								                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>
								        </div>
								        <script type="text/javascript">
								            $(function () {
								                $('#datetimepicker2').datetimepicker({
								                	format: 'YYYY-MM-DD'
								                });
								            });
								        </script>
								    </div>
								</div>
						
							</div>
						</div>
						 
						<div class="form-group">
							<label for="supplierId" class="col-sm-3 control-label">Supplier</label>
							<div class="col-sm-5">
								<form:select path="supplier.supplierId" class="form-control" id="supplierId">
									<form:options items="${supplierReport}" itemLabel="name" itemValue="supplierId" />
								</form:select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputChickPrice" class="col-sm-3 control-label">Chick Price</label>
							<div class="col-sm-5">
								<form:input path="chickPrice" type="text" class="form-control" id="inputChickPrice"  />
							</div>
						</div>	

						<div class="form-group">
							<label for="inputQuantity" class="col-sm-3 control-label">Quantity</label>
							<div class="col-sm-5">
								<form:input path="quantity" type="text" class="form-control" id="inputQuantity" />
							</div>
						</div>

						<div class="form-group">
							<label for="inputDiscount" class="col-sm-3 control-label">Discount</label>
							<div class="col-sm-5">
								<form:input path="discount" type="text" class="form-control" id="inputDiscount" />
							</div>
						</div>	
						<div class="form-group">
							<label for="inputPurchaseOrderValue" class="col-sm-3 control-label">Purchase Order Value</label>
							<div class="col-sm-5">
								<form:input path="purchaseOrderValue" type="text" class="form-control" id="inputPurchaseOrderValue" readonly="true"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputActualOrderValue" class="col-sm-3 control-label">Actual Order Value</label>
							<div class="col-sm-5">
								<form:input path="actualOrderValue" type="text" class="form-control" id="inputActualOrderValue" readonly="true"/>
							</div>
						</div>	
						<div class="form-group">
							<label for="inputWaxingStatus" class="col-sm-3 control-label">Waxing Status</label>
							<div class="col-sm-5">
								<form:checkbox path="waxingStatus" class="form-control" id="inputWaxingStatus" name="inputWaxingStatus" onClick="inb()"/>
							</div>
						</div>
						<div id="waxingDiv">
						<div id="waxingTot" class="form-group">
							<label for="inputWaxingTotal" class="col-sm-3 control-label">Waxing Total</label>
							<div class="col-sm-5">
								<form:input path="waxingTotal" type="text" class="form-control" id="inputWaxingTotal"/>
							</div>
						</div>	
						
						
						<div class="form-group">
							<label for="inputOneChickWaxingCost" class="col-sm-3 control-label">Waxing Cost For One Chick</label>
							<div class="col-sm-5">
								<form:input path="oneChickWaxingCost" type="text" class="form-control" id="inputOneChickWaxingCost"/>
							</div>
						</div>	
						</div>
						
						
						
						
						
				  <div class="col-sm-8 control-label">
				  	<input type="submit" value="Purchase Chicks" class="btn btn-primary">
				  </div>						
																																						
					</form:form>
				</div>
				
				<!-- right side of the page -->
				<%-- div class="col-md-6">
				<form class="form-horizontal">
				
						<div class="form-group">
							<label for="paymentDate" class="col-sm-3 control-label">Payment Date</label>
							<div class="col-sm-5">
								<input id="paymentDate" class="form-control"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="paymentAmount" class="col-sm-3 control-label">Amount</label>
							<div class="col-sm-5">
								<input id="paymentAmount" class="form-control"/>
							</div>
						</div>

						<div class="form-group">
							<label for="description" class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<input id="description" class="form-control"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="sname" class="col-sm-3 control-label">Supplier Name</label>
							<div class="col-sm-5">
								<input id="sname" class="form-control"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="address1" class="col-sm-3 control-label">Address Line 1</label>
							<div class="col-sm-5">
								<input id="address1" class="form-control"/>
							</div>
						</div>
						
						<div class="form-group">
							<label for="address2" class="col-sm-3 control-label">Address Line 2</label>
							<div class="col-sm-5">
								<input id="address2" class="form-control"/>
							</div>
						</div>	
						
						<div class="form-group">
							<label for="city" class="col-sm-3 control-label">City</label>
							<div class="col-sm-5">
								<input id="city" class="form-control"/>
							</div>
						</div>	
						
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-5">
								<input id="email" class="form-control"/>
							</div>
						</div>							

						<div class="form-group">
							<label for="contact" class="col-sm-3 control-label">Contact</label>
							<div class="col-sm-5">
								<input id="contact" class="form-control"/>
							</div>
						</div>
																	
				</form>
				</div>	 --%>			
			
			</div>		
		</div>
	</div>
	
	<%@include file="layouts/footer.jsp" %>
</body>
</html>