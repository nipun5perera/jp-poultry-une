<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp"%>
<title>Issue Additional Batch Feed - JP Poultry</title>

<script type="text/javascript" src="../resources/js/jquery.min.js"></script>

<script>
jQuery(function($) {
    $('#inputQuantity, #inputUnitPrice').on('keyup', function() {
    var unitPrice = document.getElementById('inputUnitPrice').value;
    var quantity = document.getElementById('inputQuantity').value;
    var amount = parseFloat(unitPrice) * parseFloat(quantity);
    
    if (!isNaN(amount)) {
       document.getElementById('inputAmount').value = amount.toFixed(2);
    }
    
    });
});
</script>
<script type="text/javascript">

$(document).ready(function() {
    $('#inputFeed').on('change', function() {
    //  alert( this.value ); // or $(this).val()
      
      var id = this.value;
      
      console.log(id);
      
      $.ajax({
    	  url: './getFeedObject.json?fid='+id+'',
    	  success: function(data){
    		  console.log(data);
    		  $('#inputUnitPrice').val(data.price);
    		  var unitPrice = document.getElementById('inputUnitPrice').value;
    	  }
    	});
      return false;
    });
});
</script>
<script>
$(document).ready(function() {  
    $('#inputFeed').change();
});
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Issue Additional Batch Feed</h1>		
			
				<form:form commandName="batchFeed" class="form-horizontal">
					<div class="form-group">
						<label for="inputDate" class="col-sm-1 control-label">Date</label>
						<div class="col-sm-3">
							
					
							<div class="container">
							    <div class="row">
							        <div class='col-sm-3'>
							            <div class="form-group">
							                <div class='input-group date' id='datetimepicker1'>
							                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
							                    <span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
							                </div>
							            </div>
							        </div>
							        <script type="text/javascript">
							            $(function () {
							                $('#datetimepicker1').datetimepicker({
							                	format: 'YYYY-MM-DD'
							                });
							            });
							        </script>
							    </div>
							</div>
					
						</div>
					</div>	
					
					<div class="form-group">
						<label for="inputBatch" class="col-sm-1 control-label">Distribute Batch ID</label>
						<div class="col-sm-2">
							<form:input path="supplyBatch.supplyBatchId" type="text" class="form-control" id="inputBatch" value="${param.bid}" readonly="true" />
						</div>
					</div>	
					<div class="form-group">
						<label for="inputBatch" class="col-sm-1 control-label">Bill No</label>
						<div class="col-sm-2">
							<form:input path="billNumber" type="text" class="form-control" id="inputBillNumber"  />
						</div>
					</div>	
					<div class="form-group">
						<label for="inputFeed" class="col-sm-1 control-label">Feed</label>
						<div class="col-sm-2">
							<%--  <form:input path="feed.feedId" type="text" class="form-control" id="inputFeed"/>--%>

							<form:select path="feed.feedId" id="inputFeed"
								class="form-control">
								<form:options items="${feedReport}" itemLabel="feedNameManufacturer"
									itemValue="feedId" />
							</form:select>
						</div>
					</div>	
					
					<div class="form-group">
						<label for="inputQuantity" class="col-sm-1 control-label">Unit Price</label>
						<div class="col-sm-2">
							<form:input path="unitPrice" type="text" class="form-control" id="inputUnitPrice"/>
						</div>
					</div>
					<div class="form-group">
						<label for="inputQuantity" class="col-sm-1 control-label">Quantity</label>
						<div class="col-sm-2">
							<form:input path="quantity" type="text" class="form-control" id="inputQuantity"/>
						</div>
					</div>
						
					<div class="form-group">
						<label for="inputQuantity" class="col-sm-1 control-label">Amount</label>
						<div class="col-sm-2">
							<form:input path="amount" type="text" class="form-control" id="inputAmount"/>
						</div>
					</div>
					
					<div class="form-group">
				  		<div class="col-sm-3 control-label">
				  			<input type="submit" value="Add Batch Feed" class="btn btn-primary">	
				  		</div>
				  	</div>			
				  	
				</form:form>
				<%@include file="getBatchFeedReport.jsp"%>
			</div>
		</div>
	</div>

	<%@include file="layouts/footer.jsp"%>
</body>
</html>