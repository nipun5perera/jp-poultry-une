<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>View Distributed Batches - JP Poultry</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
</head>
<body>
<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

				<h1 class="page-header">View Distributed Batch</h1>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Distribute Batch ID</th>
							<th>Date</th>
							<th>Field Officers</th>
							<th>Purchase Batch ID</th>
							<th>Quantity</th>
							<th>Farmer</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${supplyBatchReport}" var="supplyBatch">
							<tr>
								<td>${supplyBatch.distributeBatchID}</td>
								<td>${supplyBatch.date}</td>
								<td> ${supplyBatch.fieldOfficer.fname} ${supplyBatch.fieldOfficer.mname} ${supplyBatch.fieldOfficer.lname}</td>
								<td>${supplyBatch.purchaseBatchID}</td>
								<td>${supplyBatch.quantity}</td>
								<td>${supplyBatch.farmer.name}</td>
								<td>
								<div class="dropdown">
								 	<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Actions<span class="caret"></span></button>
									<ul class="dropdown-menu  dropdown-menu-right" role="menu" aria-labelledby="menu1">
									<li class="dropdown-header">Batch Actions</li>
										<!--  <li><a href="http://localhost:8080/jp/addBatchFeed.html?bid=${supplyBatch.supplyBatchId}" >Issue Feed</a></li>
										<li><a href="http://localhost:8080/jp/addBatchMedicine.html?id=${supplyBatch.supplyBatchId}" >Issue Medicine</a></li>-->
										
										<!--  <li><a href="http://localhost:8080/jp/addCatching.html?bid=${supplyBatch.supplyBatchId}" >Catch</a></li>-->
										
										
										<c:url var="feedReturn" value="addFeedReturn.html?bid=${supplyBatch.supplyBatchId}">Perform Catching</c:url>
										<li><a href="${feedReturn}"	>Feed Return</a></li>
										
										<c:url var="addFieldReport" value="addFieldReport.html?bid=${supplyBatch.supplyBatchId}">Perform Catching</c:url>
										<li ><a href="${addFieldReport}"	>Add Field Report</a></li>
										
										<c:url var="issueMedicine" value="addBatchMedicine.html?id=${supplyBatch.supplyBatchId}">Perform Catching</c:url>
										<li ><a href="${issueMedicine}">Issue Medicine</a></li>
										
										<c:url var="issueAdditionalFeed" value="addAdditionalBatchFeed.html?bid=${supplyBatch.supplyBatchId}">Perform Catching</c:url>
										<li ><a href="${issueAdditionalFeed}">Issue Additional Feed</a></li>
										
										<li role="separator" class="divider"></li>
										<li class="dropdown-header">General Actions</li>
										
										<c:url var="updateSupplyBatch" value="updateSupplyBatch.html?sbid=${supplyBatch.supplyBatchId}">Perform Catching</c:url>
										<li class="label-warning"><a	href="${updateSupplyBatch}" class="label-warning"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbsp;Update</a></li>
										
										<c:url var="deleteSupplyBatch" value="deleteSupplyBatch.html?sbid=${supplyBatch.supplyBatchId}">Perform Catching</c:url>
										<li class="label-danger"><a	href="${deleteSupplyBatch}" class="label-danger"><span class="glyphicon glyphicon-trash " aria-hidden="true"></span> Delete</a></li>
										
										
										
									</ul>
								</div>
								</td>
							

							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
	</div>

	<%@include file="layouts/footer.jsp" %>
</body>
</html>
