<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Add Farm - JP Poultry</title>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@include file="layouts/admin.jsp" %>
		
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		
			<h1 class="page-header">Add Farm</h1>
		
			<form:form commandName="farm" class="form-horizontal">
				<div class="form-group">
				    <label for="inputCapacity" class="col-sm-1 control-label">Capacity</label>
				    <div class="col-sm-3">
				      <form:input path="capacity" type="text" class="form-control" id="inputCapacity" placeholder="Enter Capacity" />
				    </div>
			  </div>
			  <div class="form-group">
				  <div class="col-sm-4 control-label">
				  	<input type="submit" value="Add Farm" class="btn btn-primary">
				  </div>
			 </div>
			</form:form>
			
		</div>
	</div>
</div>

	
<%@include file="layouts/footer.jsp" %>
</body>
</html>