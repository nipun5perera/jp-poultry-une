
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Medicine - JP Poultry</title>
</head>
<body>
<div class="table-responsive">
	
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Medicine ID</th>
				<th>Name</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
	
		<tbody>	
			<c:forEach items="${medicineReport}" var="medicine">
				<tr>
					<td>${medicine.medicineId}</td>
					<td>${medicine.name}</td>
					<td>${medicine.quantity}</td>
					<td>${medicine.price}</td>
					<td><a href="http://localhost:8080/jp/admin/updateMedicine.html?mid=${medicine.medicineId}" class="btn btn-warning">Update</a></td>
					<td><a href="http://localhost:8080/jp/admin/deleteMedicine.html?mid=${medicine.medicineId}" class="btn btn-danger">Delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

</body>
</html>