<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>
			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1>${message}</h1>
			<table class="table table-bordered table-hover">
				<tbody>
					<tr>
						<td>Distribute Batch ID</td>
						<td>DB-${supplyBatch.supplyBatchId}</td>
					</tr>
					<tr>
						<td>Date</td>
						<td>${supplyBatch.date}</td>
					</tr>
					<tr>
						<td>Field Officers</td>
						<td>${supplyBatch.fieldOfficer.fname}
							${supplyBatch.fieldOfficer.mname}
							${supplyBatch.fieldOfficer.lname}</td>
					</tr>
					<tr>
						<td>Purchase Batch ID</td>
						<td>PB-${supplyBatch.batch.batchId}</td>
					</tr>
					<tr>
						<td>Farmer</td>
						<td>${supplyBatch.farmer.name}</td>
					</tr>
					<tr>
						<td>Bill Number</td>
						<td>${supplyBatch.billNumber}</td>
					</tr>
					<tr>
						<td>Chick Price</td>
						<td>${supplyBatch.chickPrice}</td>
					</tr>

				</tbody>
			</table>
			</div>
		</div>
	</div>

</body>
</html>