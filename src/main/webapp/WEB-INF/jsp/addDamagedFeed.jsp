<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="layouts/header.jsp"%>
<title>Insert title here</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>

<script type="text/javascript">
	function addRowToTable() {
		var rowCount = document.getElementById('feedTable').rows.length;
		//	alert(rowCount-1);
		var pid = rowCount - 1;
		//	alert(rowCount);
		var feedClone = $('#feed').clone();
		var quantityClone = $('#quantity').clone();
		feedClone.attr("name", "damagedFeedDetails[" + pid + "].feed.feedId");
		quantityClone.attr("name", "damagedFeedDetails[" + pid + "].quantity");

		$('#feedTable')
				.find('tbody')
				.append(
						$('<tr>')
								.append($('<td>').append(feedClone))
								.append($('<td>').append(quantityClone))
								.append(
										$('<td>')
												.append(
														'<input type="button" id="del" class="delRowBtn" value="Delete"/>'))

				);

		//append("<tr><td>").append(dropClone).append("</td><td>2</td><td>3</td></tr>")

		rowCount = document.getElementById('feedTable').rows.length;
		//	alert(rowCount-1);

		$(document.body).delegate(".delRowBtn", "click", function() {
			$(this).closest("tr").remove();
		});

	}
</script>

</head>
<body>

	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

				<h1 class="page-header">Add Damaged Feed</h1>

				<form:form commandName="damagedFeed" class="form-horizontal">
				<form:errors path="*" Class="alert alert-danger" element="div" />

					<div class="form-group">
						<label for="inputDate" class="col-sm-2 control-label">Date</label>
						<div class="col-sm-5">


							<div class="container">
								<div class="row">
									<div class='col-sm-3'>
										<div class="form-group">
											<div class='input-group date' id='datetimepicker1'>
												<form:input path="date" type='text' class="form-control"
													id="inputDate" />
												<span class="input-group-addon"> <span
													class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
										</div>
									</div>
									<script type="text/javascript">
										$(function() {
											$('#datetimepicker1')
													.datetimepicker({
														format : 'YYYY-MM-DD'
													});
										});
									</script>
								</div>
							</div>

						</div>
					</div>

					<div class="form-group">
						<label for="inputQuantity" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-4">
							<form:textarea path="description" type="text" class="form-control"
								id="inputQuantity" placeholder="Enter Description"  rows="5" cols="200"/>
						</div>
					</div>


					<div class="table-responsive">
						<table id="feedTable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<td style="width: 500px;">Feed</td>
									<td style="width: 300px;">Quantity</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><form:select path="damagedFeedDetails[0].feed.feedId"
											id="feed" class="form-control">
											<form:options items="${feedReport}"
												itemLabel="feedNameManufacturer" itemValue="feedId" />
										</form:select></td>
									<td><form:input path="damagedFeedDetails[0].quantity"
											id="quantity" class="form-control" /></td>
								</tr>
							</tbody>
						</table>
					</div>

					<INPUT type="button" value="Add More" onclick="addRowToTable()"
						class="btn btn-info" />

					<input type="submit" value="Add Damaged Feed" class="btn btn-primary">

				</form:form>

			</div>
		</div>
	</div>

</body>
</html>