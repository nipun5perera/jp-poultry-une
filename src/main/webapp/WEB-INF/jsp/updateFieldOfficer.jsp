<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Field Officer</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Update Field Officer</h1>

				<form:form commandName="fieldOfficer" class="form-horizontal">
				
				
					<div class="form-group">
						<label for="inputFname" class="col-sm-1	 control-label">ID</label>
						<div class="col-sm-3">
							<form:input path="fieldOfficerId" type="text" class="form-control"
								id="inputFname" placeholder="Enter First Name" />
						</div>
					</div>
					<div class="form-group">
						<label for="inputFname" class="col-sm-1	 control-label">First	Name</label>
						<div class="col-sm-3">
							<form:input path="fname" type="text" class="form-control"
								id="inputFname" placeholder="Enter First Name" />
						</div>
					</div>

					<div class="form-group">
						<label for="inputMname" class="col-sm-1 control-label">Middle Name</label>
						<div class="col-sm-3">
							<form:input path="mname" type="text" class="form-control"
								id="inputMname" placeholder="Enter Middle Name" />
						</div>
					</div>
						 
					<div class="form-group">
						<label for="inputLname" class="col-sm-1 control-label">Last Name</label>
						<div class="col-sm-3">
							<form:input path="lname" type="text" class="form-control"
								id="inputLname" placeholder="Enter Last Name" />
						</div>
					</div>	 
					
					<div class="form-group">
						<label for="inputNIC" class="col-sm-1 control-label">NIC</label>
						<div class="col-sm-3">
							<form:input path="NIC" type="text" class="form-control"
								id="inputNIC" placeholder="NIC" />
						</div>
					</div>	
					
					<div class="form-group">
						<label for="inputContact" class="col-sm-1 control-label">Contact</label>
						<div class="col-sm-3">
							<form:input path="contact" type="text" class="form-control"
								id="inputContact" placeholder="Contact Number" />
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-4 control-label">
							<input type="submit" value="Update Field Officer" class="btn btn-primary">
						</div>
					</div>

				</form:form>

			</div>

		</div>
	</div>
</body>
</html>