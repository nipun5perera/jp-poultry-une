<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form:form  method="post" action="addBatchFeed">
		Task Id: ${task.taskId}<br>
		Description: ${task.description}<br>
		Due Date: ${task.duedate}<br>
		Completed Date :  <form:input path="performedDate" type="text" class="form-control" id="inputCapacity" placeholder="Enter Capacity" />
		Status : ${task.status}<br>
		
		<br>
		<br>

		Batch ID = ${task.batch.batchId}<br>
		Quantity = ${task.batch.quantity}<br>
		Farmer = ${task.batch.farmer.name}<br>
		Farmer's Address = ${task.batch.farmer.address}<br>
		Field Officer = ${task.batch.fieldOfficer.fname} ${task.batch.fieldOfficer.mname} <br>
		<input type="submit" value="Completed" class="btn btn-primary">
	</form:form>
</body>
</html>