<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Medicine - JP Poultry</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Add Medicine</h1>

				<form:form commandName="medicine"  class="form-horizontal">
				<form:errors path="*" Class="alert alert-danger" element="div" />
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label"> Name </label>
						<div class="col-sm-3">
							<form:input path="name" type="text" class="form-control"
								placeholder="Enter Medicine Name" />
						</div>

					</div>
					<div class="form-group">
						<label for="quantity" class="col-sm-2 control-label">Quantity
						</label>
						<div class="col-sm-3">
							<form:input path="quantity" type="text" class="form-control"
								placeholder="Quantity" />
						</div>
					</div>
					<div class="form-group">
						<label for="quantity" class="col-sm-2 control-label">Price
						</label>
						<div class="col-sm-3">
							<form:input path="price" type="text" class="form-control"
								placeholder="price" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-4 control-label">
							<input type="submit" value="Add Medicine" class="btn btn-primary">
						</div>
					</div>


				</form:form>
				<h1 class="page-header">Medicines</h1>
					<%@include file="getMedicineDetails.jsp"%>
			</div>
		</div>
	</div>
	
	<%@include file="layouts/footer.jsp"%>

</body>
</html>