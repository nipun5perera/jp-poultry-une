<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp"%>
<title>Add Medicine Order- JP Poultry</title>

<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#paymentRequest')
								.on(
										'change',
										function() {
											// alert( this.value ); // or $(this).val()

											var id = this.value;

											console.log(id);

											$
													.ajax({
														url : './getPaymentRequestObject.json?pid='
																+ id + '',
														success : function(data) {
															console.log(data);
															$('#paymentAmount')
																	.val(
																			data.amount);
															$('#paymentDate')
																	.val(
																			new Date(
																					data.date));
															$('#description')
																	.val(
																			data.description);

														}

													});
											return false;

										});
					});
</script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#supplierId')
								.on(
										'change',
										function() {
											// alert( this.value ); // or $(this).val()

											var sid = this.value;

											console.log(sid);

											$
													.ajax({
														url : 'http://localhost:8080/jp/getSupplierObject.json?sid='
																+ sid + '',
														success : function(data) {
															console.log(data);
															$('#sname').val(
																	data.name);
															$('#address1')
																	.val(
																			data.address1);
															$('#address2')
																	.val(
																			data.address2);
															$('#city').val(
																	data.city);
															$('#email').val(
																	data.email);
															$('#contact')
																	.val(
																			data.contact);

														}

													});
											return false;

										});
					});
</script>

 

<script type="text/javascript"> 
function addRowToTable()
{
	var rowCount = document.getElementById('medicine').rows.length;
//	alert(rowCount-1);
	var pid = rowCount-1;
//	alert(rowCount);
	var medicineClone =  $('#medicineId').clone();
	var unitPriceClone = $('#unitPrice').clone();
	var quantityClone = $('#quantity').clone();
	medicineClone.attr("name", "medicineOrderDetails["+pid+"].medicine.medicineId");
	unitPriceClone.attr("name", "medicineOrderDetails["+pid+"].unitPrice");
	quantityClone.attr("name", "medicineOrderDetails["+pid+"].quantity");
	
	$('#medicine').find('tbody').
	append($('<tr>')
			.append($('<td>').append(medicineClone))
			.append($('<td>').append(unitPriceClone))
			.append($('<td>').append(quantityClone))
			.append($('<td>').append('<input type="button" id="del" class="delRowBtn" value="Delete"/>'))
			
	);
	
	//append("<tr><td>").append(dropClone).append("</td><td>2</td><td>3</td></tr>")
	
//	rowCount = document.getElementById('medcineId').rows.length;
//	alert(rowCount-1);
	 
	 
	$(document.body).delegate(".delRowBtn", "click", function(){
        $(this).closest("tr").remove();        
    });  
	
	
}
</script> 


</head>
<body>

	<div class="container-fluid">
		<div class="row">


			<%@include file="layouts/admin.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Update Medicine Order</h1>
			<!-- left side of the page -->
				<div class=".col-md-6">
						<form:form commandName="medicineOrder" class="form-horizontal">



						<div class="form-group">
							<label for="medicineOrderId" class="col-sm-2 control-label">Order ID</label>
							<div class="col-sm-5">
								<form:input  id="medecineOrderId" path="medicineOrderId" value="${param.moid}" class="form-control"/>
							</div>
						</div>

						<div class="form-group">
							<label for="inputDate" class="col-sm-2 control-label">Date</label>
							<div class="col-sm-5">
								
						
								<div class="container">
								    <div class="row">
								        <div class='col-sm-3'>
								            <div class="form-group">
								                <div class='input-group date' id='datetimepicker1'>
								                    <form:input path="date" type='text' class="form-control" id="inputDate"/>
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>
								        </div>
								        <script type="text/javascript">
								            $(function () {
								                $('#datetimepicker1').datetimepicker({
								                	format: 'YYYY-MM-DD'
								                });
								            });
								        </script>
								    </div>
								</div>
						
							</div>
						</div>
						
						<div class="form-group">
							<label for="paymentRequest" class="col-sm-2 control-label">Payment
								Request</label>
							<div class="col-sm-5">
								<form:select path="paymentRequest.paymentId" id="paymentRequest" type="text" class="form-control">
									<form:options items="${paymentRequestReport}" itemLabel="date"
										itemValue="paymentId"/>
								</form:select>
							</div>
						</div>
						<div class="form-group">
						<label for="paymentAmount" class="col-sm-2 control-label">Payment Amount</label>
						<div class="col-sm-5">
							<input id="paymentAmount" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="description" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-5">
							<textarea id="description" class="form-control" rows="3" disabled></textarea>
						</div>
					</div>
<!-- 					 -->
						<div class="form-group">
							<label for="supplierId" class="col-sm-2 control-label">Supplier</label>
							<div class="col-sm-5">
								<form:select path="supplier.supplierId" id="supplierId"  class="form-control">
									<form:options items="${supplierReport}" itemLabel="name"
										itemValue="supplierId" id=""/>
								</form:select>
							</div>
						</div>
						<div class="form-group">
							<label for="inputTotal" class="col-sm-2 control-label">Total</label>
							<div class="col-sm-5">
								<form:input path="total" type="text" class="form-control" id="inputTotal"/>
							</div>
						</div>
						<div class="form-group">
							<label for="inputDiscount" class="col-sm-2 control-label">Discount
							</label>
							<div class="col-sm-5">
								<form:input path="discount" type="text" class="form-control" id="inputDiscount"/>
							</div>
						</div>





						<!-- ********************************************************************************************* -->
						<div class="table-responsive">
						<table id="medicine" class="table table-bordered table-hover">
						<thead>
							<tr>
								<td  style="width:300px;">Medicine ID</td>
								<td>Unit Price</td>
								<td>Quantity</td>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${medicineOrder.medicineOrderDetails}" var="feedOrdeerDetails" varStatus="loop">
							<tr>
								 
										
								<td><form:select path="medicineOrderDetails[${loop.index}].medicine.medicineId"
								id="medicineId" class="form-control">
								<form:options items="${medicineReport}" itemLabel="name" itemValue="medicineId" />
							</form:select></td>		
								<td><form:input path="medicineOrderDetails[${loop.index}].unitPrice"  id="unitPrice" class="form-control"/></td>
								<td><form:input path="medicineOrderDetails[${loop.index}].quantity" id="quantity" class="form-control"/></td>
							</tr>
							</c:forEach>
							</tbody>
						</table>
						</div>
						
						
						<input type="button" value="Add More" onclick="addRowToTable()" class="btn btn-info"/>

						<input type="submit" value="Update Medicine Order" class="btn btn-primary">

					</form:form>
				</div>
				
				<!--  right side of the page -->
				<%-- <div class=".col-md-4">
				<form action="#" class="form-horizontal">

					<div class="form-group">
						<label for="paymentDate" class="col-sm-6 control-label">Payment Date</label>
						<div class="col-sm-5">
							<input id="paymentDate" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="paymentAmount" class="col-sm-3 control-label">Payment Amount</label>
						<div class="col-sm-5">
							<input id="paymentAmount" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="description" class="col-sm-3 control-label">Description</label>
						<div class="col-sm-5">
							<textarea id="description" class="form-control" rows="3" disabled></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label for="sname" class="col-sm-3 control-label">Supplier Name</label>
						<div class="col-sm-5">
							<input id="sname" class="form-control" disabled>
						</div>
					</div>	
					
					<div class="form-group">
						<label for="address1" class="col-sm-3 control-label">Address Line 1</label>
						<div class="col-sm-5">
							<input id="address1" class="form-control" disabled>
						</div>
					</div>						
	
					<div class="form-group">
						<label for="address2" class="col-sm-3 control-label">Address Line 2</label>
						<div class="col-sm-5">
							<input id="address2" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="city" class="col-sm-3 control-label">City</label>
						<div class="col-sm-5">
							<input id="city" class="form-control" disabled>
						</div>
					</div>	
					
					<div class="form-group">
						<label for="email" class="col-sm-3 control-label">Email</label>
						<div class="col-sm-5">
							<input id="email" class="form-control" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="contact" class="col-sm-3 control-label">Contact Number</label>
						<div class="col-sm-5">
							<input id="contact" class="form-control" disabled>
						</div>
					</div>				  	
		
			 	</form> 				
				</div> --%>
			</div>
		</div>
	</div>
	<%@include file="layouts/footer.jsp"%>

</body>
</html>