<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<title>Task Reports - JP Poultry</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
<script>
function dateChanged(ev) {
    $(this).datepicker('hide');
    if ($('#startdate').val() != '' && $('#enddate').val() != '') {
        $('#period').text(diffInDays() + ' d.');
    } else {
        $('#period').text("-");
    }
}

</script>
<script>
function getTaskById(){
	var date = $('#inputDate').val();
	
	$("#tblTask").find("tr:gt(0)").remove();
	// alert( this.value ); // or $(this).val()

	var sid = this.value;

 

	$.ajax({
				url : 'http://localhost:8080/jp/user/getCatchingTaskByDate.json?date='+date+'',
				success : function(data) {
					console.log(data);
					
					for(var i = 0; i < data.length; i++) {
					    var obj = data[i];
					  
					    var act;
					    
					    
					    
					    var dt = new Date(obj.duedate);
					    
					  // 	alert(dt);
					    
					    
					    if(obj.type == "Feed Issuing")
					    	{
					    	//alert("Feed Issuing");
					    	act='<td><a	href="./addBatchFeed.html?tid='+obj.taskId+'&bid='+obj.supplyBatch.supplyBatchId+'"	class="btn btn-primary">Issue Feed</a></td>'
					    	}
					    else
					    	{
					    	act='<td><a href="./addCatching.html?tid='+obj.taskId+'&bid='+obj.supplyBatch.supplyBatchId+'" class="btn btn-primary">Catch</a></td>';
					    	}
					    
					    $('#tblTask tr:last').after('<tr><td>'+obj.taskId+'</td><td>'+dt+'</td><td>'+obj.status+'</td><td>'+obj.distributeBatchID+'</td>'+act+'</tr>');
					    console.log("batch "+obj.supplyBatch.supplyBatchId);
					}

				}

			});
	return false;

}
 



</script>
 
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<%@include file="layouts/admin.jsp"%>
		
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Catching Task Report</h1>
			
			<div class="form-group">
				    <label for="inputDate" class="col-sm-1 control-label">Date</label>
			 
				      
				      	<div class="container">
						    <div class="row">
						        <div class='col-sm-4'>
						            <div class="form-group">
						                <div class='input-group date' id='datetimepicker1'>
						                    <input  type='text' class="form-control" id="inputDate"/>
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
						            </div>
						        </div>
						        <script type="text/javascript">
						            $(function () {
						                $('#datetimepicker1').datetimepicker({
						                	format: 'YYYY-MM-DD'
						                	 
						                });
						            });
						          
						           
						           
						        </script>
						        <input type="button" value="Filter" onclick="getTaskById()" class="btn btn-info"/>
						    </div>
						
				    </div>
			  	
			  	</div>
			  	
			
			<div class="table-responsive">
				<table class="table table-bordered table-hover" id="tblTask">
					<thead>
						<tr>
							<td>Task Id</td>
							 
							<td>Date</td>
							<td>Status</td>
							<td>Distribute Batch ID</td>
							<td>Action</td>
							
							
						</tr>
					</thead>
						<tbody>
							<c:forEach items="${taskReport}" var="task">
								<tr>
									<td>${task.taskId}</td>
								 
									<td>${task.duedate}</td>
									<td>${task.status}</td>
									<td>${task.distributeBatchID}</td>
									<c:choose>
										<c:when test="${task.type == 'Catching'}">
											<td>
											<c:url var="performCatching" value="addCatching.html?tid=${task.taskId}&bid=${task.supplyBatch.supplyBatchId}">Perform Catching</c:url>
											   <a
												href="${performCatching}"
												class="btn btn-primary">Perform Catching</a></td>
										</c:when>
										<%-- <c:otherwise>
											<td><a
												href="http://localhost:8080/jp/addBatchFeed.html?tid=${task.taskId}&bid=${task.supplyBatch.supplyBatchId}"
												class="btn btn-primary">Issue Feed</a></td>
										</c:otherwise> --%>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>

					</table>
			</div>			
			
		</div>
		
	</div>
</div>

	<%@include file="layouts/footer.jsp" %>
</body>
</html>