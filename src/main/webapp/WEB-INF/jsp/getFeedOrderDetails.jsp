<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Discount</th>
				<th>Total</th>
				<th>Supplier ID</th>
				<th>Supplier Name</th>
				<th>Payment request</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${feedOrderReport}" var="feedOrder">
				<tr>
					<td>${feedOrder.feedOrderId}</td>
					<td>${feedOrder.date}</td>
					<td>${feedOrder.discount}</td>
					<td>${feedOrder.total}</td>
					<td>${feedOrder.supplier.supplierId}</td>
					<td>${feedOrder.supplier.name}</td>
					<td>${feedOrder.paymentRequest.status}</td>
					<td><a href="http://localhost:8080/jp/admin/updateFeedOrder.html?foid=${feedOrder.feedOrderId}" class="btn btn-warning">Update</a></td>
					<td><a href="http://localhost:8080/jp/admin/deleteFeedOrder.html?foid=${feedOrder.feedOrderId}" class="btn btn-danger">Delete</a></td>
					<c:choose>
						<c:when test="${feedOrder.paymentRequest.status=='Approved'}">
							<c:choose>
								<c:when test="${feedOrder.fulFilled == false}">
									<td><a href="http://localhost:8080/jp/admin/approveFeedOrder.html?foid=${feedOrder.feedOrderId}" class="btn btn-danger">Approve</a></td>
								</c:when>
							</c:choose>
						</c:when>
					</c:choose>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
</body>
</html>