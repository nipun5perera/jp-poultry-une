<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="layouts/header.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1>${message}</h1>
				<table class="table table-bordered table-hover">
					<tbody>
						<tr>
							<td>Date</td>
							<td>${batch.date}</td>
						</tr>
						<tr>
							<td>Purchase Batch ID</td>
							<td>PB-${batch.batchId}</td>
						</tr>
						<tr>
							<td>Quantity</td>
							<td>${batch.quantity}</td>
						</tr>
						<tr>
							<td>Actual Order Value</td>
							<td>${batch.actualOrderValue}</td>
						</tr>
						<tr>
							<td>Purchase Order Value</td>
							<td>${batch.purchaseOrderValue}</td>
						</tr>
						<tr>
							<td>Discount</td>
							<td>${batch.discount}</td>
						</tr>
						<tr>
							<td>Waxing Status</td>
							<td>${batch.waxingStatus}</td>
						</tr>
						<tr>
							<td>One Chick Waxing Cost</td>
							<td>${batch.oneChickWaxingCost}</td>
						</tr>
						<tr>
							<td>Supplier</td>
							<td>${batch.supplier.name}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>