<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="layouts/header.jsp" %>
<title>Get Purchase Batches</title>
<script type="text/javascript" src="../resources/js/jquery.min.js"></script>
</head>
<body>

<div class="container-fluid">
		<div class="row">
			<%@include file="layouts/admin.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

				<h1 class="page-header">View Purchased Batches</h1>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
						<th>ID</th>
						<th>Date</th>
						<th>Quantity</th>
						<th>Chick Price</th>
						<th>Purchase Order Value</th>
						<th>Actual Order Value</th>
						<th>Discount</th>
						<th>One Chick Waxing Cost</th>
						<th>Total Waxing Cost</th>
						<th>Supplier</th>
						<th>Payment Request</th>
							 
						</tr>
					</thead>
					<tbody>
					
						<c:forEach items="${batchReport}" var="batch">
						<tr>
							 <td>${batch.batchId}</td>
							 <td>${batch.date}</td>
							  <td>${batch.quantity}</td>
							  <td>${batch.chickPrice}</td>
							  <td>${batch.purchaseOrderValue}</td>
							  <td>${batch.actualOrderValue}</td>
							  <td>${batch.discount}</td>
							  <td>${batch.oneChickWaxingCost}</td>
							  <td>${batch.waxingTotal}</td>
							  <td>${batch.supplier.name}</td>
							  
							  <c:choose>
									<c:when test="${batch.paymentRequest.status == 'Pending'}">
									<security:authorize ifAnyGranted="ROLE_ADMIN">
										  <c:url var="paymentRequest" value="approvePaymentRequest.html?pid=${batch.paymentRequest.paymentId}">Payment Request</c:url>
							 				 <td><a href="${paymentRequest}">${batch.paymentRequest.paymentId}</a></td>
											</security:authorize>
									</c:when>
									<c:when test="${batch.approved == false}">
									<security:authorize ifAnyGranted="ROLE_ADMIN">
										  <c:url var="approveBatch" value="approveBatch.html?bid=${batch.batchId}">Approve Batch</c:url>
							 				 <td><a href="${approveBatch}">Approve Batch</a></td>
											</security:authorize>
									</c:when>

								</c:choose>
							  
							 
							  </tr>
						</c:forEach>
						
					</tbody>
				</table>
			</div>

		</div>
	</div>

	<%@include file="layouts/footer.jsp" %>

</body>
</html>