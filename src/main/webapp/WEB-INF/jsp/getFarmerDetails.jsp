<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Get Farmer Details - JP Poultry</title>
</head>
<body>
<div class="table-responsive">
		<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Farmer ID</th>
				<th>Name</th>
				<th>Address</th>
				<th>Capacity</th>
				<th>Contact</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${farmerReport}" var="farmer">
				<tr>
					<td>${farmer.farmerId}</td>
					<td>${farmer.name}</td>
					<td>${farmer.address}</td>
					<td>${farmer.capacity}</td>
					<td>${farmer.contact}</td>
					<td><a href="http://localhost:8080/jp/admin/updateFarmer.html?fid=${farmer.farmerId}" class="btn btn-warning">Update</a></td>
					<td><a href="http://localhost:8080/jp/admin/deleteFarmer.html?fid=${farmer.farmerId}" class="btn btn-danger">Delete</a></td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
</div>

<%@include file="layouts/footer.jsp" %>
</body>
</html>