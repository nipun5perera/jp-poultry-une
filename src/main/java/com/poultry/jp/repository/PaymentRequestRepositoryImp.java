package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.PaymentRequest;
import com.poultry.jp.report.PaymentRequestReport;

@Repository("paymentRequestRepository")
public class PaymentRequestRepositoryImp implements PaymentRequestRepository {

 
	@PersistenceContext
	private EntityManager em;

	public PaymentRequest save(PaymentRequest paymentRequest) {
		paymentRequest.setStatus("Pending");
		em.persist(paymentRequest);

		return paymentRequest;
	}

	public List<PaymentRequestReport> getPaymentRequestDates() {

		TypedQuery<PaymentRequestReport> paymentRequests = em.createNamedQuery(
				PaymentRequest.FIND_PAYMENT_REQUEST_DATES,
				PaymentRequestReport.class);

		return paymentRequests.getResultList();
	}

	public PaymentRequestReport getPaymentRequest(Long id) {
		 

		TypedQuery<PaymentRequestReport> query = em.createNamedQuery(PaymentRequest.FIND_PAYMENT_REQUEST_BY_ID,PaymentRequestReport.class);
		
		query.setParameter("id", id);
		
		
		return query.getResultList().get(0);
	}

	public PaymentRequest getPaymentRequestObject(Long id) {
		Session session = em.unwrap(Session.class);
		PaymentRequest paymentReequest = (PaymentRequest) session.load(PaymentRequest.class, id);
		System.out.println("date "+paymentReequest.getDate());
		
		return paymentReequest;
	}

	public List<PaymentRequestReport> getPaymentRequestReport() {
		
		TypedQuery<PaymentRequestReport> namedQuery = em.createNamedQuery(PaymentRequest.FIND_PAYMENT_REQUEST_REPORT,PaymentRequestReport.class);
		
		List<PaymentRequestReport> paymentRequestReport = namedQuery.getResultList();
		
		return paymentRequestReport;
	}

	public PaymentRequest deletePaymentRequest(Long id) {
		Session session = em.unwrap(Session.class);
		PaymentRequest paymentReq =(PaymentRequest) session.get(PaymentRequest.class,id);
		paymentReq.setDeleted(true);
		
		
		return  em.merge(paymentReq);
	}

	public PaymentRequest updatePaymentRequest(PaymentRequest paymentRequest) {

		Session session = em.unwrap(Session.class);
		
		PaymentRequest pm =  (PaymentRequest) session.load(PaymentRequest.class, paymentRequest.getPaymentId());
		
		pm.setAmount(paymentRequest.getAmount());
		pm.setDescription(paymentRequest.getDescription());
		pm.setDate(paymentRequest.getDate());
		return (PaymentRequest) session.merge(pm);
	}

	public com.poultry.jp.model.PaymentRequest PaymentRequest(Long id) {
		 
		Session session = em.unwrap(Session.class);
		
		PaymentRequest pm =  (PaymentRequest) session.load(PaymentRequest.class, id);
		pm.setStatus("Approved");
		
		return (PaymentRequest) session.merge(pm);
	}

	public PaymentRequest setPaymentRequestIsPaidStatus(Long id) {
		Session session = em.unwrap(Session.class);
		PaymentRequest pm = (PaymentRequest) session.load(PaymentRequest.class,id);
		pm.setPaid(true);
		

		return (PaymentRequest) session.merge(pm);
	}

	public PaymentRequest approvePaymentRequest(PaymentRequest paymentRequest) {
		Session session = em.unwrap(Session.class);
		PaymentRequest pm = (PaymentRequest) session.load(PaymentRequest.class,paymentRequest.getPaymentId());
		pm.setChequeID(paymentRequest.getChequeID());
		pm.setStatus("Approved");
		
		return (PaymentRequest) session.merge(pm);
	}

}
