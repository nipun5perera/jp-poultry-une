package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Medicine;
import com.poultry.jp.report.MedicineReport;

@Repository("medicineRepository")
public class MedicineRepositoryImp implements MedicineRepository {

	
	@PersistenceContext
	private EntityManager em;
	
	public Medicine save(Medicine medicine) {

		em.persist(medicine);
		em.flush();

		return medicine;
	}

	public List<MedicineReport> getMedicineDetails() {
		
		TypedQuery<MedicineReport> query = em.createNamedQuery(Medicine.FIND_MEDICINE_DETAILS,MedicineReport.class);
		
		return query.getResultList();
	}

	public Medicine getMedicine(Long id) {
		Session session = em.unwrap(Session.class);
		
		return (Medicine) session.get(Medicine.class, id);
	}

	public Medicine update(Medicine medicine) {
		Session session = em.unwrap(Session.class);
		Medicine medicineOb = (Medicine) session.get(Medicine.class, medicine.getMedicineId());
		medicineOb.setName(medicine.getName());
		medicineOb.setQuantity(medicine.getQuantity());
		
		return (Medicine) session.merge(medicineOb);
	}

	public Medicine delete(Long id) {
		Session session = em.unwrap(Session.class);
		Medicine medicine = (Medicine) session.get(Medicine.class, id);
		medicine.setDeleted(true);
		return (Medicine) session.merge(medicine);
	}

}
