package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.FieldOfficer;
import com.poultry.jp.report.FieldOfficerReport;

@Repository("fieldOfficer")
public class FieldOfficerRepositoryImp implements FieldOfficerRepository {

	@PersistenceContext
	private EntityManager em;
	
	
	public FieldOfficer save(FieldOfficer fieldOfficer) {
		
		em.persist(fieldOfficer);
		em.flush();
		return fieldOfficer;
	}


	public List<FieldOfficerReport> getFieldOfficerReport() {
		 
		TypedQuery<FieldOfficerReport> query = em.createNamedQuery(FieldOfficer.FIND_FIELD_OFFICER_REPORTS,FieldOfficerReport.class);
		return query.getResultList();
	}
	
	public List<FieldOfficerReport> getAllFieldOfficers() {
		 
		TypedQuery<FieldOfficerReport> query = em.createNamedQuery(FieldOfficer.FIND_ALL_FIELD_OFFICER,FieldOfficerReport.class);
		return query.getResultList();
	}

	
	public FieldOfficer getFieldOfficerObject(Long id) {
		Session session = em.unwrap(Session.class);
		FieldOfficer fieldOfficer = (FieldOfficer) session.get(FieldOfficer.class, id);
		return fieldOfficer;
	}


	public FieldOfficer updateFieldOfficer(FieldOfficer fieldOfficer) {
		Session session = em.unwrap(Session.class);
		
		FieldOfficer fieldOfficerOb = (FieldOfficer) session.get(FieldOfficer.class, fieldOfficer.getFieldOfficerId());
		
		fieldOfficerOb.setContact(fieldOfficer.getContact());
		fieldOfficerOb.setFname(fieldOfficer.getFname());
		fieldOfficerOb.setLname(fieldOfficer.getLname());
		fieldOfficerOb.setMname(fieldOfficer.getMname());
		fieldOfficerOb.setNIC(fieldOfficer.getNIC());
		
		
		
		return (FieldOfficer) session.merge(fieldOfficerOb);
	}


	public FieldOfficer delete(Long id) {
		Session session = em.unwrap(Session.class);
		FieldOfficer fieldOfficer = (FieldOfficer) session.get(FieldOfficer.class, id);
		fieldOfficer.setDeleted(true);
		
		return (FieldOfficer) session.merge(fieldOfficer);
	}

}
