/**
 * 
 */
package com.poultry.jp.repository;

import com.poultry.jp.model.MedicineOrderDetails;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
public interface MedicineOrderDetailsRepository {
	
	public MedicineOrderDetails save(MedicineOrderDetails medicineOrderDetails);
	
}
