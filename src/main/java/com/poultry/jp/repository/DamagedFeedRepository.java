package com.poultry.jp.repository;

import com.poultry.jp.model.Damaged_Feed;

public interface DamagedFeedRepository {
	public Damaged_Feed saveDamagedFeed(Damaged_Feed damagedFeed);

}
