/**
 * 
 */
package com.poultry.jp.repository;

import java.util.List;

import com.poultry.jp.model.Farmer;
import com.poultry.jp.report.FarmerReport;

/**
 * @author Nipun Perera
 * @created 24 Jul 2015
 *
 */
public interface FarmerRepository {
	public Farmer save(Farmer farmer);

	public List<FarmerReport> getFarmerReport();
	
	public List<FarmerReport> getAvailableFarmers();

	public List<FarmerReport> getFarmerDetails();

	public Farmer getFarmer(long id);

	public Farmer update(Farmer farmer);

	public Farmer delete(Long fid);
}
