package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Supplier;
import com.poultry.jp.report.SupplierReport;

@Repository("supplierRepository")
public class SupplierRepositoryImp implements SupplierRepository {

	@PersistenceContext
	private EntityManager em;

	public Supplier save(Supplier supplier) {

		em.persist(supplier);
		em.flush();

		return supplier;
	}

	public List<SupplierReport> getSupplierNames() {

		TypedQuery<SupplierReport> query = em.createNamedQuery(
				Supplier.FIND_SUPPLIER_NAMES, SupplierReport.class);

		return query.getResultList();
	}

	public List<SupplierReport> getSupplierDetails() {
		TypedQuery<SupplierReport> query = em.createNamedQuery(Supplier.FIND_SUPPLIER_DETAILS, SupplierReport.class);

		return query.getResultList();
	}

	public Supplier getSupplierObject(Long id) {
		Session session = em.unwrap(Session.class);
		Supplier supplier = (Supplier) session.get(Supplier.class, id);
		return supplier;
	}

	public Supplier update(Supplier supplier) {
		
		Session session = em.unwrap(Session.class);
		Supplier sup = (Supplier) session.get(Supplier.class, supplier.getSupplierId());
		
		sup.setAddress1(supplier.getAddress1());
		sup.setAddress2(supplier.getAddress2());
		sup.setCity(supplier.getCity());
		sup.setContact(supplier.getContact());
		sup.setEmail(supplier.getEmail());
		sup.setName(supplier.getName());
		
		
		
		return (Supplier) session.merge(sup);
	}

	public Supplier delete(Long id) {
		
		Session session = em.unwrap(Session.class);
		Supplier supplier = (Supplier) session.get(Supplier.class, id);
		supplier.setDeleted(true);
		
		return (Supplier) session.merge(supplier);
	}

	 
}
