package com.poultry.jp.repository;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Farmer;
import com.poultry.jp.model.Feed;
import com.poultry.jp.model.FeedIssue_Task;
import com.poultry.jp.model.SupplyBatch;
import com.poultry.jp.report.SupplyBatchReport;
import com.poultry.jp.model.Task;

@Repository("SupplyBatch")
public class SupplyBatchRepositoryImp implements SupplyBatchRepository{

	@PersistenceContext
	private EntityManager entityManager;
	
	public SupplyBatch save(SupplyBatch supplyBatch){
		

		//setting the batch quantity equal to supply quantity
		//modify UI if supply batch is separated to few farmers
		 
		Session session = entityManager.unwrap(Session.class);
		Query query =   session.createQuery("FROM SupplyBatch sb WHERE sb.isCaught = 'false' AND sb.farmer = "+supplyBatch.getFarmer().getFarmerId()+" ");
		List<SupplyBatch> existingSupplyBatches = query.list();
		
		System.out.println("Existing supply batches size ="+existingSupplyBatches.size());
		
		Farmer farmer = (Farmer) session.get(Farmer.class, supplyBatch.getFarmer().getFarmerId());
		
		int existingChicks = 0;
		
		for(int i=0;i< existingSupplyBatches.size();i++)
		{
			existingChicks+=existingSupplyBatches.get(i).getQuantity();
			
		}
		System.out.println("Existing chicks="+existingChicks);
		
		
		int currentCapacity = farmer.getCapacity() - existingChicks;
		System.out.println("currentCapacity="+currentCapacity);
		System.out.println("batch quantity"+supplyBatch.getQuantity() );
		if(currentCapacity <= supplyBatch.getQuantity())
		{
			
			System.out.println("Set farmer available false" );
			farmer.setAvailable(false);
		}
		else 
		{
			farmer.setAvailable(true);
		}
		
		
		
		session.merge(farmer); 
		
		int quantity = supplyBatch.getQuantity();
		int qt1,qt2,qt3,qt4,qt5;
		
//		supplyBatch.getBatch().setQuantity(supplyBatch.getQuantity());
		Task catching = new Task();
		
		 FeedIssue_Task feedIssueTask1 = new FeedIssue_Task();
		 FeedIssue_Task feedIssueTask2 = new FeedIssue_Task();
		 FeedIssue_Task feedIssueTask3 = new FeedIssue_Task();
		 FeedIssue_Task feedIssueTask4 = new FeedIssue_Task();
		 FeedIssue_Task feedIssueTask5 = new FeedIssue_Task();
	 
		
//		Session s = entityManager.unwrap(Session.class);
		
		qt1 = quantity/1000*4;
		
		
		feedIssueTask1.setStatus("Pending");
		Calendar c =   Calendar.getInstance();
		c.setTime(supplyBatch.getDate());
		feedIssueTask1.setDuedate(c.getTime());
		Feed booster = new Feed();
		booster.setFeedId(1);
		feedIssueTask1.setQuantity(qt1);
		feedIssueTask1.setSupplyBatch(supplyBatch);
		feedIssueTask1.setFeedType("Booster");
		feedIssueTask1.setRound(1);
		feedIssueTask1.setType("Feed Issuing");
		supplyBatch.getTask().add(feedIssueTask1);
		
		//--------------------------------------------
		qt2 = quantity/1000*8;
		feedIssueTask2.setStatus("Pending");
		c.add(Calendar.DATE, 5);
		feedIssueTask2.setDuedate(c.getTime());
		Feed starter = new Feed();
		starter.setFeedId(2);
		feedIssueTask2.setQuantity(qt2);
		feedIssueTask2.setSupplyBatch(supplyBatch);
		feedIssueTask2.setFeedType("Starter");
		feedIssueTask2.setRound(1);
		feedIssueTask2.setType("Feed Issuing");
		supplyBatch.getTask().add(feedIssueTask2);
		
		//------------------------------------------------
		
		qt3 = quantity/1000*13;
		c.add(Calendar.DATE, 8);
		feedIssueTask3.setStatus("Pending");
		feedIssueTask3.setDuedate(c.getTime());
		Feed starter1 = new Feed();
		starter1.setFeedId(2);
		feedIssueTask3.setQuantity(qt3);
		feedIssueTask3.setSupplyBatch(supplyBatch);
		feedIssueTask3.setFeedType("Starter");
		feedIssueTask3.setRound(2);
		feedIssueTask3.setType("Feed Issuing");
		supplyBatch.getTask().add(feedIssueTask3);
		
		//--------------------------------
		
		qt4 = quantity/1000*15;
		feedIssueTask4.setStatus("Pending");
		c.add(Calendar.DATE, 7);
		feedIssueTask4.setDuedate(c.getTime());
		Feed finisher = new Feed();
		finisher.setFeedId(3);
		feedIssueTask4.setQuantity(qt4);
		feedIssueTask4.setSupplyBatch(supplyBatch);
		feedIssueTask4.setFeedType("Finisher");
		feedIssueTask4.setRound(1);
		feedIssueTask4.setType("Feed Issuing");
		supplyBatch.getTask().add(feedIssueTask4);
		
		//--------------------------------
		
		qt5 = quantity/1000*25;
		feedIssueTask5.setStatus("Pending");
		c.add(Calendar.DATE, 6);
		feedIssueTask5.setDuedate(c.getTime());
		Feed finisher1 = new Feed();
		finisher1.setFeedId(3);
		feedIssueTask5.setQuantity(qt5);
		feedIssueTask5.setSupplyBatch(supplyBatch);
		feedIssueTask5.setFeedType("Finisher");
		feedIssueTask5.setRound(2);
		feedIssueTask5.setType("Feed Issuing");
		supplyBatch.getTask().add(feedIssueTask5);
		
		//--------------------------------
		
//		qt5 = quantity/1000*25;
//		feedIssue4.setStatus("Pending");
//		c.add(Calendar.DATE, 6);
//		feedIssue4.setSupplyBatch(supplyBatch);
//		feedIssue4.setDuedate(c.getTime());
//		supplyBatch.getTask().add(feedIssue4);
//		feedIssue4.setType("Feed Issuing");
//		
		catching.setStatus("Pending");
		c.add(Calendar.DATE, 9);
		catching.setSupplyBatch(supplyBatch);
		catching.setDuedate(c.getTime());
		catching.setType("Catching");
		supplyBatch.getTask().add(catching);
		
 
		
		
		entityManager.persist(supplyBatch);
		
		System.out.println("batch ID======================== "+ supplyBatch.getSupplyBatchId());
		entityManager.flush();
		
		return supplyBatch;
	}

	public List<SupplyBatchReport> getSupplyBatches() {
		
		TypedQuery<SupplyBatchReport> supplyBatchReport = entityManager.createNamedQuery(SupplyBatch.FIND_SUPPLY_BATCHES, SupplyBatchReport.class);
		
		return supplyBatchReport.getResultList();
	}

	public SupplyBatch deleteSupplyBatch(Long id) {
		
		Session session = entityManager.unwrap(Session.class);
		SupplyBatch supplyBatch = (SupplyBatch) session.get(SupplyBatch.class, id);
		supplyBatch.setDeleted(true);
		return  entityManager.merge(supplyBatch);
	}

	
	public SupplyBatch getSupplyBatch(Long id) {
		Session session = entityManager.unwrap(Session.class);
		return  (SupplyBatch) session.get(SupplyBatch.class, id);
	}

	public SupplyBatch updateSupplyBatch(SupplyBatch supplyBatch) {
		
		Session session = entityManager.unwrap(Session.class);
		SupplyBatch sb = getSupplyBatch(supplyBatch.getSupplyBatchId());
		
//		sb.setActualOrderValue(supplyBatch.getActualOrderValue());
//		sb.setChickPrice(supplyBatch.getChickPrice());
		sb.setDate(supplyBatch.getDate());
//		sb.setDiscount(supplyBatch.getDiscount());
//		sb.setOneChickWaxingCost(supplyBatch.getOneChickWaxingCost());
//		sb.setPurchaseOrderValue(supplyBatch.getPurchaseOrderValue());
		sb.setQuantity(supplyBatch.getQuantity());
		
		session.merge(sb);
		
		
		return null;
	}
}
