/**
 * 
 */
package com.poultry.jp.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Batch;
import com.poultry.jp.report.BatchReport;


@Repository("BatchRepository")
public class BatchRepositoryImp implements BatchRepository{

	@PersistenceContext
	private EntityManager entityManager;
	
	public Batch save(Batch batch){
		
		batch.getPaymentRequest().setStatus("Pending");
		batch.setIssuedAmount(0);
		entityManager.persist(batch);
		entityManager.flush();
		
		return batch;
	}

	public List<BatchReport> getBatchReport() {
		TypedQuery<BatchReport> query = entityManager.createNamedQuery(Batch.GETBATCHREPORT,BatchReport.class);
		return query.getResultList();
	}
	
	public List<Batch> getAvailableBatches()
	{
		List<Batch> batches = new ArrayList<Batch>() ;
		String sql = "FROM Batch b WHERE b.issuedAmount < b.quantity ";
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery(sql);
		batches = query.list();
		
		return batches;
		
	}

	public Batch getBatchObject(Long id) {
		Session session = entityManager.unwrap(Session.class);
		Batch batch = (Batch) session.get(Batch.class, id);
		return batch;
	}

	public List<BatchReport> getBatchDatesAndIds() {
		
		String sql = "SELECT new com.poultry.jp.report.BatchReport(b.batchId,b.date) FROM Batch b WHERE b.issuedAmount < b.quantity AND b.isDeleted = false and b.isApproved = true";
		System.out.println("sql-"+sql);
		Session session  = entityManager.unwrap(Session.class);
		
		Query quer = session.createQuery(sql);
		
		return quer.list();
	}
	
	public Batch approveBatch(Batch pbatch)
	{
		Session session = entityManager.unwrap(Session.class);
		Batch batch = (Batch) session.load(Batch.class,pbatch.getBatchId());
		batch.setApproved(true);
		
		return (Batch) session.merge(batch);
	}
	

}
