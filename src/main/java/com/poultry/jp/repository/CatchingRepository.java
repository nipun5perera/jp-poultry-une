package com.poultry.jp.repository;

import com.poultry.jp.model.Catching;

public interface CatchingRepository {

	public Catching save(Catching catching);
	
}
