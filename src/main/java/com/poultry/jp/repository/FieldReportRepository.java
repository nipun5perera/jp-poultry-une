package com.poultry.jp.repository;

import java.util.List;

import com.poultry.jp.model.FieldReport;
import com.poultry.jp.report.FieldReportReport;

public interface FieldReportRepository {

	public FieldReport save(FieldReport fieldReport);
	
	public long getNumOfChicksDied(Long id);
	
	public List<FieldReportReport> getFieldReport(Long bid);
	
}
