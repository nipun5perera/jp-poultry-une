package com.poultry.jp.repository;

import java.util.List;

import com.poultry.jp.model.Feed;
import com.poultry.jp.report.FeedReport;

public interface FeedRepository {
	
	public Feed Save(Feed feed);

	public List<FeedReport> getFeedDetails();

	public Feed getFeed(Long id);

	public Feed update(Feed feed);

	public Feed delete(Long id);

}
