package com.poultry.jp.repository;

import java.util.List;

import com.poultry.jp.model.Batch_Medicine;
import com.poultry.jp.report.Batch_MedicineReport;

public interface Batch_MedicineRepository {
	
	public Batch_Medicine save(Batch_Medicine batchMedicine);

	public List<Batch_MedicineReport> getBatchMedicines(Long id);

}
