/**
 * 
 */
package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.MedicineOrder;
import com.poultry.jp.model.MedicineOrderDetails;
import com.poultry.jp.report.MedicineOrderReport;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
@Repository("MedicineOrderRepository")
public class MedicineOrderRepositoryImp implements MedicineOrderRepository{

	@PersistenceContext
	private EntityManager entityManager;
	
	public MedicineOrder save(MedicineOrder medicineOrder){
		
		for(MedicineOrderDetails order : medicineOrder.getMedicineOrderDetails())
		{
			order.setMedicineOrder(medicineOrder);
		}
		
		medicineOrder.getPaymentRequest().setStatus("Pending");
		entityManager.persist(medicineOrder);
		entityManager.flush();
		
		return medicineOrder;
	}

	public List<MedicineOrderReport> getMedicineOrderDetails() {
		Session session = entityManager.unwrap(Session.class);
		Query query =   session.createQuery("FROM MedicineOrder as mb "
				+ "LEFT JOIN FETCH mb.paymentRequest as pr WHERE mb.inStock = 'false' and mb.isDeleted = 'false' ");
		
		
//		TypedQuery<MedicineOrderReport> query = entityManager.createNamedQuery(MedicineOrder.FIND_MEDICINE_ORDER_DETAILS,MedicineOrderReport.class);
		
		
		
		List<MedicineOrderReport> medicineOrder = query.list();
		return medicineOrder;
	}

	public MedicineOrder updateMedicineOrder(MedicineOrder medicineOrder) {
		
		Session session = entityManager.unwrap(Session.class);
		MedicineOrder mo = (MedicineOrder) session.get(MedicineOrder.class, medicineOrder.getMedicineOrderId());
		

		for(MedicineOrderDetails detail : medicineOrder.getMedicineOrderDetails())
		{	
			System.out.println("Detail quantity---------------"+ detail.getQuantity());
			
			for(int i=0;i<mo.getMedicineOrderDetails().size();i++)
			{
				if(mo.getMedicineOrderDetails().get(i).getMedicine().getMedicineId()== detail.getMedicine().getMedicineId())
				{
					mo.getMedicineOrderDetails().remove(i);
					
				}
				 
			}
			
			detail.setMedicineOrder(mo);
			session.merge(detail);
		}
		
		session.flush();
		

		mo.setDate(medicineOrder.getDate());
		mo.setTotal(medicineOrder.getTotal());
		mo.setDiscount(medicineOrder.getDiscount());
		mo.setPaymentRequest(medicineOrder.getPaymentRequest());
		mo.setSupplier(medicineOrder.getSupplier());
		
		
		session.merge(mo);
		session.flush();
		
		return mo;
	}

	public MedicineOrder getMedcineOrder(Long id) {
		Session session = entityManager.unwrap(Session.class);
		
		return (MedicineOrder) session.get(MedicineOrder.class, id);
	}

	
	public MedicineOrder delete(Long id) {
		Session session = entityManager.unwrap(Session.class);
		
		MedicineOrder medicineOrder = (MedicineOrder) session.get(MedicineOrder.class, id);
		medicineOrder.setDeleted(true);
		
		return (MedicineOrder) session.merge(medicineOrder);
	}

	public MedicineOrder updateMedicineOrderInstock(Long medOrderID) {
		Session session = entityManager.unwrap(Session.class);
		MedicineOrder mo = (MedicineOrder) session.get(MedicineOrder.class,medOrderID);
		mo.setInStock(true);
		return null;
	}
}
