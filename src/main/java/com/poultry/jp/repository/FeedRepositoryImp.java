package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Feed;
import com.poultry.jp.report.FeedReport;

@Repository("feedRepository")
public class FeedRepositoryImp implements FeedRepository {

	@PersistenceContext
	private EntityManager em;
	
	public Feed Save(Feed feed) {
		
		em.persist(feed);
		em.flush();
		
		return feed;
	}

	public List<FeedReport> getFeedDetails() {
		 TypedQuery<FeedReport> query = em.createNamedQuery(Feed.FIND_FEED_DETAILS, FeedReport.class);
		 return query.getResultList();
	}

	public Feed getFeed(Long id) {
		
		Session session = em.unwrap(Session.class);
		
		return (Feed) session.get(Feed.class, id);
	}

	public Feed update(Feed feed) {
		
		Session session = em.unwrap(Session.class);
		Feed feedOb = (Feed) session.get(Feed.class, feed.getFeedId());
		
		feedOb.setName(feed.getName());
		feedOb.setQuantity(feed.getQuantity());
		
		return (Feed) session.merge(feedOb);
	}

	public Feed delete(Long id) {
		Session session = em.unwrap(Session.class);
		Feed feed = (Feed) session.get(Feed.class, id);
		feed.setDeleted(true);
		
		return (Feed) session.merge(feed);
	}

}
