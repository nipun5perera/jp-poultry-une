/**
 * 
 */
package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Farmer;
import com.poultry.jp.report.FarmerReport;

@Repository("FarmerRepository")
public class FarmerRepositoryImp implements FarmerRepository{
	
	@PersistenceContext 
	private EntityManager entityManager;
	
	public Farmer save(Farmer farmer){
		farmer.setAvailable(true);
		entityManager.persist(farmer);
		entityManager.flush();
		
		return farmer;
	}

	public List<FarmerReport> getFarmerReport() {
		TypedQuery<FarmerReport> query = entityManager.createNamedQuery(Farmer.FIND_FARMER_NAMES,FarmerReport.class); 
		return query.getResultList();
	}

	public List<FarmerReport> getFarmerDetails() {
		TypedQuery<FarmerReport> query = entityManager.createNamedQuery(Farmer.FIND_FARMER_DETAILS,FarmerReport.class); 
		return query.getResultList();
	}

	public Farmer getFarmer(long id) {
		
		
		Session session = entityManager.unwrap(Session.class);
		return (Farmer) session.get(Farmer.class,id);
	}

	public Farmer update(Farmer farmer) {
		Session session = entityManager.unwrap(Session.class);
		
		Farmer farmerOb = (Farmer) session.get(Farmer.class, farmer.getFarmerId());
		
		farmerOb.setAddress(farmer.getAddress());
		farmerOb.setCapacity(farmer.getCapacity());
		farmerOb.setContact(farmer.getContact());
		farmerOb.setName(farmer.getName());
		
				
		
		return (Farmer) session.merge(farmerOb);
	}

	public Farmer delete(Long fid) {
		Session session = entityManager.unwrap(Session.class);
		Farmer farmer = (Farmer) session.get(Farmer.class, fid);
		farmer.setDeleted(true);
		
		
		return (Farmer) session.merge(farmer);
	}

	public List<FarmerReport> getAvailableFarmers() {
		
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("FROM Farmer f WHERE f.isAvailable = true");
		List<FarmerReport> farmers = query.list();
		return farmers;
	}
}
