package com.poultry.jp.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Damaged_Feed;

@Repository
public class DamagedFeedRepositoryImp implements DamagedFeedRepository {

	@PersistenceContext
	private EntityManager em;

	public Damaged_Feed saveDamagedFeed(Damaged_Feed damagedFeed) {
		System.out.println("came here");
		em.persist(damagedFeed);
		return damagedFeed;
	}

}
