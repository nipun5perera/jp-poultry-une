package com.poultry.jp.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Batch_Medicine;
import com.poultry.jp.report.Batch_MedicineReport;
import com.poultry.jp.model.Medicine;

@Repository("BatchMedicineRepository")
public class Batch_MedicineRepositoryImp implements Batch_MedicineRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public Batch_Medicine save(Batch_Medicine batchMedicine) {
		
		entityManager.persist(batchMedicine);
		entityManager.flush();
		
		Session session  = entityManager.unwrap(Session.class);
		Medicine medicine = (Medicine) session.get(Medicine.class, batchMedicine.getMedicine().getMedicineId());
		batchMedicine.setActualPrice(medicine.getPrice());
		
		return batchMedicine;
	}

	public List<Batch_MedicineReport> getBatchMedicines(Long id) {
		
		List<Batch_MedicineReport> batchMedicineReport = new ArrayList<Batch_MedicineReport>();
		TypedQuery<Batch_MedicineReport> query = entityManager.createNamedQuery(Batch_Medicine.FINDBATCHMEDICINEBYBATCHID, Batch_MedicineReport.class);
		query.setParameter("bid", id);
		batchMedicineReport = query.getResultList();
		
		return batchMedicineReport;
	}

}
