package com.poultry.jp.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Batch_Feed;
import com.poultry.jp.report.Batch_FeedReport;
import com.poultry.jp.model.Feed;

@Repository("BatchFeedRepository")
public class Batch_FeedRepositoryImp implements Batch_FeedRepository {

	@PersistenceContext
	private EntityManager em;
	
	public Batch_Feed save(Batch_Feed batchFeed) {

		
	 
//		Feed feed = new Feed();
		Session session  = em.unwrap(Session.class);
		Feed feed = (Feed) session.get(Feed.class, batchFeed.getFeed().getFeedId());
//		System.out.println("Feed Price="+batchFeed.getFeed().getFeedId());
		batchFeed.setActualPrice(feed.getPrice());
		em.persist(batchFeed);
		em.flush();
		
		return batchFeed;
	}

	public List<Batch_FeedReport> getBatchFeeds(Long id) {
		
		List<Batch_FeedReport> batchFeed = new ArrayList<Batch_FeedReport>();
		TypedQuery<Batch_FeedReport> query = em.createNamedQuery(Batch_Feed.FINDBATCHFEEDBYID, Batch_FeedReport.class);
		query.setParameter("bid", id);
		
		batchFeed = query.getResultList();
		
		return batchFeed;
	}

	public List<Batch_FeedReport> getAllBatchFeeds() {

		List<Batch_FeedReport> batchFeed = new ArrayList<Batch_FeedReport>();
		TypedQuery<Batch_FeedReport> query = em.createNamedQuery(Batch_Feed.FINDALLBATCHFEED, Batch_FeedReport.class);

		batchFeed = query.getResultList();

		return batchFeed;
	}

}
