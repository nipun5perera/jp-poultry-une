package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.FeedOrder;
import com.poultry.jp.model.FeedOrderDetail;
import com.poultry.jp.report.FeedOrderReport;
import com.poultry.jp.model.PaymentRequest;

@Repository("feedOrderRepository")
public class FeedOrderRepositoryImp implements FeedOrderRepository {

	@PersistenceContext
	private EntityManager em;
	
	public FeedOrder save(FeedOrder feedOrder) {
		for(FeedOrderDetail detail : feedOrder.getFeedOrderDetail())
		{
			detail.setFeedOrder(feedOrder);
		}
		feedOrder.getPaymentRequest().setStatus("Pending");
		em.persist(feedOrder);
		em.flush();

		return null;
	}

	public List<FeedOrderReport> getFeedOrderDetails() {
		
//		TypedQuery<FeedOrderReport> query = em.createNamedQuery(FeedOrder.FIND_FEED_ORDER_DETAILS, FeedOrderReport.class);
		
		Session session = em.unwrap(Session.class);
		Query query =   session.createQuery("FROM FeedOrder as fb "
				+ "LEFT JOIN FETCH fb.paymentRequest as pr");
		
		List<FeedOrderReport> feedOrder = query.list();
		
		return feedOrder;
		
		
		
	}

	public FeedOrder updateFeedOrder(FeedOrder feedOrder) {
		
		
		
		Session session = em.unwrap(Session.class);
		FeedOrder fo = (FeedOrder) session.get(FeedOrder.class, feedOrder.getFeedOrderId());
		PaymentRequest paymentRequest = fo.getPaymentRequest();
		System.out.println("feed order id------"+feedOrder.getFeedOrderDetail().size());
//		fo.getFeedOrderDetail().clear();
//		fo.setFeedOrderDetail(feedOrder.getFeedOrderDetail());
//		 
		
		for(FeedOrderDetail detail : feedOrder.getFeedOrderDetail())
		{	
			System.out.println("Detail quantity---------------"+ detail.getQuantity());
			
			for(int i=0;i<fo.getFeedOrderDetail().size();i++)
			{
				if(fo.getFeedOrderDetail().get(i).getFeed().getFeedId() == detail.getFeed().getFeedId())
				{
					fo.getFeedOrderDetail().remove(i);
					
				}
				 
			}
			detail.setFeedOrder(fo);
			session.merge(detail);
		}
		
		
		
		session.flush();
 
		//fo.setFeedOrderDetail(feedOrder.getFeedOrderDetail());
		
		
		
		System.out.println("SIZE -------------------"+fo.getFeedOrderDetail().size());
//		for(int i =0;i<fo.get)
		
		
		fo.setDate(feedOrder.getDate());
		fo.setTotal(feedOrder.getTotal());
		fo.setDiscount(feedOrder.getDiscount());
		fo.setPaymentRequest(feedOrder.getPaymentRequest());
		fo.setSupplier(feedOrder.getSupplier());
		
		
		paymentRequest.setAmount(feedOrder.getPaymentRequest().getAmount());
		paymentRequest.setDescription(feedOrder.getPaymentRequest().getDescription());
		
		fo.setPaymentRequest(paymentRequest);
		
		session.merge(fo);
		session.flush();
		
		
		
		
		return fo;
	}

	public FeedOrder getFeedOrder(Long id) {
		Session session = em.unwrap(Session.class);
		FeedOrder  feedOrder = (FeedOrder) session.get(FeedOrder.class, id);
		return feedOrder;
	}

	public FeedOrder delete(Long id) {
		// TODO Auto-generated method stub
		Session session = em.unwrap(Session.class);
		FeedOrder feedOrder = (FeedOrder) session.get(FeedOrder.class, id);
		feedOrder.setDeleted(true);
		
		return (FeedOrder) session.merge(feedOrder);
	}

	public FeedOrder updateFeedOrderInStockStatus(Long feedOrderId) {
		
		Session session = em.unwrap(Session.class);
		FeedOrder feedOrder = (FeedOrder)session.get(FeedOrder.class, feedOrderId);
		feedOrder.setInStock(true);
		return (FeedOrder) session.merge(feedOrder);
	}

	
	public FeedOrder approveFeedOrder(FeedOrder feedOrder) {
		
		
		
		Session session = em.unwrap(Session.class);
		FeedOrder fo = (FeedOrder) session.get(FeedOrder.class, feedOrder.getFeedOrderId());
		
		for(FeedOrderDetail detail : fo.getFeedOrderDetail())
		{	
			
			
			for(int i=0;i<feedOrder.getFeedOrderDetail().size();i++)
			{
				if(feedOrder.getFeedOrderDetail().get(i).getFeed().getFeedId() == detail.getFeed().getFeedId())
				{
					detail.setFilledQuantity(feedOrder.getFeedOrderDetail().get(i).getFilledQuantity());
					System.out.println("Detail quantity---------------"+ fo.getFeedOrderDetail().get(i).getFilledQuantity());
				}
				 
			}
			 
		}
		
		fo.setFulFilled(feedOrder.isFulFilled());
		session.merge(fo);
		session.flush();
		
		
		
		
		return fo;
	}
}
