package com.poultry.jp.repository;

 

import java.util.Date;
import java.util.List;

import com.poultry.jp.report.Catching_TaskReport;
import com.poultry.jp.report.FeedIssue_TaskReport;
import com.poultry.jp.model.Task;
import com.poultry.jp.report.TaskReport;

public interface TaskRepository {
	
	public List<TaskReport> getTaskReport();

	public Task getSelectedTask(String tid);

	public List<Task> getTasksByDate(Date dat);

	public Task updateTaskStatus(Task pTask);

	public List<FeedIssue_TaskReport> getFeedIssueTasks();

	public List<Catching_TaskReport> getCatchingTasks();

	public List<TaskReport> getCatchingTasksByDate(Date date);
	
	public List<FeedIssue_TaskReport> getFeedIssueTasksByDate(Date dat);

}
