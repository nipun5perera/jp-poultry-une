package com.poultry.jp.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.poultry.jp.model.FeedReturn;

@Repository("feedReturn")
public class FeedReturnRepositoryImp implements FeedReturnRepository {

	@PersistenceContext
	private EntityManager em;
	
	public FeedReturn save(FeedReturn feedReturn) {
		em.persist(feedReturn);
		em.flush();
		return feedReturn;
	}

}
