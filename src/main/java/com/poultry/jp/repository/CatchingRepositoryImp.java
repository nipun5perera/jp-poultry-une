package com.poultry.jp.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.Catching;
import com.poultry.jp.model.Farmer;
import com.poultry.jp.model.SupplyBatch;

@Repository("catchingRepository")
public class CatchingRepositoryImp implements CatchingRepository {

	@PersistenceContext
	private EntityManager em;
	
	public Catching save(Catching catching) {
		Session session = em.unwrap(Session.class);
		SupplyBatch batch = (SupplyBatch) session.get(SupplyBatch.class, catching.getSupplyBatch().getSupplyBatchId());
//		long fid = (Long) session.get(Farmer.class, catching.getSupplyBatch().getFarmer().getFarmerId());
		System.out.println("farmer ID"+batch.getFarmer().getFarmerId());
		Farmer farmer = (Farmer) session.get(Farmer.class, batch.getFarmer().getFarmerId());
		farmer.setAvailable(true);
		batch.setCaught(true);
		em.merge(farmer);
		em.merge(batch);
		em.persist(catching);
		
		em.flush();
		return catching;
	}

}
