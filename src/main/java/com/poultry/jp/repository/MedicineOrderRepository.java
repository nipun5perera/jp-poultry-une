/**
 * 
 */
package com.poultry.jp.repository;

import java.util.List;

import com.poultry.jp.model.MedicineOrder;
import com.poultry.jp.report.MedicineOrderReport;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
public interface MedicineOrderRepository {
	
	public MedicineOrder save(MedicineOrder medicineOrder);

	public List<MedicineOrderReport> getMedicineOrderDetails();

	public MedicineOrder updateMedicineOrder(MedicineOrder medicineOrder);

	public MedicineOrder getMedcineOrder(Long id);

	public MedicineOrder delete(Long id);

	public MedicineOrder updateMedicineOrderInstock(Long medOrderID);
	
}
