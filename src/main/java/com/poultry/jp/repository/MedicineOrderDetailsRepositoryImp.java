/**
 * 
 */
package com.poultry.jp.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.poultry.jp.model.MedicineOrderDetails;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
@Repository("MedicineOrderDetailsRepository")
public class MedicineOrderDetailsRepositoryImp implements MedicineOrderDetailsRepository{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public MedicineOrderDetails save(MedicineOrderDetails medicineOrderDetails){
		
		entityManager.persist(medicineOrderDetails);
		entityManager.flush();
		
		return medicineOrderDetails;
	}
}
