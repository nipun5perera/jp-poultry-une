package com.poultry.jp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.model.FieldReport;
import com.poultry.jp.report.FieldReportReport;

@Repository("fieldReport")
public class FieldReportRepositoryImp implements FieldReportRepository {

	
	@PersistenceContext
	private EntityManager em;
	
	public FieldReport save(FieldReport fieldReport) {
		
		em.persist(fieldReport);
		return fieldReport;
	}
	
	public long getNumOfChicksDied(Long id)
	{
		long chicks = 0;
		
		Session session = em.unwrap(Session.class);
		
		Query query = session.createQuery("SELECT SUM(F.numOfChicksDied) FROM FieldReport F WHERE F.supplyBatch.supplyBatchId = :bid GROUP BY F.supplyBatch.supplyBatchId");
		query.setParameter("bid", id);
		
		if(query.list().size()>0)
		{
			chicks = (Long) query.list().get(0);
		}
		return chicks;
	}
	
	
	public List<FieldReportReport> getFieldReport(Long bid)
	{
		Session session = em.unwrap(Session.class);
		
		String sql = "SELECT f.date,f.FCR FROM FieldReport f WHERE f.supplyBatch = "+bid+" ORDER BY f.date";
		
		Query query = session.createQuery(sql);
		
		List<FieldReportReport> report = query.list();
		
		return report;
		
	}

}
