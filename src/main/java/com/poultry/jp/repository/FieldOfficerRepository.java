package com.poultry.jp.repository;

import java.util.List;

import com.poultry.jp.model.FieldOfficer;
import com.poultry.jp.report.FieldOfficerReport;

public interface FieldOfficerRepository {
	
	public FieldOfficer save(FieldOfficer fieldOfficer);

	public List<FieldOfficerReport> getFieldOfficerReport();

	public FieldOfficer getFieldOfficerObject(Long id);
	
	public List<FieldOfficerReport> getAllFieldOfficers();

	public FieldOfficer updateFieldOfficer(FieldOfficer fieldOfficer);

	public FieldOfficer delete(Long id);

}
