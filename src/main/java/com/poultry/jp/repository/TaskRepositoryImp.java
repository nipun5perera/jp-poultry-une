package com.poultry.jp.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.poultry.jp.report.Catching_TaskReport;
import com.poultry.jp.model.FeedIssue_Task;
import com.poultry.jp.report.FeedIssue_TaskReport;
import com.poultry.jp.model.Task;
import com.poultry.jp.report.TaskReport;

@Repository("TaskRepository")
public class TaskRepositoryImp implements TaskRepository {

	@PersistenceContext
	private EntityManager em;
	
	public List<TaskReport> getTaskReport() {
		 
//		TypedQuery<TaskReport> query = em.createNamedQuery(Task.FINDTASKBYPENDINGSTATUS, TaskReport.class);
		String sql = "FROM Task t WHERE t.status = 'Pending' OR t.status='Incomplete' ORDER BY t.duedate ";
		Session session = em.unwrap(Session.class);
		Query query = session.createQuery(sql);
		
		@SuppressWarnings("unchecked")
		List<TaskReport> rep = query.list();
		System.out.println("Report size"+rep.size());
		return rep;
	}
	
	public Task getSelectedTask(String tid)
	{
		Task task = new Task();
		
		Session session = em.unwrap(Session.class);
		
		task = (Task) session.get(Task.class, new Long(tid));
		
		System.out.println(task.getTaskId()+"******************************************");
		return task;
		
	}
	
//	public Task updateTaskStatus1(Task pTask)
//	{
//		
//		
		
//		task.setPerformedDate(date);
//		if(task.getQuantity() <= quantity )
//		{
//			task.setStatus("Completed");
//		}
//		else
//		{
//			task.setStatus("Incomplete");
//		}
//		
//		
//		
//		em.merge(task);
//		
//		return task;
//	}

	public List<Task> getTasksByDate(Date dat) {
	 
		System.out.println("Runnin");
		String sql = "FROM Task  t WHERE (t.duedate <= :date AND (t.status = 'incomplete' or t.status= 'Pending')) ORDER BY t.duedate";
		Session session = em.unwrap(Session.class);
		Query query = session.createQuery(sql);
		query.setParameter("date", dat);
		System.out.println("SQL-"+sql);
		
		@SuppressWarnings("unchecked")
		List<Task> taskList = query.list();
		
		
		
		return taskList;
	}
	
	public List<FeedIssue_TaskReport> getFeedIssueTasks() {
		 
/*		String sql = "select t from FeedIssue_Task t where t.status ='Pending' or t.status='Incomplete' order by t.duedate ";
		Session session = em.unwrap(Session.class);
		Query query = session.createQuery(sql);
*/	 

		TypedQuery<FeedIssue_TaskReport> query = em.createNamedQuery(FeedIssue_Task.FINDFEEDISSUETASKS,FeedIssue_TaskReport.class);
		List<FeedIssue_TaskReport> taskList = query.getResultList();
		
//		for(int i=0 ; i < 10;i++)
//		{
//			System.out.println("Farmer="+taskList.get(i).getSupplyBatch());
////			System.out.println("Distribute Batch ID="+taskList.get(i).getSupplyBatch().getSupplyBatchId());
//		}
//		
		System.out.println("Size"+taskList.size());
		
		
		
		return taskList;
	}
	
	public List<FeedIssue_TaskReport> getFeedIssueTasksByDate(Date dat) {
		 
/*		System.out.println("Came Here");
		String sql = "select t from Task t where  t.type= 'Feed Issuing' and t.duedate = :date order by t.duedate "; 
		
		Session session = em.unwrap(Session.class);
		Query query = session.createQuery(sql);
		query.setParameter("date", dat);
		
		System.out.println("Size"+query.list().size());
		
		@SuppressWarnings("unchecked")
		List<Task> taskList = query.list();*/
		
		/*for(Task t : taskList)
		{
			System.out.println("farmer Name "+t.getSupplyBatch().getFarmer().getName());
		}*/
		
		
		TypedQuery<FeedIssue_TaskReport> query1 = em.createNamedQuery(FeedIssue_Task.FINDFEEDISSUETASKBYDATE,FeedIssue_TaskReport.class);
		query1.setParameter("date", dat);
		
		return query1.getResultList();
	}

	public Task updateTaskStatus(Task pTask) {
		Session session = em.unwrap(Session.class);

		Task task = (Task) session.get(Task.class, pTask.getTaskId());

		task.setStatus(pTask.getStatus());
		
		return task;
	}

	public List<Catching_TaskReport> getCatchingTasks() {
//		String sql = "select t from Task t where t.status ='Pending' AND  t.type='Catching' order by t.duedate ";
//		Session session = em.unwrap(Session.class);
//		Query query = session.createQuery(sql);
//	 
//		
//		@SuppressWarnings("unchecked")
//		List<Task> taskList = query.list();
		
		TypedQuery<Catching_TaskReport> query = em.createNamedQuery(Task.FINDCATCHINGTASKS,Catching_TaskReport.class);
		
		
		
		return query.getResultList();
	}

	public List<TaskReport> getCatchingTasksByDate(Date date) {
		
		/*String sql = "FROM Task t WHERE t.duedate= :date AND t.type = 'Catching' AND (t.status = 'Pending' OR t.status = 'incomplete' )ORDER BY t.duedate";
		Session session = em.unwrap(Session.class);
		Query query = session.createQuery(sql);
		query.setParameter("date", date);
		
		@SuppressWarnings("unchecked")
		List<Task> taskList = query.list();
		*/
		
		TypedQuery<TaskReport> query1 = em.createNamedQuery(Task.FINDCATCHINGTASKSBYDATE,TaskReport.class);
		query1.setParameter("date", date);
		return query1.getResultList();
	}

}
