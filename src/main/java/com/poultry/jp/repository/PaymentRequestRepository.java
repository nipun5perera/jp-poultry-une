package com.poultry.jp.repository;

import java.util.List;

import com.poultry.jp.model.PaymentRequest;
import com.poultry.jp.report.PaymentRequestReport;

public interface PaymentRequestRepository {
	
	public PaymentRequest save(PaymentRequest paymentRequest);
	
	public List<PaymentRequestReport> getPaymentRequestDates();
	public PaymentRequestReport getPaymentRequest(Long id);

	public PaymentRequest getPaymentRequestObject(Long id);

	public List<PaymentRequestReport> getPaymentRequestReport();

	public PaymentRequest deletePaymentRequest(Long id);

	public PaymentRequest updatePaymentRequest(PaymentRequest paymentRequest);

	public PaymentRequest PaymentRequest(Long id);

	public PaymentRequest setPaymentRequestIsPaidStatus(Long id);

	public PaymentRequest approvePaymentRequest(PaymentRequest paymentRequest);

}
