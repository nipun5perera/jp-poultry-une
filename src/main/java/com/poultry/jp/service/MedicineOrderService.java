/**
 * 
 */
package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.MedicineOrder;
import com.poultry.jp.report.MedicineOrderReport;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
public interface MedicineOrderService {
	
	public MedicineOrder save(MedicineOrder medicineOrder);

	public List<MedicineOrderReport> getMedicineOrderDetails();

	public MedicineOrder updateMedicine(MedicineOrder medicineOrder);

	public MedicineOrder getMedicineOrder(Long id);

	public MedicineOrder delete(Long id);
	
	public MedicineOrder updateMedicineOrderInstock(Long medOrderID);
	
}
