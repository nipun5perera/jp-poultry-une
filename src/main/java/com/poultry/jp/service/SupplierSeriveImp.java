package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Supplier;
import com.poultry.jp.report.SupplierReport;
import com.poultry.jp.repository.SupplierRepository;

@Service
public class SupplierSeriveImp implements SupplierService {

	
	@Autowired
	private SupplierRepository supplierRepository;
	
	@Transactional
	public Supplier save(Supplier supplier) {
		
		supplierRepository.save(supplier);
		
		return supplier;
	}

	public List<SupplierReport> getSupplierNames() {
		 
		return supplierRepository.getSupplierNames();
	}

	public List<SupplierReport> getSupplierDetails() {

		return supplierRepository.getSupplierDetails();
	}

	@Transactional(readOnly=true)
	public Supplier getSupplierObject(Long id) {
		 
		return supplierRepository.getSupplierObject(id);
	}

	@Transactional
	public Supplier update(Supplier supplier) {
		return supplierRepository.update(supplier);
	}

	@Transactional
	public Supplier delete(Long id) {
		return supplierRepository.delete(id);
	}

}
