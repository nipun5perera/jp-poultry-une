/**
 * 
 */
package com.poultry.jp.service;

import com.poultry.jp.model.MedicineOrderDetails;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
public interface MedicineOrderDetailsService {
	
	public MedicineOrderDetails save(MedicineOrderDetails medicineOrderDetails);
	
}
