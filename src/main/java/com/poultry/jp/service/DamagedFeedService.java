package com.poultry.jp.service;

import com.poultry.jp.model.Damaged_Feed;

public interface DamagedFeedService {
	
	public Damaged_Feed saveDamagedFeed(Damaged_Feed damagedFeed);
}
