/**
 * 
 */
package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Farmer;
import com.poultry.jp.report.FarmerReport;
import com.poultry.jp.repository.FarmerRepository;

/**
 * @author Nipun Perera
 * @created 24 Jul 2015
 *
 */

@Service
public class FarmerServiceImp implements FarmerService{

	@Autowired
	private FarmerRepository farmerRepository;
	
	@Transactional
	public Farmer save(Farmer farmer){
		
		farmerRepository.save(farmer);
		return farmer;
	}

	public List<FarmerReport> getFarmerReport() {
		 
		return farmerRepository.getFarmerReport();
	}

	public List<FarmerReport> getFamerDetails() {
		return farmerRepository.getFarmerDetails();
	}

	@Transactional
	public Farmer getFarmer(long id) {
		return farmerRepository.getFarmer(id);
	}
	@Transactional
	public Farmer update(Farmer farmer) {
		return farmerRepository.update(farmer);
	}

	@Transactional
	public Farmer delete(Long fid) {
		return farmerRepository.delete(fid);
	}
	
	@Transactional
	public List<FarmerReport> getAvailableFamers() {
		return farmerRepository.getAvailableFarmers();
	}
}
