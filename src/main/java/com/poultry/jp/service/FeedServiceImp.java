package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Feed;
import com.poultry.jp.report.FeedReport;
import com.poultry.jp.repository.FeedRepository;

@Service
public class FeedServiceImp implements FeedService {

	@Autowired
	private FeedRepository feedRepository;
	
	@Transactional
	public Feed save(Feed feed) {
		 
		feedRepository.Save(feed);
		return feed;
	}

	public List<FeedReport> getFeedDetails() {
		return feedRepository.getFeedDetails();
	}

	@Transactional
	public Feed getFeed(Long id) {
		return feedRepository.getFeed(id);
	}

	@Transactional
	public Feed update(Feed feed) {
		return feedRepository.update(feed);
	}

	@Transactional
	public Feed delete(Long id) {
		return feedRepository.delete(id);
	}

}
