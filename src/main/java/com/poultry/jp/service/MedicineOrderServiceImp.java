/**
 * 
 */
package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.MedicineOrder;
import com.poultry.jp.report.MedicineOrderReport;
import com.poultry.jp.repository.MedicineOrderRepository;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
@Service
public class MedicineOrderServiceImp implements MedicineOrderService{

	@Autowired
	private MedicineOrderRepository medicineOrderRepository;
	
	@Transactional
	public MedicineOrder save(MedicineOrder medicineOrder){
		
		medicineOrderRepository.save(medicineOrder);
		return medicineOrder;
	}

	@Transactional
	public List<MedicineOrderReport> getMedicineOrderDetails() {
		 
		return medicineOrderRepository.getMedicineOrderDetails();
	}

	@Transactional
	public MedicineOrder updateMedicine(MedicineOrder medicineOrder) {
		 
		return medicineOrderRepository.updateMedicineOrder(medicineOrder);
	}
	
	@Transactional
	public MedicineOrder getMedicineOrder(Long id) {
		return medicineOrderRepository.getMedcineOrder(id);
	}

	@Transactional
	public MedicineOrder delete(Long id) {
		return medicineOrderRepository.delete(id);
	}

	@Transactional
	public MedicineOrder updateMedicineOrderInstock(Long medOrderID) {
		return medicineOrderRepository.updateMedicineOrderInstock(medOrderID);
	}
}
