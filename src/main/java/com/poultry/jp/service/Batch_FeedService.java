package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.Batch_Feed;
import com.poultry.jp.report.Batch_FeedReport;

public interface Batch_FeedService {
	
	public Batch_Feed save(Batch_Feed batchFeed);

	public List<Batch_FeedReport> getBatchFeeds(Long id);
	
	public List<Batch_FeedReport> getAllBatchFeeds() ;

}
