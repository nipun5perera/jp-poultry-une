package com.poultry.jp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Damaged_Feed;
import com.poultry.jp.repository.DamagedFeedRepository;

@Service
public class DamagedFeedServiceImp implements DamagedFeedService {

	@Autowired
	private DamagedFeedRepository damagedFeedRepository;
	
	@Transactional
	public Damaged_Feed saveDamagedFeed(Damaged_Feed damagedFeed) {
		return damagedFeedRepository.saveDamagedFeed(damagedFeed);
	}

}
