package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.Medicine;
import com.poultry.jp.report.MedicineReport;

public interface MedicineService {
	
	public Medicine save(Medicine medicine);

	public List<MedicineReport> getMedicineDetails();

	public Medicine getMedicine(Long id);

	public Medicine update(Medicine medicine);

	public Medicine delete(Long id);

}
