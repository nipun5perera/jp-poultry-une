package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.FieldReport;
import com.poultry.jp.report.FieldReportReport;

public interface FieldReportService {
	
	public FieldReport save(FieldReport fieldReport);
	
	public long getNumOfChicksDied(Long id);
	
	public List<FieldReportReport> getFieldReport(Long bid);

}
