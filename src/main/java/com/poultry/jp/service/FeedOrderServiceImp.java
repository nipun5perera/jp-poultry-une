package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.FeedOrder;
import com.poultry.jp.report.FeedOrderReport;
import com.poultry.jp.repository.FeedOrderRepository;

@Service
public class FeedOrderServiceImp implements FeedOrderService {

	@Autowired
	private FeedOrderRepository feedOrderRepository;
	
	@Transactional
	public FeedOrder save(FeedOrder feedOrder) {
		
		feedOrderRepository.save(feedOrder);
		return feedOrder;
	}

	@Transactional
	public List<FeedOrderReport> getFeedOrderDetails() {
		 
		return feedOrderRepository.getFeedOrderDetails();
	}

	@Transactional
	public FeedOrder updateFeedOrder(FeedOrder feedOrder) {
		return feedOrderRepository.updateFeedOrder(feedOrder);
		
	}
	
	@Transactional
	public FeedOrder getFeedOrder(Long id) {
		return feedOrderRepository.getFeedOrder(id);
	}

	@Transactional
	public FeedOrder delete(Long id) {
		return feedOrderRepository.delete(id);
	}

	@Transactional
	public FeedOrder updateFeedOrderInStockStatus(Long feedOrderId) {
		 
		return feedOrderRepository.updateFeedOrderInStockStatus(feedOrderId);
	}

	@Transactional
	public FeedOrder approveFeedOrder(FeedOrder feedOrder) {
		return feedOrderRepository.approveFeedOrder(feedOrder);
	}
	

}
