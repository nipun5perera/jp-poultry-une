package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Batch_Medicine;
import com.poultry.jp.report.Batch_MedicineReport;
import com.poultry.jp.repository.Batch_MedicineRepository;

@Service
public class Batch_MedicineServiceImp implements Batch_MedicineService {

	@Autowired
	private Batch_MedicineRepository batchMedicineRepository;
	
	@Transactional
	public Batch_Medicine save(Batch_Medicine batchMedicine) {

		batchMedicineRepository.save(batchMedicine);
		
		return batchMedicine;
	}

	public List<Batch_MedicineReport> getBatchMedicines(Long id) {
		 
		return batchMedicineRepository.getBatchMedicines(id);
	}

}
