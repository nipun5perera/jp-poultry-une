package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.SupplyBatch;
import com.poultry.jp.report.SupplyBatchReport;
import com.poultry.jp.repository.SupplyBatchRepository;

@Service
public class SupplyBatchServiceImp implements SupplyBatchService{
	
	@Autowired
	private SupplyBatchRepository supplyBatchRepository;
	
	@Transactional
	public SupplyBatch save(SupplyBatch supplyBatch){
		supplyBatchRepository.save(supplyBatch);
		return supplyBatch;
	}

	public List<SupplyBatchReport> getSupplyBatches() {
		return supplyBatchRepository.getSupplyBatches();
	}

	@Transactional
	public SupplyBatch deleteSupplyBatch(Long id) {
		return supplyBatchRepository.deleteSupplyBatch(id);
	}

	@Transactional
	public SupplyBatch getSupplyBatch(Long id) {
		return supplyBatchRepository.getSupplyBatch(id);
	}
	
	@Transactional
	public SupplyBatch updateSupplyBatch(SupplyBatch supplyBatch) {
		return supplyBatchRepository.updateSupplyBatch(supplyBatch);
	}
}
