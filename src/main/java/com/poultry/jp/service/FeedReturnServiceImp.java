package com.poultry.jp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.FeedReturn;
import com.poultry.jp.repository.FeedReturnRepository;

@Service
public class FeedReturnServiceImp implements FeedReturnService {
	
	@Autowired
	private FeedReturnRepository feedReturnRepository;
	
	@Transactional
	public FeedReturn save(FeedReturn feedReturn)
	{
		feedReturnRepository.save(feedReturn);
		return feedReturn;
		
	}

}
