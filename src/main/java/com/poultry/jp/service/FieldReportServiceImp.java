package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.FieldReport;
import com.poultry.jp.report.FieldReportReport;
import com.poultry.jp.repository.FieldReportRepository;

@Service
public class FieldReportServiceImp implements FieldReportService {

	@Autowired
	private FieldReportRepository fieldReportRepository;
	
	@Transactional
	public FieldReport save(FieldReport fieldReport) {
		fieldReportRepository.save(fieldReport);
		return fieldReport;
	}

	@Transactional
	public long getNumOfChicksDied(Long id) {
		return fieldReportRepository.getNumOfChicksDied(id);
	}

	@Transactional
	public List<FieldReportReport> getFieldReport(Long bid) {
		return fieldReportRepository.getFieldReport(bid);
	}

}
