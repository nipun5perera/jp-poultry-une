package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Batch_Feed;
import com.poultry.jp.report.Batch_FeedReport;
import com.poultry.jp.repository.Batch_FeedRepository;

@Service
public class Batch_FeedServiceImp implements Batch_FeedService {

	
	@Autowired
	private Batch_FeedRepository batchFeedRepository;
	
	@Transactional
	public Batch_Feed save(Batch_Feed batchFeed) {
		
		batchFeedRepository.save(batchFeed);
		
		return batchFeed;
	}

	public List<Batch_FeedReport> getBatchFeeds(Long id) {
		return batchFeedRepository.getBatchFeeds(id);
	}

	public List<Batch_FeedReport> getAllBatchFeeds() {
		return batchFeedRepository.getAllBatchFeeds();
	}

	 

}
