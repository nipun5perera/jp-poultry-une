package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Medicine;
import com.poultry.jp.report.MedicineReport;
import com.poultry.jp.repository.MedicineRepository;

@Service
public class MedicineServiceImp implements MedicineService {
	
	@Autowired
	private MedicineRepository medicineRepository;

	@Transactional
	public Medicine save(Medicine medicine) {
		
		medicineRepository.save(medicine);
		return medicine;
	}

	public List<MedicineReport> getMedicineDetails() {
		
		return medicineRepository.getMedicineDetails();

	}

	@Transactional
	public Medicine getMedicine(Long id) {
		 
		return medicineRepository.getMedicine(id);
	}

	@Transactional
	public Medicine update(Medicine medicine) {
		return medicineRepository.update(medicine);
	}

	@Transactional
	public Medicine delete(Long id) {
		
		return medicineRepository.delete(id);
	}

}
