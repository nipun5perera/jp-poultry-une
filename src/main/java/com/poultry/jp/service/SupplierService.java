package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.Supplier;
import com.poultry.jp.report.SupplierReport;

public interface SupplierService {
	
	public Supplier save(Supplier supplier);

	public List<SupplierReport> getSupplierNames();

	public List<SupplierReport> getSupplierDetails();

	public Supplier getSupplierObject(Long id);

	public Supplier update(Supplier supplier);

	public Supplier delete(Long id);

	 

}
