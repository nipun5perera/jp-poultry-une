package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.Feed;
import com.poultry.jp.report.FeedReport;

public interface FeedService {
	
	public Feed save(Feed feed);

	public List<FeedReport> getFeedDetails();

	public Feed getFeed(Long id);

	public Feed update(Feed feed);

	public Feed delete(Long id);

}
