package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.FieldOfficer;
import com.poultry.jp.report.FieldOfficerReport;

public interface FieldOfficerService {

	public FieldOfficer save(FieldOfficer fieldOfficer);

	public List<FieldOfficerReport> getFieldOfficerReport();

	public FieldOfficer getFieldOfficerObject(Long id);
	
	public List<FieldOfficerReport> getAllFieldOfficers();

	public FieldOfficer update(FieldOfficer fieldOfficer);

	public FieldOfficer delete(Long id);
}
