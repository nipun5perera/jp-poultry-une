package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.FieldOfficer;
import com.poultry.jp.report.FieldOfficerReport;
import com.poultry.jp.repository.FieldOfficerRepository;

@Service
public class FieldOfficerServiceImp implements FieldOfficerService {

	@Autowired
	private FieldOfficerRepository fieldOfficerRepository;
	
	@Transactional
	public FieldOfficer save(FieldOfficer fieldOfficer) {
		
		fieldOfficerRepository.save(fieldOfficer);
		
		return fieldOfficer;
	}

	public List<FieldOfficerReport> getFieldOfficerReport() {
		 
		return fieldOfficerRepository.getFieldOfficerReport();
	}

	@Transactional
	public FieldOfficer getFieldOfficerObject(Long id) {
		FieldOfficer fieldOfficer = fieldOfficerRepository.getFieldOfficerObject(id);
		return fieldOfficer;
	}

	public List<FieldOfficerReport> getAllFieldOfficers() {
		return fieldOfficerRepository.getAllFieldOfficers();
	}

	@Transactional
	public FieldOfficer update(FieldOfficer fieldOfficer) {
		return fieldOfficerRepository.updateFieldOfficer(fieldOfficer);
	}

	@Transactional
	public FieldOfficer delete(Long id) {
		return fieldOfficerRepository.delete(id);
	}

}
