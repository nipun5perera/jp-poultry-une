/**
 * 
 */
package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.Farmer;
import com.poultry.jp.report.FarmerReport;

/**
 * @author Nipun Perera
 * @created 24 Jul 2015
 *
 */

public interface FarmerService {
	
	public Farmer save(Farmer farmer);

	public List<FarmerReport> getFarmerReport();
	
	public List<FarmerReport> getAvailableFamers();

	public List<FarmerReport> getFamerDetails();

	public Farmer getFarmer(long attribute);

	public Farmer update(Farmer farmer);

	public Farmer delete(Long fid);
	
	
	
}
