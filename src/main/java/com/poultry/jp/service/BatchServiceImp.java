/**
 * 
 */
package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Batch;
import com.poultry.jp.report.BatchReport;
import com.poultry.jp.repository.BatchRepository;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
@Service
public class BatchServiceImp implements BatchService{

	@Autowired
	private BatchRepository batchRepository;
	
	@Transactional
	public Batch save(Batch batch){
		
		batchRepository.save(batch);
		return batch;
	}

	public List<BatchReport> getBatchReport() {
		List<BatchReport> batchReport = batchRepository.getBatchReport();
		return batchReport;
	}
	
	@Transactional
	public Batch getBatchObject(Long id) {
		 
		return batchRepository.getBatchObject(id);
	}

	@Transactional
	public List<BatchReport> getBatchDatesAndIds() {
		return batchRepository.getBatchDatesAndIds();
	}

	@Transactional
	public Batch approveBatch(Batch pbatch) {
		 
		return batchRepository.approveBatch(pbatch);
	}
	
	

}
