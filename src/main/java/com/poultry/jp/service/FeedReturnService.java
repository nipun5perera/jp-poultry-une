package com.poultry.jp.service;

import com.poultry.jp.model.FeedReturn;

public interface FeedReturnService {
	
	public FeedReturn save(FeedReturn feedReturn);

}
