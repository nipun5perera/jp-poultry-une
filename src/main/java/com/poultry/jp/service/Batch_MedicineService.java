package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.Batch_Medicine;
import com.poultry.jp.report.Batch_MedicineReport;

public interface Batch_MedicineService {
	
	public Batch_Medicine save(Batch_Medicine batchMedicine);

	public List<Batch_MedicineReport> getBatchMedicines(Long id);

}
