package com.poultry.jp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.PaymentRequest;
import com.poultry.jp.report.PaymentRequestReport;
import com.poultry.jp.repository.PaymentRequestRepository;

@Service
public class PaymentRequestServiceImp implements PaymentRequestService {

	@Autowired
	private PaymentRequestRepository paymentRequestRepository;
	
	@Transactional
	public PaymentRequest save(PaymentRequest paymentRequest) {
		
		paymentRequestRepository.save(paymentRequest);
		return paymentRequest;
	}

	public List<PaymentRequestReport> getPaymentRequestDates() {
		return paymentRequestRepository.getPaymentRequestDates();
	}

	public PaymentRequestReport getPaymentRequest(Long id) {
		
		PaymentRequestReport paymentRequest = paymentRequestRepository.getPaymentRequest(id);
		return paymentRequest;
	}

	@Transactional
	public PaymentRequest getPaymentRequestObject(Long id) {
		PaymentRequest paymentRequest = paymentRequestRepository.getPaymentRequestObject(id);
		return paymentRequest;
	}

	public List<PaymentRequestReport> getPaymentRequestReport() {
		return paymentRequestRepository.getPaymentRequestReport();
	}
	
	@Transactional
	public PaymentRequest deletePaymentRequest(Long id) {
		 
		return paymentRequestRepository.deletePaymentRequest(id);
	}
	
	@Transactional
	public PaymentRequest updatePaymentRequest(PaymentRequest paymentRequest) {
		return paymentRequestRepository.updatePaymentRequest(paymentRequest);
	}

	@Transactional
	public PaymentRequest approvePaymentRequest(Long id) {
		return paymentRequestRepository.PaymentRequest(id);
	}

	@Transactional
	public PaymentRequest setPaymentRequestIsPaidStatus(Long id) {
		 
		return paymentRequestRepository.setPaymentRequestIsPaidStatus(id);
	}

	@Transactional
	public PaymentRequest approvePaymentRequest(PaymentRequest paymentRequest) {
		return paymentRequestRepository.approvePaymentRequest(paymentRequest);
	}

}
