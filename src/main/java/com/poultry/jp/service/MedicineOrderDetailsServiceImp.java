/**
 * 
 */
package com.poultry.jp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.MedicineOrderDetails;
import com.poultry.jp.repository.MedicineOrderDetailsRepository;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
@Service
public class MedicineOrderDetailsServiceImp implements MedicineOrderDetailsService{
	
	@Autowired
	private MedicineOrderDetailsRepository medicineOrderDetailsRepository;
	
	@Transactional
	public MedicineOrderDetails save(MedicineOrderDetails medicineOrderDetails){
		
		medicineOrderDetailsRepository.save(medicineOrderDetails);
		return medicineOrderDetails;
	}
}
