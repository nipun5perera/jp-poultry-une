package com.poultry.jp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.poultry.jp.model.FeedOrder;
import com.poultry.jp.report.FeedOrderReport;

@Service public interface FeedOrderService {
	
	public FeedOrder save(FeedOrder feedOrder);

	public List<FeedOrderReport> getFeedOrderDetails();

	public FeedOrder updateFeedOrder(FeedOrder feedOrder);

	public FeedOrder getFeedOrder(Long id);

	public FeedOrder delete(Long id);
	
	public FeedOrder updateFeedOrderInStockStatus(Long feedOrderId);
	
	public FeedOrder approveFeedOrder(FeedOrder feedOrder);

}
