/**
 * 
 */
package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.Batch;
import com.poultry.jp.report.BatchReport;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
public interface BatchService {

	public Batch save(Batch batch);

	public List<BatchReport> getBatchReport();
	
	public Batch getBatchObject(Long id);

	public List<BatchReport> getBatchDatesAndIds();

	public Batch approveBatch(Batch pbatch);
}
