package com.poultry.jp.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.report.Catching_TaskReport;
import com.poultry.jp.report.FeedIssue_TaskReport;
import com.poultry.jp.model.Task;
import com.poultry.jp.report.TaskReport;
import com.poultry.jp.repository.TaskRepository;

@Service
public class TaskServiceImp implements TaskService {

	@Autowired
	private TaskRepository taskRepository;
	
	
	@Transactional
	public List<TaskReport> getTaskReport() {
		 
		return taskRepository.getTaskReport();
	}

	@Transactional
	public Task getSelectedTask(String tid) {
		return taskRepository.getSelectedTask(tid);
	}
	@Transactional
	public Task updateTaskStatus(Task task) {
		return taskRepository.updateTaskStatus(task);
	}

	@Transactional
	public List<Task> getTasksByDate(Date dat) {
		 
		return taskRepository.getTasksByDate(dat);
	}

	@Transactional
	public List<FeedIssue_TaskReport> getFeedIssueTasks() {
		// TODO Auto-generated method stub
		return   taskRepository.getFeedIssueTasks();
	}

	@Transactional
	public List<Catching_TaskReport> getCatchingTasks() {
		return taskRepository.getCatchingTasks();
	}

	@Transactional
	public List<TaskReport> getCatchingTasksByDate(Date date) {
		// TODO Auto-generated method stub
		return taskRepository.getCatchingTasksByDate(date);
	}

	@Transactional
	public List<FeedIssue_TaskReport> getFeedIssueTasksByDate(Date dat)
	{
		return taskRepository.getFeedIssueTasksByDate(dat);
	}
//	@Transactional
//	public Task updateTaskStatus(Task task) {
//		return taskRepository.updateTaskStatus(task);
//		
//	}

}
