package com.poultry.jp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.poultry.jp.model.Catching;
import com.poultry.jp.repository.CatchingRepository;

@Service
public class CatchingServiceImp implements CatchingService {

	@Autowired
	private CatchingRepository catchingRepo;
	
	@Transactional
	public Catching save(Catching catching) {
		catchingRepo.save(catching); 
		return catching;
	}

}
