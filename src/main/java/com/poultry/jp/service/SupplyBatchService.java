/**
 * 
 */
package com.poultry.jp.service;

import java.util.List;

import com.poultry.jp.model.SupplyBatch;
import com.poultry.jp.report.SupplyBatchReport;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
public interface SupplyBatchService {
	
	public SupplyBatch save(SupplyBatch supplyBatch);

	public List<SupplyBatchReport> getSupplyBatches();

	public SupplyBatch deleteSupplyBatch(Long id);

	public SupplyBatch getSupplyBatch(Long id);

	public SupplyBatch updateSupplyBatch(SupplyBatch supplyBatch);

	
}
