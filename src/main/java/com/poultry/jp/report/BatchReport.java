package com.poultry.jp.report;

import com.poultry.jp.model.PaymentRequest;
import com.poultry.jp.model.Supplier;

import java.util.Date;

public class BatchReport {

	private Long batchId;
	private int quantity;
	private double chickPrice;
	private double purchaseOrderValue;
	private double actualOrderValue;
	private double discount;
	private double waxingTotal;
	private double oneChickWaxingCost;
	private boolean waxingStatus;
	private Supplier supplier;
	private PaymentRequest paymentRequest;
	private Date date;
	private boolean isApproved;

	public BatchReport(Long batchId, Date date, int quantity,
			double chickPrice, double purchaseOrderValue,
			double actualOrderValue, double discount, double waxingTotal,
			double oneChickWaxingCost, boolean waxingStatus, Supplier supplier,
			PaymentRequest paymentRequest) {
		super();
		this.batchId = batchId;
		this.quantity = quantity;
		this.chickPrice = chickPrice;
		this.purchaseOrderValue = purchaseOrderValue;
		this.actualOrderValue = actualOrderValue;
		this.discount = discount;
		this.waxingTotal = waxingTotal;
		this.oneChickWaxingCost = oneChickWaxingCost;
		this.waxingStatus = waxingStatus;
		this.supplier = supplier;
		this.paymentRequest = paymentRequest;
		this.date = date;
	}
	
	public BatchReport(Long batchId, Date date, int quantity,
			double chickPrice, double purchaseOrderValue,
			double actualOrderValue, double discount, double waxingTotal,
			double oneChickWaxingCost, boolean waxingStatus, Supplier supplier,
			PaymentRequest paymentRequest,boolean isApproved) {
		super();
		this.batchId = batchId;
		this.quantity = quantity;
		this.chickPrice = chickPrice;
		this.purchaseOrderValue = purchaseOrderValue;
		this.actualOrderValue = actualOrderValue;
		this.discount = discount;
		this.waxingTotal = waxingTotal;
		this.oneChickWaxingCost = oneChickWaxingCost;
		this.waxingStatus = waxingStatus;
		this.supplier = supplier;
		this.paymentRequest = paymentRequest;
		this.date = date;
		this.isApproved = isApproved;
	}

	
	public BatchReport(Long batchId, Date date) {
		super();
		this.batchId = batchId;
		this.date = date;
	}


	public PaymentRequest getPaymentRequest() {
		return paymentRequest;
	}

	public void setPaymentRequest(PaymentRequest paymentRequest) {
		this.paymentRequest = paymentRequest;
	}

	public double getActualOrderValue() {
		return actualOrderValue;
	};

	public double getChickPrice() {
		return chickPrice;
	}

	public double getDiscount() {
		return discount;
	}

	public double getOneChickWaxingCost() {
		return oneChickWaxingCost;
	}

	public double getPurchaseOrderValue() {
		return purchaseOrderValue;
	}

	public boolean getWaxingStatus() {
		return waxingStatus;
	}

	public double getWaxingTotal() {
		return waxingTotal;
	}

	public void setActualOrderValue(double actualOrderValue) {
		this.actualOrderValue = actualOrderValue;
	}

	public void setChickPrice(double chickPrice) {
		this.chickPrice = chickPrice;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public void setOneChickWaxingCost(double oneChickWaxingCost) {
		this.oneChickWaxingCost = oneChickWaxingCost;
	}

	public void setPurchaseOrderValue(double purchaseOrderValue) {
		this.purchaseOrderValue = purchaseOrderValue;
	}

	public void setWaxingStatus(boolean waxingStatus) {
		this.waxingStatus = waxingStatus;
	}

	public void setWaxingTotal(double waxingTotal) {
		this.waxingTotal = waxingTotal;
	}

	public Long getBatchId() {
		return batchId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getPurchaseBatchID()
	{
		return "CP-"+batchId;
	}


	public boolean isApproved() {
		return isApproved;
	}


	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

}
