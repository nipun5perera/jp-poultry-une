package com.poultry.jp.report;

import java.util.Date;

public class TaskReport {
	
	private Long taskId;
	private String status;
	private Date duedate;
	private Date performedDate;
	private String type;
	private SupplyBatch supplyBatch;
	
	public TaskReport(Long taskId, String status, Date duedate,
			    String type) {
		super();
		this.taskId = taskId;
		this.status = status;
		this.duedate = duedate;
		this.type = type;
	}

	public TaskReport(Long taskId, String status, Date duedate,
			 SupplyBatch supplyBatch, String type) {
		super();
		this.taskId = taskId;
		this.status = status;
		this.duedate = duedate;
		this.type = type;
		this.supplyBatch = supplyBatch;
	}
	
	public TaskReport(Long taskId, String status, Date duedate,
			Date performedDate, String type) {
		super();
		this.taskId = taskId;
		this.status = status;
		this.duedate = duedate;
		this.performedDate = performedDate;
		this.type = type;
		
	}
	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDuedate() {
		return duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public Date getPerformedDate() {
		return performedDate;
	}

	public void setPerformedDate(Date performedDate) {
		this.performedDate = performedDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}

	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}
	
	public String getDistributeBatchID()
	{
		return "DB-"+supplyBatch.getSupplyBatchId();
	}
	
	
}
