package com.poultry.jp.report;

public class FieldReportReport {
	
	private Long fieldReportId;
	private double amountAte;
	private double FCR;
	private double averageWeight;
	private int numOfChicksDied;
	
	public Long getFieldReportId() {
		return fieldReportId;
	}
	public void setFieldReportId(Long fieldReportId) {
		this.fieldReportId = fieldReportId;
	}
	public double getAmountAte() {
		return amountAte;
	}
	public void setAmountAte(double amountAte) {
		this.amountAte = amountAte;
	}
	public double getFCR() {
		return FCR;
	}
	public void setFCR(double fCR) {
		FCR = fCR;
	}
	public double getAverageWeight() {
		return averageWeight;
	}
	public void setAverageWeight(double averageWeight) {
		this.averageWeight = averageWeight;
	}
	public int getNumOfChicksDied() {
		return numOfChicksDied;
	}
	public void setNumOfChicksDied(int numOfChicksDied) {
		this.numOfChicksDied = numOfChicksDied;
	}
}
