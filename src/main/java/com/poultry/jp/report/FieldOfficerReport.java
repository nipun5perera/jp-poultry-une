package com.poultry.jp.report;

public class FieldOfficerReport {

	private Long fieldOfficerId;
	private String fname;
	private String lname;
	private String mname;
	private String contact;
	private String NIC;
	
	
	public FieldOfficerReport(Long fieldOfficerId, String fname, String lname,String mname) {
		super();
		this.fieldOfficerId = fieldOfficerId;
		this.fname = fname;
		this.lname = lname;
		this.mname = mname;
	}
	
	

	public FieldOfficerReport(Long fieldOfficerId, String fname, String lname,
			String mname, String contact, String NIC) {
		super();
		this.fieldOfficerId = fieldOfficerId;
		this.fname = fname;
		this.lname = lname;
		this.mname = mname;
		this.contact = contact;
		this.NIC = NIC;
	}



	public String getMname() {
		return mname;
	}



	public void setMname(String mname) {
		this.mname = mname;
	}



	public String getContact() {
		return contact;
	}



	public void setContact(String contact) {
		this.contact = contact;
	}



	public String getNIC() {
		return NIC;
	}



	public void setNIC(String nIC) {
		NIC = nIC;
	}



	public Long getFieldOfficerId() {
		return fieldOfficerId;
	}

	public void setFieldOfficerId(Long fieldOfficerId) {
		this.fieldOfficerId = fieldOfficerId;
	}

	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getFieldOfficerFullName()
	{
		return fname+" "+mname+" "+lname;
	}
}
