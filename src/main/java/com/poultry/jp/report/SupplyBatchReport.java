package com.poultry.jp.report;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class SupplyBatchReport {
	
	private Batch batch;
	
	private FieldOfficer fieldOfficer;
	private Long supplyBatchId;
	private int quantity;
	private Date date;
	private Farmer farmer;
	private Catching catching;
	private Set<Task> task = new HashSet<Task>();
	private Set<Batch_Medicine> batchMedicine= new HashSet<Batch_Medicine>();
	private Set<Batch_Feed> batchFeed = new HashSet<Batch_Feed>();
	private Set<FieldReport> fieldReport = new HashSet<FieldReport>();
	private Set<FeedReturn> feedReturn = new HashSet<FeedReturn>();
	private boolean isCaught;
	private boolean isDeleted;
	private Date created;
	private Date updated;
	
	public SupplyBatchReport(Batch batch, FieldOfficer fieldOfficer,
			Long supplyBatchId, int quantity, Date date, Farmer farmer) {
		super();
		this.batch = batch;
		this.fieldOfficer = fieldOfficer;
		this.supplyBatchId = supplyBatchId;
		this.quantity = quantity;
		this.date = date;
		this.farmer = farmer;
	}
	public Batch getBatch() {
		return batch;
	}
	public void setBatch(Batch batch) {
		this.batch = batch;
	}
	public FieldOfficer getFieldOfficer() {
		return fieldOfficer;
	}
	public void setFieldOfficer(FieldOfficer fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	public Long getSupplyBatchId() {
		return supplyBatchId;
	}
	public void setSupplyBatchId(Long supplyBatchId) {
		this.supplyBatchId = supplyBatchId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Farmer getFarmer() {
		return farmer;
	}
	public void setFarmer(Farmer farmer) {
		this.farmer = farmer;
	}
	public Catching getCatching() {
		return catching;
	}
	public void setCatching(Catching catching) {
		this.catching = catching;
	}
	public Set<Task> getTask() {
		return task;
	}
	public void setTask(Set<Task> task) {
		this.task = task;
	}
	public Set<Batch_Medicine> getBatchMedicine() {
		return batchMedicine;
	}
	public void setBatchMedicine(Set<Batch_Medicine> batchMedicine) {
		this.batchMedicine = batchMedicine;
	}
	public Set<Batch_Feed> getBatchFeed() {
		return batchFeed;
	}
	public void setBatchFeed(Set<Batch_Feed> batchFeed) {
		this.batchFeed = batchFeed;
	}
	public Set<FieldReport> getFieldReport() {
		return fieldReport;
	}
	public void setFieldReport(Set<FieldReport> fieldReport) {
		this.fieldReport = fieldReport;
	}
	public Set<FeedReturn> getFeedReturn() {
		return feedReturn;
	}
	public void setFeedReturn(Set<FeedReturn> feedReturn) {
		this.feedReturn = feedReturn;
	}
	public boolean isCaught() {
		return isCaught;
	}
	public void setCaught(boolean isCaught) {
		this.isCaught = isCaught;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public String getDistributeBatchID()
	{
		return "DB-"+supplyBatchId;
	}
	
	public String getPurchaseBatchID()
	{
		return "PB-"+batch.getBatchId();
	}
}
