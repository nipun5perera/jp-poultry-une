package com.poultry.jp.report;

import java.util.Date;

public class MedicineOrderReport {

	private Long medicineOrderId;
	private Date date;
	private double total;
	private double discount;
	private Supplier supplier;
	private boolean inStock;
	
	private PaymentRequest paymentRequest;
	
	public PaymentRequest getPaymentRequest() {
		return paymentRequest;
	}

	public void setPaymentRequest(PaymentRequest paymentRequest) {
		this.paymentRequest = paymentRequest;
	}

	public MedicineOrderReport(Long medicineOrderId, Date date, double total,
			double discount, Supplier supplier,boolean inStock) {
		super();
		this.medicineOrderId = medicineOrderId;
		this.date = date;
		this.total = total;
		this.discount = discount;
		this.supplier = supplier;
		this.inStock = inStock;
	}
	
	public MedicineOrderReport(Long medicineOrderId, Date date, double total,
			double discount, Supplier supplier) {
		super();
		this.medicineOrderId = medicineOrderId;
		this.date = date;
		this.total = total;
		this.discount = discount;
		this.supplier = supplier;
	}
	
	public Long getMedicineOrderId() {
		return medicineOrderId;
	}



	public void setMedicineOrderId(Long medicineOrderId) {
		this.medicineOrderId = medicineOrderId;
	}



	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public boolean isInStock() {
		return inStock;
	}
	public void setInStock(boolean inStock) {
		this.inStock = inStock;
	}
	
}
