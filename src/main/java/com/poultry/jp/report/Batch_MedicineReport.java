package com.poultry.jp.report;

import java.util.Date;


public class Batch_MedicineReport {
	
	private Long batchMedicineId;
	SupplyBatch batch;
	Medicine medicine;
	private Date date;
	private int quantity;
	private int billNumber;
	
	
	public Batch_MedicineReport(Long batchMedicineId, SupplyBatch batch,
			Medicine medicine, Date date, int quantity, int billNumber) {
		super();
		this.batchMedicineId = batchMedicineId;
		this.batch = batch;
		this.medicine = medicine;
		this.date = date;
		this.quantity = quantity;
		this.billNumber = billNumber;
	}
	
	public Batch_MedicineReport(Long batchMedicineId, SupplyBatch batch,
			Medicine medicine, Date date, int quantity) {
		super();
		this.batchMedicineId = batchMedicineId;
		this.batch = batch;
		this.medicine = medicine;
		this.date = date;
		this.quantity = quantity;
	}
	public Long getBatchMedicineId() {
		return batchMedicineId;
	}
	public void setBatchMedicineId(Long batchMedicineId) {
		this.batchMedicineId = batchMedicineId;
	}
	public SupplyBatch getBatch() {
		return batch;
	}
	public void setBatch(SupplyBatch batch) {
		this.batch = batch;
	}
	public Medicine getMedicine() {
		return medicine;
	}
	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getBillNumber() {
		return billNumber;
	}
	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}
	
}
