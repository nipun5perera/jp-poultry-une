package com.poultry.jp.report;

import java.util.Date;

public class Batch_FeedReport {

	private Long batchFeedId;
	SupplyBatch supplyBatch;
	Feed feed;
	private Date date;
	private int quantity;
	private int billNumber;
	
	public Batch_FeedReport(Long batchFeedId, SupplyBatch supplyBatch, Feed feed,Date date, int quantity) {
		super();
		this.batchFeedId = batchFeedId;
		this.supplyBatch = supplyBatch;
		this.feed = feed;
		this.date = date;
		this.quantity = quantity;
	}
	
	public Batch_FeedReport(Long batchFeedId, SupplyBatch supplyBatch,
			Feed feed, Date date, int quantity, int billNumber) {
		super();
		this.batchFeedId = batchFeedId;
		this.supplyBatch = supplyBatch;
		this.feed = feed;
		this.date = date;
		this.quantity = quantity;
		this.billNumber = billNumber;
	}

	public Long getBatchFeedId() {
		return batchFeedId;
	}
	public void setBatchFeedId(Long batchFeedId) {
		this.batchFeedId = batchFeedId;
	}
	 
	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}
	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}
	public Feed getFeed() {
		return feed;
	}
	public void setFeed(Feed feed) {
		this.feed = feed;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getBillNumber() {
		return billNumber;
	}
	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}
	
	public String getFeedIssueID()
	{
		return "FI-"+batchFeedId;
	}
	
	
}
