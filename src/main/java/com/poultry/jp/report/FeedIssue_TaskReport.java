package com.poultry.jp.report;

import com.poultry.jp.model.SupplyBatch;

import java.util.Date;


public class FeedIssue_TaskReport extends TaskReport{

	private int quantity;
	private int round;
	private String feedType;
	
	public FeedIssue_TaskReport(Long taskId, String status, Date duedate,SupplyBatch supplybatch,
			String type, int quantity, int round, String feedType) {
		super(taskId, status, duedate,supplybatch, type);
		this.quantity = quantity;
		this.round = round;
		this.feedType = feedType;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getRound() {
		return round;
	}
	public void setRound(int round) {
		this.round = round;
	}
	public String getFeedType() {
		return feedType;
	}
	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}

	@Override
	public Long getTaskId() {
		// TODO Auto-generated method stub
		return super.getTaskId();
	}

	@Override
	public void setTaskId(Long taskId) {
		// TODO Auto-generated method stub
		super.setTaskId(taskId);
	}

	@Override
	public String getStatus() {
		// TODO Auto-generated method stub
		return super.getStatus();
	}

	@Override
	public void setStatus(String status) {
		// TODO Auto-generated method stub
		super.setStatus(status);
	}

	@Override
	public Date getDuedate() {
		// TODO Auto-generated method stub
		return super.getDuedate();
	}

	@Override
	public void setDuedate(Date duedate) {
		// TODO Auto-generated method stub
		super.setDuedate(duedate);
	}

	@Override
	public Date getPerformedDate() {
		// TODO Auto-generated method stub
		return super.getPerformedDate();
	}

	@Override
	public void setPerformedDate(Date performedDate) {
		// TODO Auto-generated method stub
		super.setPerformedDate(performedDate);
	}


	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return super.getType();
	}

	@Override
	public void setType(String type) {
		// TODO Auto-generated method stub
		super.setType(type);
	}

	@Override
	public SupplyBatch getSupplyBatch() {
		// TODO Auto-generated method stub
		return super.getSupplyBatch();
	}

	@Override
	public void setSupplyBatch(SupplyBatch supplyBatch) {
		// TODO Auto-generated method stub
		super.setSupplyBatch(supplyBatch);
	}

	@Override
	public String getDistributeBatchID() {
		// TODO Auto-generated method stub
		return super.getDistributeBatchID();
	}
	 
	
	

}
