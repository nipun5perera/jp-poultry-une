package com.poultry.jp.report;

public class FarmerReport {
	
	private Long farmerId;
	private String name;
	private String contact;
	private int capacity;
	private String address;
  
	
	 
	public FarmerReport(Long farmerId, String name) {
		super();
		this.farmerId = farmerId;
		this.name = name;
	}

	public FarmerReport(Long farmerId, String name, String contact,
			int capacity, String address) {
		super();
		this.farmerId = farmerId;
		this.name = name;
		this.contact = contact;
		this.capacity = capacity;
		this.address = address;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Long getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Long farmerId) {
		this.farmerId = farmerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
 
	

}
