package com.poultry.jp.report;

public class FeedReport {

	private long feedId;
	private String name;
	private long quantity;
	private String manufacturer;
	private double price;

	public FeedReport(long feedId, String name, long quantity,
			String manufacturer, double price) {
		super();
		this.feedId = feedId;
		this.name = name;
		this.quantity = quantity;
		this.manufacturer = manufacturer;
		this.price = price;
	}

	public FeedReport(long feedId, String name, long quantity) {
		super();
		this.feedId = feedId;
		this.name = name;
		this.quantity = quantity;
	}

	public long getFeedId() {
		return feedId;
	}

	public void setFeedId(long feedId) {
		this.feedId = feedId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getFeedNameManufacturer() {
		return name + "," + manufacturer;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
