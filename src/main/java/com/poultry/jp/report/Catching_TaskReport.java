package com.poultry.jp.report;

import com.poultry.jp.model.SupplyBatch;

import java.util.Date;

public class Catching_TaskReport extends TaskReport{

	public Catching_TaskReport(Long taskId, String status, Date duedate,
                               SupplyBatch supplyBatch, String type) {
		super(taskId, status, duedate, supplyBatch, type);
	}

	@Override
	public Long getTaskId() {
		// TODO Auto-generated method stub
		return super.getTaskId();
	}

	@Override
	public void setTaskId(Long taskId) {
		// TODO Auto-generated method stub
		super.setTaskId(taskId);
	}

	@Override
	public String getStatus() {
		// TODO Auto-generated method stub
		return super.getStatus();
	}

	@Override
	public void setStatus(String status) {
		// TODO Auto-generated method stub
		super.setStatus(status);
	}

	@Override
	public Date getDuedate() {
		// TODO Auto-generated method stub
		return super.getDuedate();
	}

	@Override
	public void setDuedate(Date duedate) {
		// TODO Auto-generated method stub
		super.setDuedate(duedate);
	}

	@Override
	public Date getPerformedDate() {
		// TODO Auto-generated method stub
		return super.getPerformedDate();
	}

	@Override
	public void setPerformedDate(Date performedDate) {
		// TODO Auto-generated method stub
		super.setPerformedDate(performedDate);
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return super.getType();
	}

	@Override
	public void setType(String type) {
		// TODO Auto-generated method stub
		super.setType(type);
	}

	@Override
	public SupplyBatch getSupplyBatch() {
		// TODO Auto-generated method stub
		return super.getSupplyBatch();
	}

	@Override
	public void setSupplyBatch(SupplyBatch supplyBatch) {
		// TODO Auto-generated method stub
		super.setSupplyBatch(supplyBatch);
	}

	@Override
	public String getDistributeBatchID() {
		// TODO Auto-generated method stub
		return super.getDistributeBatchID();
	}
	

}
