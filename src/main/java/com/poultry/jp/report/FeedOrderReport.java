package com.poultry.jp.report;

import com.poultry.jp.model.FeedOrderDetail;
import com.poultry.jp.model.Supplier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FeedOrderReport {
	
	private Long feedOrderId;
	private Date date;
	private double total;
	private double discount;
	private Supplier supplier;
	private boolean inStock;
	private List<FeedOrderDetail> feedOrderDetail = new ArrayList<FeedOrderDetail>();


	
	
	public FeedOrderReport(Long feedOrderId, Date date, double total,
			double discount, Supplier supplier,
			List<FeedOrderDetail> feedOrderDetail) {
		super();
		this.feedOrderId = feedOrderId;
		this.date = date;
		this.total = total;
		this.discount = discount;
		this.supplier = supplier;
		this.feedOrderDetail = feedOrderDetail;
	}

	public FeedOrderReport(Long feedOrderId, Date date, double total, double discount,
			Supplier supplier) {
		super();
		this.feedOrderId = feedOrderId;
		this.date = date;
		this.total = total;
		this.discount = discount;
		this.supplier = supplier;
	}
	
	public FeedOrderReport(Long feedOrderId, Date date, double total, double discount,
			Supplier supplier,boolean inStock) {
		super();
		this.feedOrderId = feedOrderId;
		this.date = date;
		this.total = total;
		this.discount = discount;
		this.supplier = supplier;
		this.inStock = inStock;
	}
	 
	public Long getFeedOrderId() {
		return feedOrderId;
	}

	public void setFeedOrderId(Long feedOrderId) {
		this.feedOrderId = feedOrderId;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public List<FeedOrderDetail> getFeedOrderDetail() {
		return feedOrderDetail;
	}

	public void setFeedOrderDetail(List<FeedOrderDetail> feedOrderDetail) {
		this.feedOrderDetail = feedOrderDetail;
	}

	public boolean isInStock() {
		return inStock;
	}

	public void setInStock(boolean inStock) {
		this.inStock = inStock;
	}

	

}
