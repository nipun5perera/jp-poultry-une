package com.poultry.jp.report;

public class MedicineReport {

	private Long medicineId;
	private String name;
	private int quantity;
	private double price;
	
	
	
	public MedicineReport(Long medicineId, String name, int quantity) {
		super();
		this.medicineId = medicineId;
		this.name = name;
		this.quantity = quantity;
	}
	public MedicineReport(Long medicineId, String name, int quantity,
			double price) {
		super();
		this.medicineId = medicineId;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
	}
	public Long getMedicineId() {
		return medicineId;
	}
	public void setMedicineId(Long medicineId) {
		this.medicineId = medicineId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
