package com.poultry.jp.report;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class PaymentRequestReport {

	private Long paymentId;
	private double amount;
	private String description;
	private String status;
	private boolean isDeleted;

	private Date created;
	private Date updated;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	private FeedOrder feedOrder;
	private MedicineOrder medicineOrder;
	private SupplyBatch supplyBatch;

	public PaymentRequestReport(Long paymentId, Date date) {
		super();
		this.paymentId = paymentId;
		this.date = date;
	}

	public PaymentRequestReport(Long paymentId, double amount,
			String description, String status, Date date) {
		super();
		this.paymentId = paymentId;
		this.amount = amount;
		this.description = description;
		this.status = status;
		this.date = date;

	}

	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public FeedOrder getFeedOrder() {
		return feedOrder;
	}

	public void setFeedOrder(FeedOrder feedOrder) {
		this.feedOrder = feedOrder;
	}

	public MedicineOrder getMedicineOrder() {
		return medicineOrder;
	}

	public void setMedicineOrder(MedicineOrder medicineOrder) {
		this.medicineOrder = medicineOrder;
	}

	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}

	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	
}
