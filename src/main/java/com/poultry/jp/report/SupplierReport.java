package com.poultry.jp.report;

public class SupplierReport {
	
	private Long supplierId;
	private String name;
	
	private String email;
	private String contact;
	private String address1;
	private String address2;
	private String city;
	
	
	
	 


	public SupplierReport(Long supplierId, String name, String email,
			String contact, String address1, String address2, String city) {
		super();
		this.supplierId = supplierId;
		this.name = name;
		this.email = email;
		this.contact = contact;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getContact() {
		return contact;
	}


	public void setContact(String contact) {
		this.contact = contact;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public Long getSupplierId() {
		return supplierId;
	}


	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}


	public SupplierReport(Long supplierId,String name)
	{
		this.supplierId = supplierId;
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

}
