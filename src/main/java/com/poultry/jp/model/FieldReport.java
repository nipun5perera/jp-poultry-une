package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

 
@Entity
public class FieldReport {

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long fieldReportId;
	private double amountAte;
	private double FCR;
	private double averageWeight;
	private int numOfChicksDied;
	private int age;
	
	
	@ManyToOne
	private FieldOfficer fieldOfficer;
	
	@ManyToOne
	private SupplyBatch supplyBatch;
	
	
	
	public Long getFieldReportId() {
		return fieldReportId;
	}
	public void setFieldReportId(Long fieldReportId) {
		this.fieldReportId = fieldReportId;
	}
	public double getAmountAte() {
		return amountAte;
	}
	public void setAmountAte(double amountAte) {
		this.amountAte = amountAte;
	}
	public double getFCR() {
		return FCR;
	}
	public void setFCR(double fCR) {
		FCR = fCR;
	}
	public double getAverageWeight() {
		return averageWeight;
	}
	public void setAverageWeight(double averageWeight) {
		this.averageWeight = averageWeight;
	}
	public int getNumOfChicksDied() {
		return numOfChicksDied;
	}
	public void setNumOfChicksDied(int numOfChicksDied) {
		this.numOfChicksDied = numOfChicksDied;
	}
	public FieldOfficer getFieldOfficer() {
		return fieldOfficer;
	}
	public void setFieldOfficer(FieldOfficer fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	 
	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}
	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
}
