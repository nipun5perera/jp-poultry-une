package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQueries({
	@NamedQuery(name = Medicine.FIND_MEDICINE_DETAILS, query = "Select new com.poultry.jp.report.MedicineReport(m.medicineId,m.name,m.quantity,m.price) from Medicine m WHERE m.isDeleted = false")})
public class Medicine {
	
	public static final String FIND_MEDICINE_DETAILS = "findMedicineDetails";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long medicineId;
	@NotEmpty
	private String name;
	@NotNull
	private int quantity;
	@NotNull
	private double price;
	
	private boolean isDeleted;
	
//	@OneToMany(mappedBy = "medicine")
//	private List<MedicineOrderDetails> MedicineOrderDetails = new ArrayList<MedicineOrderDetails>();
//	
//	
//	@OneToMany(mappedBy="medicine")
//	private List<Batch_Medicine> batchMedicine;
	
	public Long getMedicineId() {
		return medicineId;
	}


	public void setMedicineId(Long medicineId) {
		this.medicineId = medicineId;
	}


//	public List<Batch_Medicine> getBatchMedicine() {
//		return batchMedicine;
//	}
//
//
//	public void setBatchMedicine(List<Batch_Medicine> batchMedicine) {
//		this.batchMedicine = batchMedicine;
//	}
//
//
//	public List<MedicineOrderDetails> getMedicineOrderDetails() {
//		return MedicineOrderDetails;
//	}
//
//
//	public void setMedicineOrderDetails(
//			List<MedicineOrderDetails> medicineOrderDetails) {
//		MedicineOrderDetails = medicineOrderDetails;
//	}

	public String getName() {
		return name;
	}
	public int getQuantity() {
		return quantity;
	}
	 
	public void setName(String name) {
		this.name = name;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
