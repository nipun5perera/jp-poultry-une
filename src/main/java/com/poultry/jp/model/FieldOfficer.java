package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@NamedQueries(
		{
			@NamedQuery(name=FieldOfficer.FIND_FIELD_OFFICER_REPORTS,query="Select new com.poultry.jp.report.FieldOfficerReport(f.fieldOfficerId,f.fname,f.lname,f.mname) from FieldOfficer f where f.isDeleted = false"),
					@NamedQuery(name= FieldOfficer.FIND_ALL_FIELD_OFFICER,query="SELECT new com.poultry.jp.report.FieldOfficerReport(f.fieldOfficerId,f.fname,f.lname,f.mname,f.contact,f.NIC) from FieldOfficer f where f.isDeleted = false")
		})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="fieldOfficerId")
public class FieldOfficer {
	
	public static final String FIND_FIELD_OFFICER_REPORTS="findFieldOfficerrReports";
	public static final String FIND_ALL_FIELD_OFFICER = "findAllFieldOfficer";

//	@OneToMany(mappedBy = "fieldOfficer", fetch = FetchType.EAGER)
//	private Set<Batch> batch = new HashSet<Batch>();

//	@OneToMany(mappedBy="fieldOfficer",fetch = FetchType.EAGER)
//	private Set<FieldReport> fieldReports = new HashSet<FieldReport>();
//	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long fieldOfficerId;
	private String fname;
	private String mname;
	private String lname;
	private String contact;
	private String NIC;
	private boolean isDeleted;
	
//	@JsonIgnore
//	public Set<Batch> getBatch() {
//		return batch;
//	}
//	public void setBatch(Set<Batch> batch) {
//		this.batch = batch;
//	}
	public Long getFieldOfficerId() {
		return fieldOfficerId;
	}
	public void setFieldOfficerId(Long fieldOfficerId) {
		this.fieldOfficerId = fieldOfficerId;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getNIC() {
		return NIC;
	}
	public void setNIC(String nIC) {
		NIC = nIC;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public String getFieldOfficerFullName()
	{
		return fname+" "+mname+" "+lname;
	}
	
//	@JsonIgnore
//	public Set<FieldReport> getFieldReports() {
//		return fieldReports;
//	}
//	public void setFieldReports(Set<FieldReport> fieldReports) {
//		this.fieldReports = fieldReports;
//	}
	
	
}
