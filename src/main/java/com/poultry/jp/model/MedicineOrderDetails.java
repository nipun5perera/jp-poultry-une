/**
 * 
 */
package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */

@Entity
public class MedicineOrderDetails {
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long medicineOrderId;
	private double unitPrice;
	private int quantity;
	
	@ManyToOne
	@JoinColumn(name="order_id")
	private MedicineOrder medicineOrder;
	
	@ManyToOne
	@JoinColumn(name="medicine_id")
	private Medicine medicine;
	
	public MedicineOrderDetails(){
		medicineOrder = new MedicineOrder();
	};
	
	public MedicineOrderDetails(Medicine medicine, MedicineOrder medicineOrder, double unitPrice, int quantity){
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.medicineOrder = medicineOrder;
		this.medicine = medicine;
	}
	
	/**
	 * @return the medicine
	 */
	public Medicine getMedicine() {
		return medicine;
	}

	/**
	 * @param medicine the medicine to set
	 */
	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}

	/**
	 * @return the medicineOrder
	 */
	public MedicineOrder getMedicineOrder() {
		return medicineOrder;
	}

	/**
	 * @param medicineOrder the medicineOrder to set
	 */
	public void setMedicineOrder(MedicineOrder medicineOrder) {
		this.medicineOrder = medicineOrder;
	}

	public Long getMedicineOrderId() {
		return medicineOrderId;
	}

	public void setMedicineOrderId(Long medicineOrderId) {
		this.medicineOrderId = medicineOrderId;
	}

	/**
	 * @return the unitPrice
	 */
	public double getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
