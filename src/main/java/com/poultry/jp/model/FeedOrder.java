package com.poultry.jp.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
@Entity
@NamedQueries({ 
	@NamedQuery(name = FeedOrder.FIND_FEED_ORDER_DETAILS, query = "Select new com.poultry.jp.report.FeedOrderReport(fo.feedOrderId,fo.date,fo.total,fo.discount,fo.supplier,fo.inStock) from FeedOrder fo WHERE fo.isDeleted = false") })
public class FeedOrder {

	public static final String FIND_FEED_ORDER_DETAILS = "findFeedOrderDetails";
	public static final String FIN_FEED_ORDER = "findFeedOrder";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long feedOrderId;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date date;
	@NotNull
	private double total;
	@NotNull
	private double discount;
	private boolean inStock;
	
	private boolean isDeleted;
	private Date created;
	private Date updated;
	private boolean isFulFilled;
	

	@OneToMany(mappedBy = "feedOrder", cascade = CascadeType.ALL ,fetch = FetchType.EAGER,orphanRemoval=true)
	private List<FeedOrderDetail> feedOrderDetail = new ArrayList<FeedOrderDetail>();

	@ManyToOne
	private Supplier supplier;
	
	@OneToOne(cascade={CascadeType.MERGE , CascadeType.PERSIST})
	@JoinColumn(name="paymentRequest_id")
	@Valid
	private PaymentRequest paymentRequest;

	public FeedOrder() {
	}

	public FeedOrder(Date date, double total, double discount) {
		this.date = date;
		this.total = total;
		this.discount = discount;
	}

	public double getDiscount() {
		return discount;
	}

	public List<FeedOrderDetail> getFeedOrderDetail() {
		return feedOrderDetail;
	};

 
	public Supplier getSupplier() {
		return supplier;
	}

	public double getTotal() {
		return total;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public void setFeedOrderDetail(List<FeedOrderDetail> feedOrderDetail) {
		this.feedOrderDetail = feedOrderDetail;
	}
 

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	public PaymentRequest getPaymentRequest() {
		return paymentRequest;
	}

	public void setPaymentRequest(PaymentRequest paymentRequest) {
		this.paymentRequest = paymentRequest;
	}

	public Long getFeedOrderId() {
		return feedOrderId;
	}

	public void setFeedOrderId(Long feedOrderId) {
		this.feedOrderId = feedOrderId;
	}
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@PrePersist
	protected void onCreate() {
		created = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public boolean isInStock() {
		return inStock;
	}

	public void setInStock(boolean inStock) {
		this.inStock = inStock;
	}

	public boolean isFulFilled() {
		return isFulFilled;
	}

	public void setFulFilled(boolean isFulFilled) {
		this.isFulFilled = isFulFilled;
	}
	
	
	
	 
}
