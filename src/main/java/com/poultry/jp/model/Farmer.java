package com.poultry.jp.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@NamedQueries({
		@NamedQuery(name = Farmer.FIND_FARMER_NAMES, query = "Select new com.poultry.jp.report.FarmerReport(f.farmerId,f.fid) from Farmer f WHERE f.isDeleted = false"),
		@NamedQuery(name = Farmer.FIND_FARMER_DETAILS, query = "Select new com.poultry.jp.report.FarmerReport(f.farmerId,f.name,f.contact,f.capacity,f.address) from Farmer f where f.isDeleted = false") })
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="farmerId")
public class Farmer {

	public static final String FIND_FARMER_NAMES = "findFarmerNames";
	public static final String FIND_FARMER_DETAILS= "findFarmerDetails";

	@JsonBackReference
	@OneToMany(mappedBy = "farmer",fetch = FetchType.EAGER)
	private Set<SupplyBatch> supplyBatch = new HashSet<SupplyBatch>();

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long farmerId;
	@NotEmpty
	private String name;
	private String address;
	private String contact;
	@NotNull
	private int capacity;
	@NotEmpty
	private String fid;
	private boolean isDeleted;
	private boolean isAvailable;

	public String getAddress() {
		return address;
	}

	 
	public Set<SupplyBatch> getSupplyBatch() {
		return supplyBatch;
	}

	public void setSupplyBatch(Set<SupplyBatch> supplyBatch) {
		this.supplyBatch = supplyBatch;
	}

	public int getCapacity() {
		return capacity;
	}

	public String getContact() {
		return contact;
	}

	public String getName() {
		return name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Long farmerId) {
		this.farmerId = farmerId;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public String getFid() {
		return fid;
	}


	public void setFid(String fid) {
		this.fid = fid;
	}


	public boolean isAvailable() {
		return isAvailable;
	}


	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	
	
	
}
