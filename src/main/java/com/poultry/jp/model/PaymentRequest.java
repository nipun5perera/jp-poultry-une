package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@NamedQueries(
		{ @NamedQuery(name = PaymentRequest.FIND_PAYMENT_REQUEST_DATES, query = "SELECT new com.poultry.jp.report.PaymentRequestReport(p.paymentId,p.date) FROM PaymentRequest p where p.isDeleted = false AND p.status='Approved' and p.isPaid= false"),
		@NamedQuery(name= PaymentRequest.FIND_PAYMENT_REQUEST_BY_ID,query="SELECT new com.poultry.jp.report.PaymentRequestReport(p.paymentId,p.amount,p.description,p.status,p.date ) "
				+ "FROM PaymentRequest p where p.paymentId = :id"),
		@NamedQuery(name=PaymentRequest.FIND_PAYMENT_REQUEST_REPORT,query="SELECT new com.poultry.jp.report.PaymentRequestReport(p.paymentId,p.amount,p.description,p.status,p.date)FROM PaymentRequest p WHERE P.isDeleted = false AND p.status = 'Pending'")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PaymentRequest{
	
	public static final String FIND_PAYMENT_REQUEST_DATES = "findPaymentReequestDates";
	public static final String FIND_PAYMENT_REQUEST_BY_ID = "findPaymentReequestById";
	public static final String FIND_PAYMENT_REQUEST_REPORT = "findPaymentRequestReport";
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long paymentId;
	@NotNull
	private double amount;
	@Type(type = "text")
	@NotEmpty
	private String description;
	private String chequeID;
	private String status;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date date;
	
	private boolean isDeleted;
	private boolean isPaid;
	
	private Date created;
	  private Date updated;

//	@OneToOne(mappedBy = "paymentRequest")
//	private FeedOrder feedOrder;
//
//	@OneToOne(mappedBy = "paymentRequest")
//	private MedicineOrder medicineOrder;
//
//	@OneToOne(mappedBy = "paymentRequest")
//	private SupplyBatch supplyBatch;

	 
	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	 
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@PrePersist
	protected void onCreate() {
		created = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public boolean isPaid() {
		return isPaid;
	}

	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	public String getChequeID() {
		return chequeID;
	}

	public void setChequeID(String chequeID) {
		this.chequeID = chequeID;
	}
	
}
