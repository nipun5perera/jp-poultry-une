package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQueries(
		{
			@NamedQuery(name=Supplier.FIND_SUPPLIER_NAMES,query="Select new com.poultry.jp.report.SupplierReport(s.supplierId,s.name) from Supplier s WHERE s.isDeleted = false"),
					@NamedQuery(name = Supplier.FIND_SUPPLIER_DETAILS,query="Select new com.poultry.jp.report.SupplierReport(s.supplierId,s.name,s.email,s.contact,s.address1,s.address2,s.city) from Supplier s where s.isDeleted = false")
					
		})
public class Supplier {

	public static final String FIND_SUPPLIER_NAMES="findSupplierNames";
	public static final String FIND_SUPPLIER_DETAILS = "findSupplierDetails";
	
	@Id
	@GeneratedValue
	private Long supplierId;
	@NotEmpty
	private String name;
	private String email;
	private String contact;
	@NotEmpty
	private String address1;
	private String address2;
	private String city;
	
	private boolean isDeleted;

	 

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	 

	 

}
