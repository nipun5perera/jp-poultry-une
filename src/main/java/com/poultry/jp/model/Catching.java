package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Catching {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long catchingId;
	private int numOfChicks;
	private double liveWeight;
	private double averageWeight;
	private double FCR;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	
	
	@OneToOne
	@JoinColumn(name="supplyBatch_id")
	private SupplyBatch supplyBatch;
	
	
	public Long getCatchingId() {
		return catchingId;
	}
	public void setCatchingId(Long catchingId) {
		this.catchingId = catchingId;
	}
	public int getNumOfChicks() {
		return numOfChicks;
	}
	public void setNumOfChicks(int numOfChicks) {
		this.numOfChicks = numOfChicks;
	}
	public double getLiveWeight() {
		return liveWeight;
	}
	public void setLiveWeight(double liveWeight) {
		this.liveWeight = liveWeight;
	}
	public double getAverageWeight() {
		return averageWeight;
	}
	public void setAverageWeight(double averageWeight) {
		this.averageWeight = averageWeight;
	}
	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}
	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getFCR() {
		return FCR;
	}
	public void setFCR(double fCR) {
		FCR = fCR;
	}
	
}
