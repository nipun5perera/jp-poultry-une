/**
 * 
 */
package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Nipun Perera
 * @created 23 Jul 2015
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = Feed.FIND_FEED_DETAILS, query = "Select new com.poultry.jp.report.FeedReport(f.feedId,f.name,f.quantity,f.manufacturer,f.price) from Feed f where f.isDeleted = false")})
public class Feed {
	
	public static final String FIND_FEED_DETAILS = "findFeedDetails";

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long feedId;
	@NotEmpty
	private String name;
	@NotNull
	private long quantity;
	private boolean isDeleted;
	@NotEmpty
	private String manufacturer;
	@NotNull
	private double price;
	
	
//	@OneToMany(mappedBy="feed",cascade = CascadeType.ALL)
//	private List<FeedOrderDetail> detail;
//	
//	@OneToMany(mappedBy="feed")
//	private List<Batch_Feed> feedMedicine;
//	
//	@OneToMany(mappedBy="feed")
//	private Set<FeedReturn> feedReturn = new HashSet<FeedReturn>();
//	
	
	
	
//	@OneToMany(mappedBy = "feed") // mappedBy references the fieldname in the other Java class
//    private List<FeedOrderDetail> details;
//	 
//	public List<FeedOrderDetail> getDetails() {
//		return details;
//	}
//
//	public void setDetails(List<FeedOrderDetail> details) {
//		this.details = details;
//	}

	 
	public Feed(){};
	
	 

	public Feed(String name,long quantity){
		this.name = name;
		this.quantity = quantity;
	}

	public long getFeedId() {
		return feedId;
	}

	public void setFeedId(long feedId) {
		this.feedId = feedId;
	}

	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}

	 
	public long getQuantity() {
		return quantity;
	}

	 
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getFeedNameManufacturer()
	{
		return name+","+manufacturer;
	}
	
}
