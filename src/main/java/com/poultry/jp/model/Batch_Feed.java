package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@NamedQueries({@NamedQuery(name=Batch_Feed.FINDBATCHFEEDBYID,query="SELECT new com.poultry.jp.report.Batch_FeedReport(bf.batchFeedId,bf.supplyBatch,bf.feed,bf.date,bf.quantity) FROM Batch_Feed bf WHERE bf.supplyBatch.supplyBatchId = :bid"),
		@NamedQuery(name=Batch_Feed.FINDALLBATCHFEED,query="SELECT new com.poultry.jp.report.Batch_FeedReport(bf.batchFeedId,bf.supplyBatch,bf.feed,bf.date,bf.quantity,bf.billNumber) FROM Batch_Feed bf")})
public class  Batch_Feed {
	
	public static final String FINDBATCHFEEDBYID = "findBatchFeedById";
	public static final String FINDALLBATCHFEED = "findAllBatchFeed";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long batchFeedId;

	@ManyToOne
	@JoinColumn(name = "supplyBatch_id")
	private SupplyBatch supplyBatch;
	
	@ManyToOne
	@JoinColumn(name = "feed_id")
	Feed feed;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date date;

	private int quantity;
	private double unitPrice;
	private double amount;
	
	@OneToOne(cascade=CascadeType.MERGE)
	private Task task ;
	
	
	private double actualPrice;
	
	 
	/*	@ManyToOne
	@JoinColumn(name="feedOrder_id")
	@PrimaryKeyJoinColumn
	private FeedOrder feedOrder;*/
	
	private int billNumber;
	
	 
	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}

	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}

	public Date getDate() {
		return date;
	}

	public Feed getFeed() {
		return feed;
	}

	public int getQuantity() {
		return quantity;
	}
 
	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setFeed(Feed feed) {
		this.feed = feed;
	}
 
	public Long getBatchFeedId() {
		return batchFeedId;
	}

	public void setBatchFeedId(Long batchFeedId) {
		this.batchFeedId = batchFeedId;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}

	public double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(double actualPrice) {
		this.actualPrice = actualPrice;
	}
}
