/**
 * FeedOrderDetails class with three attributes
 */
package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class FeedOrderDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long feedOrderDetailId;
	private double unitPrice;
	private long quantity;
	private int filledQuantity;
	
	
	@ManyToOne
	@JoinColumn(name="feed_id")
	private Feed feed;
	
	
	@ManyToOne 
	@JoinColumn(name="order_id")
	private FeedOrder feedOrder ;
	
	
	
	
	public FeedOrderDetail() {
		super();
		feedOrder = new FeedOrder();
	}

	public Feed getFeed() {
		return feed;
	}

	public FeedOrder getFeedOrder() {
		return feedOrder;
	}

	public Long getFeedOrderDetailId() {
		return feedOrderDetailId;
	}

	public void setFeedOrderDetailId(Long feedOrderDetailId) {
		this.feedOrderDetailId = feedOrderDetailId;
	}

	public long getQuantity() {
		return quantity;
	}

	// @ManyToOne(cascade=CascadeType.MERGE)
	// @JoinColumn(name="feed_id")
	// private Feed feed;

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setFeed(Feed feed) {
		this.feed = feed;
	}

	public void setFeedOrder(FeedOrder feedOrder) {
		this.feedOrder = feedOrder;
		 
	}

	 
//
//	public Feed getFeed() {
//		return feed;
//	}
//
//	public void setFeed(Feed feed) {
//		this.feed = feed;
//	}

 
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getFilledQuantity() {
		return filledQuantity;
	}

	public void setFilledQuantity(int filledQuantity) {
		this.filledQuantity = filledQuantity;
	}
	
	
}
