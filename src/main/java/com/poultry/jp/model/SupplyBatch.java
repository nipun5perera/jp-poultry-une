package com.poultry.jp.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@NamedQueries({ @NamedQuery(name = SupplyBatch.FIND_SUPPLY_BATCHES, query = "Select new com.poultry.jp.report.SupplyBatchReport(s.batch, s.fieldOfficer,s.supplyBatchId, s.quantity, s.date, s.farmer) from SupplyBatch s WHERE s.isDeleted = false AND s.isCaught = false") })
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "supplyBatchId")
public class SupplyBatch {

	public static final String FIND_SUPPLY_BATCHES = "findSupplyBatches";

	@ManyToOne
	private Batch batch;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private FieldOfficer fieldOfficer;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long supplyBatchId;
	@NotNull
	private int quantity;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date date;
	@ManyToOne(fetch = FetchType.EAGER)
	private Farmer farmer;
	@OneToOne(mappedBy="supplyBatch")
	private Catching catching;
	@NotNull
	private double chickPrice;
	private int billNumber;
	
	@OneToMany(mappedBy = "supplyBatch",fetch = FetchType.EAGER,cascade = CascadeType.PERSIST)
	@JsonBackReference
	private Set<Task> task = new HashSet<Task>();
	
	@OneToMany(mappedBy = "supplyBatch",fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Batch_Medicine> batchMedicine= new HashSet<Batch_Medicine>();
	
	@OneToMany(mappedBy = "supplyBatch",fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Batch_Feed> batchFeed = new HashSet<Batch_Feed>();
	
	@OneToMany(mappedBy = "supplyBatch",fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<FieldReport> fieldReport = new HashSet<FieldReport>();
	
	@OneToMany(mappedBy="supplyBatch",fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<FeedReturn> feedReturn = new HashSet<FeedReturn>();
	
	private boolean isCaught;
	private boolean isDeleted;
	private Date created;
	private Date updated;

	public SupplyBatch() {
	}

	public SupplyBatch(int quantity) {

		this.quantity = quantity;
	}

	@org.codehaus.jackson.annotate.JsonIgnore
	public Batch getBatch() {
		return batch;
	}

	
	public int getQuantity() {
		return quantity;
	}

	public Farmer getFarmer() {
		return farmer;
	}

	public void setFarmer(Farmer farmer) {
		this.farmer = farmer;
	}
	
	public Long getSupplyBatchId() {
		return supplyBatchId;
	}

	public void setSupplyBatchId(Long supplyBatchId) {
		this.supplyBatchId = supplyBatchId;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setFieldOfficer(FieldOfficer fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}

	public FieldOfficer getFieldOfficer() {
		return fieldOfficer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public Set<Task> getTask() {
		return task;
	}

	public void setTask(Set<Task> task) {
		this.task = task;
	}
	
	public Set<Batch_Medicine> getBatchMedicine() {
		return batchMedicine;
	}

	public void setBatchMedicine(Set<Batch_Medicine> batchMedicine) {
		this.batchMedicine = batchMedicine;
	}
	
	public Set<Batch_Feed> getBatchFeed() {
		return batchFeed;
	}

	public void setBatchFeed(Set<Batch_Feed> batchFeed) {
		this.batchFeed = batchFeed;
	}
	
	@PrePersist
	protected void onCreate() {
		created = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public boolean isCaught() {
		return isCaught;
	}

	public void setCaught(boolean isCaught) {
		this.isCaught = isCaught;
	}
	public Catching getCatching() {
		return catching;
	}

	public void setCatching(Catching catching) {
		this.catching = catching;
	}

	public Set<FieldReport> getFieldReport() {
		return fieldReport;
	}

	public void setFieldReport(Set<FieldReport> fieldReport) {
		this.fieldReport = fieldReport;
	}

	public Set<FeedReturn> getFeedReturn() {
		return feedReturn;
	}

	public void setFeedReturn(Set<FeedReturn> feedReturn) {
		this.feedReturn = feedReturn;
	}

	public double getChickPrice() {
		return chickPrice;
	}

	public void setChickPrice(double chickPrice) {
		this.chickPrice = chickPrice;
	}

	public int getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}
}
