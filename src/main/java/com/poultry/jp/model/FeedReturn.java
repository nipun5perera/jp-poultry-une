package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class FeedReturn {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long feedReturnId;
	private double quantity;
	
	@ManyToOne
	private SupplyBatch supplyBatch;
	
	@ManyToOne
	private Feed feed;

	
	
	public Long getFeedReturnId() {
		return feedReturnId;
	}

	public void setFeedReturnId(Long feedReturnId) {
		this.feedReturnId = feedReturnId;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}

	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}

	public Feed getFeed() {
		return feed;
	}

	public void setFeed(Feed feed) {
		this.feed = feed;
	}
	
	
	
}
