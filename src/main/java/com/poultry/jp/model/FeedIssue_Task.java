package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@NamedQueries({
@NamedQuery(name=FeedIssue_Task.FINDFEEDISSUETASKS,query="SELECT new com.poultry.jp.report.FeedIssue_TaskReport(t.taskId,t.status,t.duedate,t.supplyBatch,t.type,t.quantity,t.round,t.feedType) FROM FeedIssue_Task t WHERE t.status ='Pending' or t.status='Incomplete' order by t.duedate"),
@NamedQuery(name=FeedIssue_Task.FINDFEEDISSUETASKBYDATE,query="SELECT new com.poultry.jp.report.FeedIssue_TaskReport(t.taskId,t.status,t.duedate,t.supplyBatch,t.type,t.quantity,t.round,t.feedType) FROM FeedIssue_Task t WHERE (t.status ='Pending' or t.status='Incomplete') AND t.duedate = :date order by t.duedate")
})
public class FeedIssue_Task extends Task  {
	
	public static final String FINDFEEDISSUETASKS ="findFeedIssueTasks";
	public static final String FINDFEEDISSUETASKBYDATE = "findFeedIssueTaskByDate";
	private int quantity;
	private int round;
	private String feedType;
	
	public String getFeedType() {
		return feedType;
	}
	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public Date getDuedate() {
		return super.getDuedate();
	}
	@Override
	public void setDuedate(Date duedate) {
		super.setDuedate(duedate);;
	}
	@Override
	public Long getTaskId() {
		// TODO Auto-generated method stub
		return super.getTaskId();
	}
	@Override
	public void setTaskId(Long taskId) {
		// TODO Auto-generated method stub
		super.setTaskId(taskId);
	}
	@Override
	public String getStatus() {
		// TODO Auto-generated method stub
		return super.getStatus();
	}
	@Override
	public void setStatus(String status) {
		// TODO Auto-generated method stub
		super.setStatus(status);
	}
	@Override
	public Date getPerformedDate() {
		// TODO Auto-generated method stub
		return super.getPerformedDate();
	}
	@Override
	public void setPerformedDate(Date performedDate) {
		// TODO Auto-generated method stub
		super.setPerformedDate(performedDate);
	}
	@Override
	@JsonManagedReference
	public SupplyBatch getSupplyBatch() {
		// TODO Auto-generated method stub
		return super.getSupplyBatch();
	}
	@Override
	public void setSupplyBatch(SupplyBatch supplyBatch) {
		// TODO Auto-generated method stub
		super.setSupplyBatch(supplyBatch);
	}
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return super.getType();
	}
	@Override
	public void setType(String type) {
		// TODO Auto-generated method stub
		super.setType(type);
	}
	public int getRound() {
		return round;
	}
	public void setRound(int round) {
		this.round = round;
	}
	
	
	
}
