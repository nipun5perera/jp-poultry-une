package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@NamedQueries({@NamedQuery(name=Batch_Medicine.FINDBATCHMEDICINEBYBATCHID,query="SELECT new com.poultry.jp.report.Batch_MedicineReport(bm.batchMedicineId,bm.supplyBatch,bm.medicine,bm.date,bm.quantity) FROM Batch_Medicine bm WHERE bm.supplyBatch.supplyBatchId= :bid")})
public class Batch_Medicine {
	
	public static final String FINDBATCHMEDICINEBYBATCHID ="findBatchMedicineByBatchId";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long batchMedicineId;

	@ManyToOne
	@JoinColumn(name = "supplyBatch_id")
	SupplyBatch supplyBatch;
 
	@ManyToOne
	@JoinColumn(name = "medicine_id")
	Medicine medicine;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	
	private int quantity;
	private double unitPrice;
	private double amount;
	private int billNumber;
	//private double issueUnitPrice;
	private double actualPrice;

	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}

	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public Medicine getMedicine() {
		return medicine;
	}

	public int getQuantity() {
		return quantity;
	}
	public Long getBatchMedicineId() {
		return batchMedicineId;
	}

	public void setBatchMedicineId(Long batchMedicineId) {
		this.batchMedicineId = batchMedicineId;
	}

	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}

	public double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(double actualPrice) {
		this.actualPrice = actualPrice;
	}
}
