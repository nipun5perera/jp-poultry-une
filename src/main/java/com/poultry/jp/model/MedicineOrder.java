package com.poultry.jp.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@NamedQueries({
	@NamedQuery(name = MedicineOrder.FIND_MEDICINE_ORDER_DETAILS, query = "Select new com.poultry.jp.report.MedicineOrderReport(mo.medicineOrderId,mo.date,mo.total,mo.discount,mo.supplier,mo.inStock) from MedicineOrder mo  WHERE mo.isDeleted = false AND mo.inStock = false ")})
public class MedicineOrder {

	
	public static final String FIND_MEDICINE_ORDER_DETAILS ="findMedicineOrderDetails";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long medicineOrderId;

	@OneToMany(mappedBy = "medicineOrder", cascade = CascadeType.ALL,fetch = FetchType.EAGER,orphanRemoval=true)
	private List<MedicineOrderDetails> medicineOrderDetails = new ArrayList<MedicineOrderDetails>();

	@ManyToOne
	private Supplier supplier;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date date;
	
	@NotNull
	private double total;
	@NotNull
	private double discount;
	private boolean inStock;
	
	@OneToOne(cascade={CascadeType.MERGE , CascadeType.PERSIST})
	@JoinColumn(name="paymentRequest_id")
	@Valid
	private PaymentRequest paymentRequest;
	
	private boolean isDeleted;
	private Date created;
	private Date updated;

	protected MedicineOrder() {

	};

	public MedicineOrder(Supplier supplier, double discount, Date date,
			double total) {
		this.discount = discount;
		this.date = date; 
		this.total = total;
		this.supplier = supplier;
	}

	public List<MedicineOrderDetails> getMedicineOrderDetails() {
		return medicineOrderDetails;
	}

	public void setMedicineOrderDetails(
			List<MedicineOrderDetails> medicineOrderDetails) {
		this.medicineOrderDetails = medicineOrderDetails;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getDiscount() {
		return discount;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public double getTotal() {
		return total;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public Long getMedicineOrderId() {
		return medicineOrderId;
	}

	public void setMedicineOrderId(Long medicineOrderId) {
		this.medicineOrderId = medicineOrderId;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public PaymentRequest getPaymentRequest() {
		return paymentRequest;
	}

	public void setPaymentRequest(PaymentRequest paymentRequest) {
		this.paymentRequest = paymentRequest;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@PrePersist
	protected void onCreate() {
		created = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public boolean isInStock() {
		return inStock;
	}

	public void setInStock(boolean inStock) {
		this.inStock = inStock;
	}
	

}
