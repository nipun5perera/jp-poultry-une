package com.poultry.jp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Damaged_Feed_Detail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long damagedFeedDetailId;
	
	@ManyToOne
	private Feed feed;
	
	@ManyToOne
	private Damaged_Feed damagedFeedDetail;
	
	private int quantity;
	
	public Long getDamagedFeedDetailId() {
		return damagedFeedDetailId;
	}
	public void setDamagedFeedDetailId(Long damagedFeedDetailId) {
		this.damagedFeedDetailId = damagedFeedDetailId;
	}
	public Feed getFeed() {
		return feed;
	}
	public void setFeed(Feed feed) {
		this.feed = feed;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
