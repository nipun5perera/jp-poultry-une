package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;



@Entity
@NamedQueries({
@NamedQuery(name=Task.FINDTASKBYPENDINGSTATUS,query="SELECT new com.poultry.jp.report.TaskReport(t.taskId,t.status,t.duedate, t.type ) FROM Task t WHERE t.status = 'Pending' OR t.status = 'Incomplete' ORDER BY t.duedate"),
@NamedQuery(name=Task.GETTHESELECTEDTASK,query="SELECT new com.poultry.jp.report.TaskReport(t.taskId,t.status,t.duedate ,t.supplyBatch,t.type) FROM Task t WHERE t.taskId = :tid"),
@NamedQuery(name=Task.FINDCATCHINGTASKS,query="SELECT new com.poultry.jp.report.Catching_TaskReport(t.taskId,t.status,t.duedate,t.supplyBatch,t.type) FROM Task t WHERE t.type = 'Catching' AND (t.status='Pending' OR t.status='Incomplete')"),
@NamedQuery(name=Task.FINDCATCHINGTASKSBYDATE,query="SELECT new com.poultry.jp.report.Catching_TaskReport(t.taskId,t.status,t.duedate,t.supplyBatch,t.type) FROM Task t WHERE t.duedate = :date AND t.type = 'Catching' AND (t.status='Pending' OR t.status='Incomplete')")
})

@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@batch")
@Inheritance(strategy = InheritanceType.JOINED)
public class Task {
	
	public static final String FINDTASKBYPENDINGSTATUS = "findTaskByPendingStatus";
	public static final String GETTHESELECTEDTASK = "getTheSelectedTask";
	public static final String FINDCATCHINGTASKS = "findCatchingTasks";
	public static final String FINDCATCHINGTASKSBYDATE = "findCatchingTasksByDate";
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long taskId;
	private String status;
	@DateTimeFormat(pattern = "yyyy-MM-dd")	
	@Temporal(TemporalType.DATE)
	private Date duedate;
	private Date performedDate;
	private String type;
	
	@ManyToOne
	private SupplyBatch supplyBatch;
	
	
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDuedate() {
		return duedate;
	}
	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}
	public Date getPerformedDate() {
		return performedDate;
	}
	public void setPerformedDate(Date performedDate) {
		this.performedDate = performedDate;
	}
 
	@JsonManagedReference
	public SupplyBatch getSupplyBatch() {
		return supplyBatch;
	}
	public void setSupplyBatch(SupplyBatch supplyBatch) {
		this.supplyBatch = supplyBatch;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
