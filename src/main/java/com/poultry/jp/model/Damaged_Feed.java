package com.poultry.jp.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Damaged_Feed {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long damageFeedId;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date date;
	@NotEmpty
	private String description;
	
	@OneToMany(mappedBy = "damagedFeedDetail",cascade = CascadeType.ALL ,fetch = FetchType.EAGER,orphanRemoval=true)
	private List<Damaged_Feed_Detail> damagedFeedDetail = new ArrayList<Damaged_Feed_Detail>();
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDamageFeedId() {
		return damageFeedId;
	}
	public void setDamageFeedId(Long damageFeedId) {
		this.damageFeedId = damageFeedId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public List<Damaged_Feed_Detail> getDamagedFeedDetail() {
		return damagedFeedDetail;
	}
	public void setDamagedFeedDetail(List<Damaged_Feed_Detail> damagedFeedDetail) {
		this.damagedFeedDetail = damagedFeedDetail;
	}
	
}
