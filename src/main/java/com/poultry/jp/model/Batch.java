package com.poultry.jp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;



@Entity
@NamedQueries({
	@NamedQuery(name= Batch.GETBATCHREPORT,query="SELECT new com.poultry.jp.report.BatchReport(b.batchId,b.date,b.quantity,b.chickPrice,b.purchaseOrderValue,b.actualOrderValue,b.discount,b.waxingTotal,b.oneChickWaxingCost,b.waxingStatus,b.supplier,b.paymentRequest,b.isApproved) FROM Batch b ")
	
})
//@JsonIgnoreProperties(value = { "farmer" ,"fieldOfficer","supplyBatch"})
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@task")
public class Batch {

	public static final String GETBATCHREPORT = "findBatchReportByFieldOfficer";
	public static final String GET_BATCHDATESANDID = "getBatchDates&Ids";
	public static final String FIND_BATCHBYID ="findBatchById";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long batchId;
	@NotNull
	private int quantity;
	@NotNull
	private double chickPrice;
	@NotNull
	private double purchaseOrderValue;
	@NotNull
	private double actualOrderValue;
	
	private double discount;
	
	private double waxingTotal;
	private double oneChickWaxingCost;
	private boolean waxingStatus;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date date;
	
	@ManyToOne
	private Supplier supplier;
	
	private int issuedAmount;
	
	
	@OneToOne(cascade={CascadeType.MERGE , CascadeType.PERSIST})
	@JoinColumn(name = "paymentRequest_id")
	@Valid
	private PaymentRequest paymentRequest;
	
	private boolean isDeleted;
	private Date created;
	private Date updated;
	
	private boolean isApproved;

	
	public PaymentRequest getPaymentRequest() {
		return paymentRequest;
	}

	public void setPaymentRequest(PaymentRequest paymentRequest) {
		this.paymentRequest = paymentRequest;
	}
	
	public double getActualOrderValue() {
		return actualOrderValue;
	};
	
	public double getChickPrice() {
		return chickPrice;
	}

	public double getDiscount() {
		return discount;
	}

	public double getOneChickWaxingCost() {
		return oneChickWaxingCost;
	}

	public double getPurchaseOrderValue() {
		return purchaseOrderValue;
	}

	public boolean getWaxingStatus() {
		return waxingStatus;
	}

	public double getWaxingTotal() {
		return waxingTotal;
	}

	public void setActualOrderValue(double actualOrderValue) {
		this.actualOrderValue = actualOrderValue;
	}

	
	public void setChickPrice(double chickPrice) {
		this.chickPrice = chickPrice;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public void setOneChickWaxingCost(double oneChickWaxingCost) {
		this.oneChickWaxingCost = oneChickWaxingCost;
	}

	public void setPurchaseOrderValue(double purchaseOrderValue) {
		this.purchaseOrderValue = purchaseOrderValue;
	}
	
	public void setWaxingStatus(boolean waxingStatus) {
		this.waxingStatus = waxingStatus;
	}

	public void setWaxingTotal(double waxingTotal) {
		this.waxingTotal = waxingTotal;
	}

	
	public Long getBatchId() {
		return batchId;
	}
	 
	public int getQuantity() {
		return quantity;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	@PrePersist
	protected void onCreate() {
		created = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public int getIssuedAmount() {
		return issuedAmount;
	}

	public void setIssuedAmount(int issuedAmount) {
		this.issuedAmount = issuedAmount;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
}
