package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.model.Feed;
import com.poultry.jp.report.FeedReport;
import com.poultry.jp.service.FeedService;

@Controller
public class FeedController {

	@Autowired
	private FeedService feedService;;

	@RequestMapping(value = "admin/addFeed", method = RequestMethod.GET)
	public String addFeedGET(@ModelAttribute("feed") Feed feed,Model model) {

		List<FeedReport> feedDetails = feedService.getFeedDetails();

		model.addAttribute("feedDetails", feedDetails);
		return "addFeed";
	}
	
	@RequestMapping(value = "admin/addFeed", method = RequestMethod.POST)
	public String addFeedPOST(@Valid @ModelAttribute("feed") Feed feed,BindingResult result) {
		
		if (result.hasErrors()) {
			return "addFeed";
		} else {
			feedService.save(feed);
			return "redirect:addFeed.html";
		}
	}
	
	@RequestMapping(value = "admin/updateFeed", method = RequestMethod.GET)
	public String updateFeedGET(@ModelAttribute("feed") Feed feed,Model model,HttpServletRequest reqeust) {

		Long id = new Long(reqeust.getParameter("fid"));
		Feed feedOb = feedService.getFeed(id);
		model.addAttribute("feed", feedOb);
		return "updateFeed";
	}
	
	@RequestMapping(value = "admin/updateFeed", method = RequestMethod.POST)
	public String updateFeedPOST(@ModelAttribute("feed") Feed feed) {
		
		feedService.update(feed);

		return "redirect:addFeed.html";
	}

	@RequestMapping(value = "user/getFeedDetails", method = RequestMethod.GET)
	public String getFeedDetails(@ModelAttribute("feed") Feed feed, Model model) {

		List<FeedReport> feedDetails = feedService.getFeedDetails();

		model.addAttribute("feedDetails", feedDetails);

		return "getFeedDetails";
	}
	
	@RequestMapping(value = "admin/deleteFeed", method = RequestMethod.GET)
	public String deleteFeedGET(@RequestParam("fid")String fid)
	{
		Long id = new Long(fid);
		feedService.delete(id);
		return "redirect:addFeed.html";
	}
	
	@RequestMapping(value = {"admin/getFeedObject","user/getFeedObject"}, method = RequestMethod.GET)
	public @ResponseBody Feed getFeedObject(@RequestParam("fid")String fid)
	{
		Long id = new Long(fid);
		Feed feed = feedService.getFeed(id);
		return feed;
	}


	 

}
