package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poultry.jp.model.FeedOrder;
import com.poultry.jp.report.FeedOrderReport;
import com.poultry.jp.report.FeedReport;
import com.poultry.jp.report.PaymentRequestReport;
import com.poultry.jp.report.SupplierReport;
import com.poultry.jp.service.FeedOrderService;
import com.poultry.jp.service.FeedService;
import com.poultry.jp.service.PaymentRequestService;
import com.poultry.jp.service.SupplierService;

@Controller
public class FeedOrderController {

	@Autowired
	private FeedOrderService feedOrderService;

	@Autowired
	private SupplierService supplierService;

	@Autowired
	private PaymentRequestService paymentRequestSerivice;

	@Autowired
	private FeedService feedService;

	@RequestMapping(value = {"user/addFeedOrder","admin/addFeedOrder"}, method = RequestMethod.GET)
	public String addFeedOrderGET(
			@ModelAttribute("feedOrder") FeedOrder feedOrder, Model model) {

		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<FeedOrderReport> feedOrderReport = feedOrderService.getFeedOrderDetails();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivice.getPaymentRequestDates();
		List<FeedReport> feedReport = feedService.getFeedDetails();

		model.addAttribute("feedOrderReport", feedOrderReport);
		model.addAttribute("supplierReport", supplierReport);
		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("feedReport", feedReport);

		return "addFeedOrder";
	}
	
	@RequestMapping(value = {"user/addFeedOrder","admin/addFeedOrder"}, method = RequestMethod.POST)
	public String addFeedOrderPOST(@Valid @ModelAttribute("feedOrder") FeedOrder feedOrder,BindingResult result, Model model) {

		if (result.hasErrors()) {
			return addFeedOrderGET(feedOrder, model);
		} else {
			feedOrderService.save(feedOrder);
			// paymentRequestSerivice.setPaymentRequestIsPaidStatus(feedOrder.getPaymentRequest().getPaymentId());
			return "redirect:addFeedOrder.html";
		}
	}

	@RequestMapping(value = "admin/updateFeedOrder", method = RequestMethod.GET)
	public String updateFeedOrderGET(
			@ModelAttribute("feedOrder") FeedOrder feedOrder, Model model,HttpServletRequest request) {

		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<FeedOrderReport> feedOrderReport = feedOrderService.getFeedOrderDetails();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivice.getPaymentRequestDates();
		List<FeedReport> feedReport = feedService.getFeedDetails();
		
		Long id = new Long(request.getParameter("foid"));
		FeedOrder fo = feedOrderService.getFeedOrder(id);
//		System.out.println("Payment request ID--"+fo.getPaymentRequest().getPaymentId());
		model.addAttribute("feedOrder", fo);
		model.addAttribute("feedOrderReport", feedOrderReport);
		model.addAttribute("supplierReport", supplierReport);
		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("feedReport", feedReport);

		return "updateFeedOrder";
	}
	
	@RequestMapping(value = "admin/updateFeedOrder", method = RequestMethod.POST)
	public String updateFeedOrderPOST(@ModelAttribute("feedOrder") FeedOrder feedOrder) {
		
		feedOrderService.updateFeedOrder(feedOrder);
		 

		return "redirect:addFeedOrder.html";
	}
	
	@RequestMapping(value = "admin/deleteFeedOrder", method = RequestMethod.GET)
	public String deleteFeedOrderGET(@RequestParam("foid")String foid)
	{
		Long id = new Long(foid);
		feedOrderService.delete(id);
		return "redirect:addFeedOrder.html";
	}
	
	@RequestMapping(value = {"user/getFeedOrderDetails","admin/getFeedOrderDetails"}, method = RequestMethod.GET)
	public String getFeedOrderDetails(@ModelAttribute("feedOrder") FeedOrder feedOrder, Model model) {

		List<FeedOrderReport> feedOrderReport = feedOrderService.getFeedOrderDetails();
		model.addAttribute("feedOrderReport", feedOrderReport);
		return "getFeedOrderDetails";
	}
	
	@RequestMapping(value="admin/updateFeedOrderInStockStatus",method = RequestMethod.GET)
	public String updateFeedOrderInStockStatus(@RequestParam("foid")String foid)
	{
		Long id = new Long(foid);
		feedOrderService.updateFeedOrderInStockStatus(id);
		return "redirect:addFeedOrder.html";
	}
	
	@RequestMapping(value = "admin/approveFeedOrder", method = RequestMethod.GET)
	public String approveFeedOrderGET(@ModelAttribute("feedOrder") FeedOrder feedOrder, Model model,HttpServletRequest request) {

		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<FeedOrderReport> feedOrderReport = feedOrderService.getFeedOrderDetails();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivice.getPaymentRequestDates();
		List<FeedReport> feedReport = feedService.getFeedDetails();
		
		Long id = new Long(request.getParameter("foid"));
		FeedOrder fo = feedOrderService.getFeedOrder(id);
		model.addAttribute("feedOrder", fo);
		model.addAttribute("feedOrderReport", feedOrderReport);
		model.addAttribute("supplierReport", supplierReport);
		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("feedReport", feedReport);

		return "approveFeedOrder";
	}

	@RequestMapping(value = "admin/approveFeedOrder", method = RequestMethod.POST)
	public String approveFeedOrderPOST(@ModelAttribute("feedOrder") FeedOrder feedOrder ) {

		feedOrderService.approveFeedOrder(feedOrder);

		return "redirect:addFeedOrder.html";
	}
}
