package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.model.Supplier;
import com.poultry.jp.report.SupplierReport;
import com.poultry.jp.service.SupplierService;

@Controller
public class SupplierController {
	
	@Autowired
	private SupplierService supplierService;
	
	@RequestMapping(value="admin/addSupplier",method = RequestMethod.GET)
	public String addSupplierGET(@ModelAttribute("supplier")Supplier supplier,Model model) {
		
		List<SupplierReport> supplierReport = supplierService.getSupplierDetails();
		
		model.addAttribute("supplierReport",supplierReport);
	
		return "addSupplier";
		
	}
	@RequestMapping(value="admin/addSupplier",method = RequestMethod.POST)
	public String addSupplierPOST(@Valid  @ModelAttribute("supplier")Supplier supplier,BindingResult result) {
	
		if (result.hasErrors()) {
			return "addSupplier";
		} else {
			supplierService.save(supplier);
			return "redirect:addSupplier.html";
		}
	}
	
	@RequestMapping(value="admin/updateSupplier",method = RequestMethod.GET)
	public String updateSupplierGET(@ModelAttribute("supplier")Supplier supplier,HttpServletRequest request,Model model) {
		
		Long id = new Long(request.getParameter("sid"));
		Supplier supplier1 = supplierService.getSupplierObject(id);
		
		model.addAttribute("supplier",supplier1);
	
		return "updateSupplier";
		
	}
	@RequestMapping(value="admin/updateSupplier",method = RequestMethod.POST)
	public String updateSupplierPOST(@ModelAttribute("supplier")Supplier supplier) {
	
		supplierService.update(supplier);
		
		return "redirect:addSupplier.html";
		
	}
	
	@RequestMapping(value = "admin/deleteSupplier", method = RequestMethod.GET)
	public String deleteMedicineOrderGET(@RequestParam("sid")String sid)
	{
		Long id = new Long(sid);
		supplierService.delete(id);
		return "redirect:addSupplier.html";
	}
	
	
	
	
	
	
	
	@RequestMapping(value="user/getSupplierReport",method = RequestMethod.GET)
	public String getSupplierReport(Model model) {
	
		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		
		model.addAttribute("supplierReport",supplierReport);
		
		return "getSupplierReport";
		
	}
	
	
	@RequestMapping(value = "user/getSupplierObject", method = RequestMethod.GET)
	public @ResponseBody Supplier getSupplierObject(@RequestParam("sid") String sid) {
		System.out.println("came here with pid "+sid);

		Long id = new Long(sid);

		Supplier supplier = supplierService.getSupplierObject(id);

		return supplier;

	}

}
