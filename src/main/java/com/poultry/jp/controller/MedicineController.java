package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.model.Medicine;
import com.poultry.jp.report.MedicineReport;
import com.poultry.jp.service.MedicineService;

@Controller
public class MedicineController {
	
	
	@Autowired
	private MedicineService medicineService;
	
	@RequestMapping(value="admin/addMedicine",method = RequestMethod.GET)
	public String addMedicineGET(@ModelAttribute("medicine")Medicine supplier,Model model) {
		
		List<MedicineReport> medicineReport = medicineService.getMedicineDetails();
		model.addAttribute("medicineReport", medicineReport);
	
		return "addMedicine";
		
	}
	
	@RequestMapping(value="admin/addMedicine",method = RequestMethod.POST)
	public String addMedicinePOST(@Valid @ModelAttribute("medicine")Medicine medicine,BindingResult result) {

		if (result.hasErrors()) {
			return "addMedicine";
		} else {
			medicineService.save(medicine);
			return "redirect:addMedicine.html";
		}
	}
	
	@RequestMapping(value="admin/updateMedicine",method = RequestMethod.GET)
	public String updateMedicineGET(@ModelAttribute("medicine")Medicine supplier,Model model,HttpServletRequest request) {
		
		Long id = new Long(request.getParameter("mid"));
		Medicine medicine = medicineService.getMedicine(id);
		model.addAttribute("medicine", medicine);
	
		return "updateMedicine";
		
	}
	
	@RequestMapping(value="admin/updateMedicine",method = RequestMethod.POST)
	public String updateMedicinePOST(@ModelAttribute("medicine")Medicine medicine) {
	
		medicineService.update(medicine);
		
		return "redirect:addMedicine.html";
	}
	
	@RequestMapping(value="user/getMedicineDetails",method = RequestMethod.GET)
	public String getMedicineDetails(@ModelAttribute("medicine")Medicine supplier,Model model) {
		
		List<MedicineReport> medicineReport = medicineService.getMedicineDetails();
		model.addAttribute("medicineReport", medicineReport);
	
		return "getMedicineDetails";
		
	}
	
	@RequestMapping(value = "admin/deleteMedicine", method = RequestMethod.GET)
	public String deleteMedicineGET(@RequestParam("mid")String mid)
	{
		Long id = new Long(mid);
		medicineService.delete(id);
		return "redirect:addMedicine.html";
	}

	@RequestMapping(value = {"admin/getMedicineObject","user/getMedicineObject"}, method = RequestMethod.GET)
	public @ResponseBody Medicine getMedicineObject(@RequestParam("mid")String fid)
	{
		Long id = new Long(fid);
		Medicine medicine = medicineService.getMedicine(id);
		return medicine;
	}
	
}
