package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.poultry.jp.report.BatchReport;
import com.poultry.jp.report.FarmerReport;
import com.poultry.jp.report.FieldOfficerReport;
import com.poultry.jp.report.PaymentRequestReport;
import com.poultry.jp.report.SupplierReport;
import com.poultry.jp.model.SupplyBatch;
import com.poultry.jp.report.SupplyBatchReport;
import com.poultry.jp.service.BatchService;
import com.poultry.jp.service.FarmerService;
import com.poultry.jp.service.FieldOfficerService;
import com.poultry.jp.service.PaymentRequestService;
import com.poultry.jp.service.SupplierService;
import com.poultry.jp.service.SupplyBatchService;

@Controller
public class SupplyBatchController {
	
	@Autowired
	private SupplyBatchService supplyBatchService;
	
	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private FieldOfficerService fieldOfficerService;
	
	@Autowired
	private FarmerService farmerService;
	
	@Autowired
	private PaymentRequestService paymentRequestService;
	
	@Autowired
	private BatchService batchService;
	
	@Autowired
	private BatchService batch;
	
	@RequestMapping(value= {"user/addSupplyBatch","admin/addSupplyBatch"}, method = RequestMethod.GET)
	public String saveSupplyBatchGET(@ModelAttribute("supplyBatch")SupplyBatch supplyBatch,Model model){
		
		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<FarmerReport> farmerReport = farmerService.getAvailableFamers();
		List<FieldOfficerReport> fieldOfficerReport = fieldOfficerService.getFieldOfficerReport();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestService.getPaymentRequestDates();
		List<BatchReport> batchReport = batchService.getBatchDatesAndIds();

		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("supplierReport", supplierReport);
		model.addAttribute("farmerReport", farmerReport);
		model.addAttribute("fieldOfficerReport", fieldOfficerReport);
		model.addAttribute("batchReport", batchReport);
		
		return "addSupplyBatch";
	}
	
	@RequestMapping(value={"user/addSupplyBatch","admin/addSupplyBatch"}, method = RequestMethod.POST)
	public String saveSupplyBatchPOST(@Valid @ModelAttribute("supplyBatch")SupplyBatch supplyBatch,BindingResult result){
		
		if (result.hasErrors()) {
			return "addSupplyBatch";
		} else {
			System.out.println("Field Officer ID:"
					+ supplyBatch.getFieldOfficer());
			supplyBatchService.save(supplyBatch);
			return "redirect:viewSupplyBatch.html?sbid="
					+ supplyBatch.getSupplyBatchId();
		}
	}
	
	
	
	@RequestMapping(value= "admin/updateSupplyBatch",method = RequestMethod.GET)
	public String updateSupplyBathGET(HttpServletRequest request,Model model)
	{
 
		String pid = request.getParameter("sbid");
		Long id = new Long(pid);
		SupplyBatch  supplyBatch = supplyBatchService.getSupplyBatch(id);
		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<FarmerReport> farmerReport = farmerService.getFarmerReport();
		List<FieldOfficerReport> fieldOfficerReport = fieldOfficerService.getFieldOfficerReport();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestService.getPaymentRequestDates();

		

		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("supplierReport", supplierReport);
		model.addAttribute("farmerReport", farmerReport);
		model.addAttribute("fieldOfficerReport", fieldOfficerReport);
		model.addAttribute("supplyBatch", supplyBatch);
		return "updateSupplyBatch";
	}
	
	@RequestMapping(value="admin/updateSupplyBatch",method = RequestMethod.POST)
	public String updateSupplyBathPOST(@ModelAttribute("supplyBatch")SupplyBatch supplyBatch)
	{
 
		supplyBatchService.updateSupplyBatch(supplyBatch);
		return "getSupplyBatches";
	}
	
	
	
	@RequestMapping(value= {"admin/getSupplyBatches","user/getSupplyBatches"}, method = RequestMethod.GET)
	public String getSupplyBatch(@ModelAttribute("supplyBatch")SupplyBatch supplyBatch,Model model){
		
		List<SupplyBatchReport> supplyBatchReport = supplyBatchService.getSupplyBatches();

		model.addAttribute("supplyBatchReport", supplyBatchReport);
		
		return "getSupplyBatches";
	}
	
	@RequestMapping(value="admin/deleteSupplyBatch",method = RequestMethod.GET)
	public String deleteSupplyBatchGET(@RequestParam("sbid")String sbid)
	{
		Long id = new Long(sbid);
		supplyBatchService.deleteSupplyBatch(id);
		return "redirect:getSupplyBatches.html";
	}
	
	@RequestMapping(value= {"admin/viewSupplyBatch","user/viewSupplyBatch"},method = RequestMethod.GET)
	public String viewSupplyBatch(@RequestParam("sbid")String sbid,Model model,RedirectAttributes redirectAttributes)
	{
		Long id = new Long(sbid);
		SupplyBatch supplyBatch = supplyBatchService.getSupplyBatch(id);
		
		model.addAttribute("supplyBatch", supplyBatch);
		model.addAttribute("message","Distribute batch added successfully.");
		
		return "viewSupplyBatch";
	}
}
