package com.poultry.jp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poultry.jp.report.Batch_FeedReport;
import com.poultry.jp.report.FeedReport;
import com.poultry.jp.model.FeedReturn;
import com.poultry.jp.service.BatchService;
import com.poultry.jp.service.Batch_FeedService;
import com.poultry.jp.service.FeedReturnService;
import com.poultry.jp.service.FeedService;

@Controller
public class FeedReturnController {
	
	@Autowired
	private FeedReturnService feedReturnService;
	
	@Autowired
	private BatchService batchService;
	
	@Autowired
	private Batch_FeedService batchFeedService;
	

	@Autowired
	private FeedService feedService;
	
	
	
	
	@RequestMapping(value = {"user/addFeedReturn","admin/addFeedReturn"}, method = RequestMethod.GET)
	public String addFeedReturn(@ModelAttribute("feedReturn") FeedReturn feedReturn, Model model,@RequestParam("bid")String bid) {
		
//		Long l = new Long(1);
		//Batch batch = batchService.getBatchObject(l);
		
		List<Batch_FeedReport> batchFeedReport = batchFeedService.getBatchFeeds(new Long(bid));
		model.addAttribute("batchFeedReport", batchFeedReport);
		
		for(Batch_FeedReport report: batchFeedReport)
		{
			System.out.println("Feed"+report.getFeed().getManufacturer());
		}
		
		List<FeedReport> feedReport = feedService.getFeedDetails();
		model.addAttribute("feedReport",feedReport);

		return "addFeedReturn";
	}
	
	@RequestMapping(value = {"user/addFeedReturn","admin/addFeedReturn"}, method = RequestMethod.POST)
	public String updateFeedReturn(
			@ModelAttribute("feedReturn") FeedReturn feedReturn, Model model) {

		feedReturnService.save(feedReturn);

		return "getSupplyBatches";
	}

}
