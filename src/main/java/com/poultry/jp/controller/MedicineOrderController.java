package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poultry.jp.model.MedicineOrder;
import com.poultry.jp.report.MedicineOrderReport;
import com.poultry.jp.report.MedicineReport;
import com.poultry.jp.report.PaymentRequestReport;
import com.poultry.jp.report.SupplierReport;
import com.poultry.jp.service.MedicineOrderService;
import com.poultry.jp.service.MedicineService;
import com.poultry.jp.service.PaymentRequestService;
import com.poultry.jp.service.SupplierService;

@Controller
public class MedicineOrderController {

	@Autowired
	private MedicineOrderService medicineOrderService;

	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private PaymentRequestService paymentRequestSerivice;
	
	@Autowired
	private MedicineService medicineService;
	

	@RequestMapping(value =  {"user/addMedicineOrder","admin/addMedicineOrder"}, method = RequestMethod.GET)
	public String addMedicineOrderGET(@ModelAttribute("medicineOrder") MedicineOrder medicineOrder,Model model) {

		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<MedicineOrderReport> medicineOrderReport = medicineOrderService.getMedicineOrderDetails();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivice.getPaymentRequestDates();
		List<MedicineReport> medicineReport = medicineService.getMedicineDetails();
		
		model.addAttribute("medicineOrderReport",medicineOrderReport);
		model.addAttribute("supplierReport", supplierReport);
		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("medicineReport", medicineReport);

		return "addMedicineOrder";
	}
	
	@RequestMapping(value = {"user/addMedicineOrder","admin/addMedicineOrder"}, method = RequestMethod.POST)
	public String addMedicineOrderPOST(@Valid @ModelAttribute("medicineOrder") MedicineOrder medicineOrder,BindingResult result,Model model) {
 
		
		if (result.hasErrors()) {
			return addMedicineOrderGET(medicineOrder, model);
		} else {
			//paymentRequestSerivice.setPaymentRequestIsPaidStatus(medicineOrder.getPaymentRequest().getPaymentId());
			medicineOrderService.save(medicineOrder);
			

			return "redirect:addMedicineOrder.html";
		}
	}
	

	@RequestMapping(value = {"user/getMedicineOrder","admin/getMedicineOrder"}, method = RequestMethod.GET)
	public String getMedicineOrder(@ModelAttribute("medicineOrder") MedicineOrder medicineOrder,Model model) {

		List<MedicineOrderReport> medicineOrderReport = medicineOrderService.getMedicineOrderDetails();
//		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivice.getPaymentRequestDates();
		
		model.addAttribute("medicineOrderReport",medicineOrderReport);
//		model.addAttribute("paymentRequestReport", paymentRequestReport);

		return "getMedicineOrder";
	}
	
	
	
	@RequestMapping(value = "admin/updateMedicineOrder", method = RequestMethod.GET)
	public String updateMedicineOrderGET( Model model,HttpServletRequest request) {

		Long id = new Long(request.getParameter("moid"));
		MedicineOrder mo = medicineOrderService.getMedicineOrder(id);
		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<MedicineOrderReport> medicineOrderReport = medicineOrderService.getMedicineOrderDetails();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivice.getPaymentRequestDates();
		List<MedicineReport> medicineReport = medicineService.getMedicineDetails();
		
		model.addAttribute("medicineOrder",mo);
		model.addAttribute("medicineOrderReport",medicineOrderReport);
		model.addAttribute("supplierReport", supplierReport);
		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("medicineReport", medicineReport);

		return "updateMedicineOrder";
	}
	
	@RequestMapping(value = "admin/updateMedicineOrder", method = RequestMethod.POST)
	public String updateMedicineOrderPOST(@ModelAttribute("medicineOrder") MedicineOrder medicineOrder,Model model) {
		
		medicineOrderService.updateMedicine(medicineOrder);

		return "redirect:addMedicineOrder.html";
	}
	
	@RequestMapping(value = "admin/deleteMedicineOrder", method = RequestMethod.GET)
	public String deleteMedicineOrderGET(@RequestParam("moid")String moid)
	{
		Long id = new Long(moid);
		medicineOrderService.delete(id);
		return "redirect:addMedicineOrder.html";
	}
	
	
	@RequestMapping(value="admin/updateMedicineOrderInStockStatus",method = RequestMethod.GET)
	public String updateMedicineOrderInStockStatus(@RequestParam("moid")String moid)
	{
		Long id = new Long(moid);
		medicineOrderService.updateMedicineOrderInstock(id);
		return "redirect:addMedicineOrder.html";
	}

}
