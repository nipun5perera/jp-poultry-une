/**
 * 
 */
package com.poultry.jp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.poultry.jp.model.MedicineOrderDetails;
import com.poultry.jp.service.MedicineOrderDetailsService;

/**
 * @author Nipun Perera
 * @created 25 Jul 2015
 *
 */
@Controller
public class MedicineOrderDetailsController {
	
	@Autowired
	private MedicineOrderDetailsService medicineOrderDetailsService;
	
	@RequestMapping(value="addMedicineOrderDetails", method = RequestMethod.GET)
	public String addMedicineOrderDetails(@ModelAttribute("medicineOrderDetails")MedicineOrderDetails medicineOrderDetails){
		
		return "addMedicineOrderDetails";
	}
	
	@RequestMapping(value="addMedicineOrderDetails",method = RequestMethod.POST)
	public String updateMedicineOrderDetails(@ModelAttribute("medicineOrderDetails")MedicineOrderDetails medicineOrderDetails){
		
		medicineOrderDetailsService.save(medicineOrderDetails);
		return "addMedicineOrderDetails";
	}
}
