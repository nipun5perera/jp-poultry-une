package com.poultry.jp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.poultry.jp.model.Damaged_Feed;
import com.poultry.jp.report.FeedReport;
import com.poultry.jp.service.DamagedFeedService;
import com.poultry.jp.service.FeedService;

@Controller
public class Damaged_FeedController {
	
	@Autowired
	private DamagedFeedService damagedFeedService;
	
	@Autowired
	private FeedService feedService;;
	
	@RequestMapping(value="admin/addDamagedFeed",method = RequestMethod.GET)
	public String addDamagedFeedGET(@ModelAttribute("damagedFeed")Damaged_Feed damagedFeed,Model model) {
		
		List<FeedReport> feedReport = feedService.getFeedDetails();

		model.addAttribute("feedReport", feedReport);
		
		return "addDamagedFeed";
		
	}
	
	@RequestMapping(value="admin/addDamagedFeed",method = RequestMethod.POST)
	public String addDamagedFeedPOST(@Valid @ModelAttribute("damagedFeed")Damaged_Feed damagedFeed,BindingResult result) {
	
		if (result.hasErrors()) {
			return "addDamagedFeed";
		} else {
			damagedFeedService.saveDamagedFeed(damagedFeed);
			return "redirect:addDamagedFeed.html";
		}
	}

}
