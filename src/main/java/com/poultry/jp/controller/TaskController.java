package com.poultry.jp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.report.Catching_TaskReport;
import com.poultry.jp.report.FeedIssue_TaskReport;
import com.poultry.jp.report.TaskReport;
import com.poultry.jp.service.TaskService;


@Controller
public class TaskController {
	
	 @Autowired
	 private TaskService taskService;
	 
	 
	 @RequestMapping(value= {"admin/getTaskReport","user/getTaskReport"},method = RequestMethod.GET)
	 public String getTaskReport(Model model,HttpServletRequest request) {
		 
		
		List<FeedIssue_TaskReport> taskReport = taskService.getFeedIssueTasks();

		model.addAttribute("taskReport", taskReport);

		return "getTaskReport";

		}
	 
	 @RequestMapping(value= {"admin/getCatchingTaskReport","user/getCatchingTaskReport"},method = RequestMethod.GET)
	 public String getCatchingTaskReport(Model model,HttpServletRequest request) {
		 
		
		List<Catching_TaskReport> taskReport = taskService.getCatchingTasks();

		model.addAttribute("taskReport", taskReport);

		return "getCatchingTaskReport";

		}

	@RequestMapping(value = {"admin/getTaskReport","user/getTaskReport"}, method = RequestMethod.POST)
	public String renderCompleteTask(Model model,HttpServletRequest request) {

		System.out.println("rerquest ====="+ request.getParameter("thhh"));
		

//		Task task = taskService.getSelectedTask(tid);
//		
//		model.addAttribute("task", task);

		return "addBatchFeed";

	}
	 
	@RequestMapping(value = {"admin/getTaskByDate","user/getTaskByDate"}, method = RequestMethod.GET)
	public @ResponseBody List<FeedIssue_TaskReport> getTaskByDate(@RequestParam("date") String date) {
		 

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dat = null;
		try {
			dat = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		List<FeedIssue_TaskReport> tasksByDate = taskService.getFeedIssueTasksByDate(dat);
		
		for(FeedIssue_TaskReport t : tasksByDate)
		{
			System.out.println("Farmers-"+t.getSupplyBatch().getFarmer().getName());
		}

 

		return tasksByDate;

	}
	
	@RequestMapping(value = {"admin/getCatchingTaskByDate","user/getCatchingTaskByDate"}, method = RequestMethod.GET)
	public @ResponseBody List<TaskReport> getCatchingTaskByDate(@RequestParam("date") String date) {
		 

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dat = null;
		try {
			dat = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		List<TaskReport> tasksByDate = taskService.getCatchingTasksByDate(dat);

 

		return tasksByDate;

	}



}
