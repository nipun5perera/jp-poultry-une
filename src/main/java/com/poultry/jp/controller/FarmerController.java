package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poultry.jp.model.Farmer;
import com.poultry.jp.report.FarmerReport;
import com.poultry.jp.service.FarmerService;

 
@Controller
public class FarmerController {

	@Autowired
	private FarmerService farmerService;

	@RequestMapping(value = "admin/addFarmer", method = RequestMethod.GET)
	public String addFarmerGET(@ModelAttribute("farmer") Farmer farmer, Model model) {

		List<FarmerReport> farmerReport = farmerService.getFamerDetails();

		model.addAttribute("farmerReport", farmerReport);
		 
		return "addFarmer";
	}

	@RequestMapping(value = "admin/addFarmer", method = RequestMethod.POST)
	public String addFarmerPOST(@Valid @ModelAttribute("farmer") Farmer farmer,BindingResult result) {

		if (result.hasErrors()) {
			return "addFarmer";
		} else {
			farmerService.save(farmer);
			return "redirect:addFarmer.html";
		}
	}
	
	
	@RequestMapping(value = "admin/updateFarmer", method = RequestMethod.GET)
	public String updateFarmerGET(@ModelAttribute("farmer") Farmer farmer,HttpServletRequest request, Model model) {

		Long id = new Long(request.getParameter("fid"));
		Farmer farmerOb= farmerService.getFarmer(id);

		model.addAttribute("farmer", farmerOb);
		 
		return "updateFarmer";
	}

	@RequestMapping(value = "admin/updateFarmer", method = RequestMethod.POST)
	public String updateFarmerPOST(@ModelAttribute("farmer") Farmer farmer) {

		farmerService.update(farmer);

		return "redirect:addFarmer.html";
	}
	
	@RequestMapping(value = "admin/deleteFarmer", method = RequestMethod.GET)
	public String deleteFarmerGET(@RequestParam("fid")String fid)
	{
		Long id = new Long(fid);
		farmerService.delete(id);
		return "redirect:addFarmer.html";
	}
	

	@RequestMapping(value = "user/getFarmerDetails", method = RequestMethod.GET)
	public String getFarmerDetails(Model model) {
		List<FarmerReport> farmerReport = farmerService.getFamerDetails();
		model.addAttribute("farmerReport", farmerReport);
		return "getFarmerDetails";
	}
	
	
	
	
}
