/**
 * 
 */
package com.poultry.jp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.model.Batch;
import com.poultry.jp.report.BatchReport;
import com.poultry.jp.report.PaymentRequestReport;
import com.poultry.jp.report.SupplierReport;
import com.poultry.jp.service.BatchService;
import com.poultry.jp.service.PaymentRequestService;
import com.poultry.jp.service.SupplierService;

@Controller
public class BatchController {

	@Autowired
	private BatchService batchService;
	
	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private PaymentRequestService paymentRequestService;
	
	
	@RequestMapping(value= {"user/addBatch","admin/addBatch"}, method = RequestMethod.GET)
	public String addBatch(@ModelAttribute("batch")Batch batch,Model model){
		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestService.getPaymentRequestDates();

		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("supplierReport", supplierReport);
		return "addBatch";
	}
	
	@RequestMapping(value= {"user/addBatch","admin/addBatch"}, method = RequestMethod.POST)
	public String updateBatch(@Valid @ModelAttribute("batch")Batch batch,BindingResult result){
		
		if (result.hasErrors()) {
			return "addBatch";
		} else {
			Batch bat = batchService.save(batch);

			paymentRequestService.setPaymentRequestIsPaidStatus(batch
					.getPaymentRequest().getPaymentId());
			return "redirect:viewBatch.html?bid=" + bat.getBatchId();
		}
	}
	
	@RequestMapping(value= {"user/getBatchReport","admin/getBatchReport"},method = RequestMethod.GET)
	public String getBatchReport(Model model) {
	
/*		List<SupplierReport> supplierReport = supplierService.getSupplierNames();
		List<PaymentRequestReport> paymentRequestReport = paymentRequestService.getPaymentRequestDates();

		model.addAttribute("paymentRequestReport", paymentRequestReport);
		model.addAttribute("supplierReport", supplierReport);
*/		

		List<BatchReport> batchReport = batchService.getBatchReport();
		model.addAttribute("batchReport", batchReport);
		
		return "getBatchReport";
		
	}
	
	
	@RequestMapping(value = {"admin/getBatchObject","user/getBatchObject"}, method = RequestMethod.GET)
	public @ResponseBody Batch getBatchObject(@RequestParam("bid") String bid) {
		System.out.println("came here with pid "+bid);

		Long id = new Long(bid);

		Batch batch = batchService.getBatchObject(id);

		return batch;

	}
	
	@RequestMapping(value= {"user/viewBatch","admin/viewBatch"},method = RequestMethod.GET)
	public String viewBatch(Model model,String bid) {
	
		Long id = new Long(bid);
		Batch batch = batchService.getBatchObject(id);

		model.addAttribute("batch", batch);
		model.addAttribute("message", "Batch added successfully.");
		
		
		return "viewBatch";
		
	}
	
	@RequestMapping(value= {"admin/approveBatch"},method = RequestMethod.GET)
	public String approveBatch(Model model,String bid) {
	
		Batch batch = new Batch();
		batch.setBatchId(new Long(bid));
		batchService.approveBatch(batch);
		return "getBatchReport";
		
	}
	
	
	
}
