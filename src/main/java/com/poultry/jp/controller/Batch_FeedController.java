package com.poultry.jp.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poultry.jp.model.Batch_Feed;
import com.poultry.jp.report.Batch_FeedReport;
import com.poultry.jp.report.FeedReport;
import com.poultry.jp.model.Task;
import com.poultry.jp.service.Batch_FeedService;
import com.poultry.jp.service.FeedService;
import com.poultry.jp.service.TaskService;

@Controller
public class Batch_FeedController {
	
	@Autowired
	private Batch_FeedService batchFeedService;
	
	@Autowired
	private FeedService feedService;
	
	@Autowired
	private TaskService taskService;
	
	
	@RequestMapping(value={"admin/addBatchFeed","user/addBatchFeed"}, method = RequestMethod.GET)
	public String addBatchFeed(@RequestParam("bid")String bid,@ModelAttribute("batchFeed")Batch_Feed batchFeed,Model model){
		Long batchId = new Long(bid);
		List<FeedReport> feedReport = feedService.getFeedDetails();
		List<Batch_FeedReport> batchFeedReport = batchFeedService.getBatchFeeds(batchId);
		model.addAttribute("batchFeedReport", batchFeedReport);
		model.addAttribute("feedReport",feedReport);
		System.out.println("GET Method");
		return "addBatchFeed";
	}
	
	@RequestMapping(value= {"admin/addBatchFeed","user/addBatchFeed"}, method = RequestMethod.POST)
	public String updateBatchFeed(@ModelAttribute("batchFeed")Batch_Feed batchFeed,HttpServletRequest request){
		System.out.println("POST Methods");
		Date performedDate = batchFeed.getDate();
		Long id = batchFeed.getTask().getTaskId();
//		int quantity = new Integer(request.getParameter("quantity"));
		String status = request.getParameter("status");
		
		System.out.println("Status "+status);
		System.out.println("-----------------------------\n Task id "+batchFeed.getTask().getTaskId());
		
		Task task = new Task();
		task.setPerformedDate(performedDate);
		task.setTaskId(id);
		task.setStatus(status);
		task.setSupplyBatch(batchFeed.getSupplyBatch());
		
		
		taskService.updateTaskStatus(task);
		batchFeedService.save(batchFeed);
		return "redirect:getTaskReport.html";
	}
	
	//add additional batch feed
	@RequestMapping(value="admin/addAdditionalBatchFeed", method = RequestMethod.GET)
	public String addAdditionalBatchFeed(@RequestParam("bid")String bid,@ModelAttribute("batchFeed")Batch_Feed batchFeed,Model model){
		Long batchId = new Long(bid);
		List<FeedReport> feedReport = feedService.getFeedDetails();
		List<Batch_FeedReport> batchFeedReport = batchFeedService.getBatchFeeds(batchId);
		model.addAttribute("batchFeedReport", batchFeedReport);
		model.addAttribute("feedReport",feedReport);
		System.out.println("GET Method");
		return "addAdditionalBatchFeed";
	}
	
	@RequestMapping(value="admin/addAdditionalBatchFeed", method = RequestMethod.POST)
	public String addAdditionalBatchFeed(@ModelAttribute("batchFeed")Batch_Feed batchFeed,HttpServletRequest request){

		batchFeedService.save(batchFeed);
		return "redirect:getTaskReport.html";
	}
	
	@RequestMapping(value= {"user/getBatchFeedReport","admin/getBatchFeedReport"},method=RequestMethod.GET)
	public String getBatchFeedReport(Model model,@RequestParam("bid")String bid)
	{
		Long batchId = new Long(bid);
		
		List<Batch_FeedReport> batchFeedReport = batchFeedService.getBatchFeeds(batchId);
		model.addAttribute("batchFeedReport", batchFeedReport);
		
		return  "getBatchFeedReport";
	}

}
