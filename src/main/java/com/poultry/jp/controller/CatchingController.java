package com.poultry.jp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poultry.jp.model.Catching;
import com.poultry.jp.model.Task;
import com.poultry.jp.service.CatchingService;
import com.poultry.jp.service.FieldReportService;
import com.poultry.jp.service.TaskService;

@Controller
public class CatchingController {
	
	@Autowired
	private CatchingService catchingService;
	
	@Autowired
	private FieldReportService fieldReportService;
	
	@Autowired
	private TaskService taskService;

	@RequestMapping(value= {"user/addCatching","admin/addCatching"}, method = RequestMethod.GET)
	public String addCatching(@ModelAttribute("catching")Catching catching,@RequestParam String bid,Model model)
	{
		Long batchId = new Long(bid);
		
		long numOfChicksDied = fieldReportService.getNumOfChicksDied(batchId);
		model.addAttribute("numOfChicksDied", numOfChicksDied);
		
		
		return "addCatching";
	}
	
	
	@RequestMapping(value= {"user/addCatching","admin/addCatching"} , method = RequestMethod.POST)
	public String updateCatching(@ModelAttribute("catching")Catching catching,HttpServletRequest request )
	{
		System.out.println("supply"+catching.getSupplyBatch().getSupplyBatchId());
		Long taskId = Long.parseLong(request.getParameter("task"));
		 
		
		
		Task task = new Task();
		task.setPerformedDate(catching.getDate());
		task.setTaskId(taskId);
		task.setStatus("Completed");
		task.setSupplyBatch(catching.getSupplyBatch());
////		task.setSupplyBatch(batchFeed.getSupplyBatch());
////		
//		
		taskService.updateTaskStatus(task);
 
		catchingService.save(catching);
		return "getSupplyBatches";
	}
	
	
	
}
