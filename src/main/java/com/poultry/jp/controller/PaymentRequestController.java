package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.model.PaymentRequest;
import com.poultry.jp.report.PaymentRequestReport;
import com.poultry.jp.service.MailService;
import com.poultry.jp.service.PaymentRequestService;

@Controller
public class PaymentRequestController {

	@Autowired
	private PaymentRequestService paymentRequestSerivce;
	
	private  MailService mailService = new MailService();
		
	@RequestMapping(value = {"user/addPaymentRequest","admin/addPaymentRequest"}, method = RequestMethod.GET)
	public String addPaymentRequestGET(Model model,@ModelAttribute("paymentRequest") PaymentRequest paymentRequest) {

		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivce.getPaymentRequestReport();

		model.addAttribute("paymentRequestReport", paymentRequestReport);
		return "addPaymentRequest";
	}

	@RequestMapping(value = {"user/addPaymentRequest","admin/addPaymentRequest"}, method = RequestMethod.POST)
	public String addPaymentRequestPOST(@ModelAttribute("paymentRequest") PaymentRequest paymentRequest) {

		paymentRequestSerivce.save(paymentRequest);
		

		return "redirect:addPaymentRequest.html";
	}
	@RequestMapping(value="admin/deletePaymentRequest",method = RequestMethod.GET)
	public String deletePaymentRequestGET(@RequestParam("pid")String pid)
	{
		Long id = new Long(pid);
		System.out.println(id);
		paymentRequestSerivce.deletePaymentRequest(id);
		return "redirect:addPaymentRequest.html";
	}
	
	@RequestMapping(value="admin/updatePaymentRequest",method = RequestMethod.GET)
	public String updatePaymentRequestGET(HttpServletRequest request,Model model)
	{
 
		String pid = request.getParameter("pid");
		Long id = new Long(pid);
		PaymentRequest paymentRequest = paymentRequestSerivce.getPaymentRequestObject(id);
		model.addAttribute("paymentRequest", paymentRequest);
		return "updatePaymentRequest";
	}
	
	@RequestMapping(value="admin/updatePaymentRequest",method = RequestMethod.POST)
	public String updatePaymentRequestPOST(@ModelAttribute("paymentRequest") PaymentRequest paymentRequest)
	{
 
		paymentRequestSerivce.updatePaymentRequest(paymentRequest);
		 
		return "redirect:addPaymentRequest.html";
	}

	@RequestMapping(value= {"user/getPaymentRequestReport","admin/getPaymentRequestReport"} , method = RequestMethod.GET)
	public String getPaymentRequestReport(Model model)
	{
		List<PaymentRequestReport> paymentRequestReport = paymentRequestSerivce.getPaymentRequestReport();
		
		model.addAttribute("paymentRequestReport", paymentRequestReport);
		
		return "getPaymentRequestReport";
	}
	
	@RequestMapping(value = {"user/getPaymentRequest","admin/getPaymentRequest"}, method = RequestMethod.GET)
	public @ResponseBody PaymentRequestReport findPaymentRequest(@RequestParam("pid") String pid) {

		Long id = new Long(pid);
		PaymentRequestReport paymentRequest = paymentRequestSerivce.getPaymentRequest(id);

		return paymentRequest;

	}

	@RequestMapping(value = {"user/getPaymentRequestObject","admin/getPaymentRequestObject"}, method = RequestMethod.GET)
	public @ResponseBody PaymentRequest getPaymentRequestObject(@RequestParam("pid") String pid) {
		System.out.println("came here with pid "+pid);

		Long id = new Long(pid);

		PaymentRequest paymentRequest = paymentRequestSerivce
				.getPaymentRequestObject(id);

		return paymentRequest;

	}
	
	@RequestMapping(value="admin/approvePaymentRequest",method = RequestMethod.GET)
	public String approvePaymentRequestGET(HttpServletRequest request,Model model)
	{
		String pid = request.getParameter("pid");
		Long id = new Long(pid);
		PaymentRequest paymentRequest = paymentRequestSerivce.getPaymentRequestObject(id);
		model.addAttribute("paymentRequest", paymentRequest);
		return "approvePaymentRequest";
	}
	
	@RequestMapping(value="admin/approvePaymentRequest",method = RequestMethod.POST)
	public String approvePaymentRequestPOST(@ModelAttribute("paymentRequest") PaymentRequest paymentRequest)
	{
		paymentRequestSerivce.approvePaymentRequest(paymentRequest);
		 
		return "redirect:getPaymentRequestReport.html";
	}
	
}
