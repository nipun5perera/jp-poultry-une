package com.poultry.jp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.report.FieldOfficerReport;
import com.poultry.jp.model.FieldReport;
import com.poultry.jp.report.FieldReportReport;
import com.poultry.jp.service.FieldOfficerService;
import com.poultry.jp.service.FieldReportService;

@Controller
public class FieldReportController {

	@Autowired
	private FieldReportService fieldReportService;
	
	@Autowired
	private FieldOfficerService fieldOfficerService;
	
	@RequestMapping(value = {"user/addFieldReport","admin/addFieldReport"}, method = RequestMethod.GET)
	public String addFieldReport(@ModelAttribute("fieldReport")FieldReport fieldReport,Model model,@RequestParam String bid){
		
		List<FieldOfficerReport> fieldOfficerReport = fieldOfficerService.getFieldOfficerReport();
		model.addAttribute("fieldOfficerReport", fieldOfficerReport);
		
		return "addFieldReport";
	}
	
	@RequestMapping(value = {"user/addFieldReport","admin/addFieldReport"}, method = RequestMethod.POST)
	public String updateFieldReport(@ModelAttribute("fieldReport")FieldReport fieldReport){
		
		fieldReportService.save(fieldReport);
		
		return "redirect:getSupplyBatches.html";
	}
	
	@RequestMapping(value = {"user/getFieldReport","admin/getFieldReport"}, method = RequestMethod.GET)
	public @ResponseBody List<FieldReportReport> getFieldReport(@RequestParam("bid") String bid) {

		Long id = new Long(bid);

		List<FieldReportReport> report =  fieldReportService.getFieldReport(id);

		return report;

	}

}
