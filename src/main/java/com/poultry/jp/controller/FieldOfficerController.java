package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poultry.jp.model.FieldOfficer;
import com.poultry.jp.report.FieldOfficerReport;
import com.poultry.jp.service.FieldOfficerService;

@Controller
public class FieldOfficerController {
	
	@Autowired
	private FieldOfficerService fieldOfficerService;
	
	@RequestMapping(value = "admin/addFieldOfficer", method = RequestMethod.GET)
	public String addFeildOfficerGET(Model model,@ModelAttribute("fieldOfficer")FieldOfficer fieldOfficer){
		List<FieldOfficerReport> fieldOfficerReports = fieldOfficerService.getAllFieldOfficers();
		model.addAttribute("fieldOfficerReport", fieldOfficerReports);
		return "addFieldOfficer";
	}
	
	@RequestMapping(value = "admin/addFieldOfficer", method = RequestMethod.POST)
	public String addFeildOfficerPOST(@ModelAttribute("fieldOfficer")FieldOfficer fieldOfficer){
		
		fieldOfficerService.save(fieldOfficer);
		
		return "redirect:addFieldOfficer.html";
	}
	

	@RequestMapping(value = "admin/updateFieldOfficer", method = RequestMethod.GET)
	public String updateFeildOfficerGET(Model model,@ModelAttribute("fieldOfficer")FieldOfficer fieldOfficer,HttpServletRequest request){
		
		Long id = new Long(request.getParameter("foid"));
		FieldOfficer fieldOfficerOb = fieldOfficerService.getFieldOfficerObject(id);
		model.addAttribute("fieldOfficer", fieldOfficerOb);
		return "updateFieldOfficer";
	}
	
	@RequestMapping(value = "admin/updateFieldOfficer", method = RequestMethod.POST)
	public String updateFeildOfficerPOST(@ModelAttribute("fieldOfficer")FieldOfficer fieldOfficer){
		
		fieldOfficerService.update(fieldOfficer);
		
		return "redirect:addFieldOfficer.html";
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = "user/getFieldOfficer", method = RequestMethod.GET)
	public @ResponseBody FieldOfficer getFieldOfficer(@RequestParam("foid") String foid) {
		System.out.println("came here with pid "+foid);

		Long id = new Long(foid);

		FieldOfficer fieldOfficer = fieldOfficerService.getFieldOfficerObject(id);

		return fieldOfficer;

	}
	
	@RequestMapping(value="user/getFieldOfficerReport",method = RequestMethod.GET)
	public String getFieldOfficerReports(Model model)
	{
		List<FieldOfficerReport> fieldOfficerReports = fieldOfficerService.getAllFieldOfficers();
		model.addAttribute("fieldOfficerReport", fieldOfficerReports);
		return "getFieldOfficerReport";
	}
	
	
	@RequestMapping(value = "admin/deleteFieldOfficer", method = RequestMethod.GET)
	public String deleteFieldOfficerGET(@RequestParam("foid")String foid)
	{
		Long id = new Long(foid);
		fieldOfficerService.delete(id);
		return "redirect:addFieldOfficer.html";
	}
	
	

}
