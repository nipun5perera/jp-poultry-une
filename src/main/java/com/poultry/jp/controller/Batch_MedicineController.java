package com.poultry.jp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.poultry.jp.model.Batch_Medicine;
import com.poultry.jp.report.Batch_MedicineReport;
import com.poultry.jp.report.MedicineReport;
import com.poultry.jp.service.Batch_MedicineService;
import com.poultry.jp.service.MedicineService;
import com.poultry.jp.service.TaskService;

@Controller
public class Batch_MedicineController {
	
	@Autowired
	private Batch_MedicineService batch_medicineSerive;
	
	@Autowired
	private MedicineService medicineService;
	
	@Autowired
	private TaskService taskService;
	
	
	@RequestMapping(value= {"user/addBatchMedicine","admin/addBatchMedicine"}, method = RequestMethod.GET)
	public String addBatchMedicine(@RequestParam("id")String id,@ModelAttribute("batchMedicine")Batch_Medicine batchMedicine,Model model){
		System.out.println("came here");
		
		Long bid = new Long(id);
		
		List<Batch_MedicineReport> batchMedicineList = batch_medicineSerive.getBatchMedicines(bid);
		List<MedicineReport> medinceReport = medicineService.getMedicineDetails();
		
		model.addAttribute("batchMedicineList",batchMedicineList);
		model.addAttribute("medicineReport",medinceReport);
		
//		for(int i=0;i < batchMedicineList.size();i++)
//		{
//			System.out.println(batchMedicineList.get(i).getQuantity());
//			
//		}
		
		
		return "addBatchMedicine";
	}
	
	@RequestMapping(value= {"user/addBatchMedicine","admin/addBatchMedicine"}, method = RequestMethod.POST)
	public String updateBatchMedicine(@ModelAttribute("batchMedicine")Batch_Medicine batchMedicine,HttpServletRequest request){
		System.out.println("came here post method");
		batch_medicineSerive.save(batchMedicine);
		 
		return "redirect:getSupplyBatches.html";
	}

}
