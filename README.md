# README #

This README would normally document whatever steps are necessary to get your application up and running.

### TOOLS & INFO ###
JP poultry has been developed to manage day to day business in a poultry farm, using java ee, Spring MVC, Hibernate and Spring security. To setup the project locally you need to have following installed in you computer.

* Spring tool suit
* mysql 5.5
* Apache tomcat 5.5

### What is this repository for? ###

* This repo is createtd to develop a day to day business management in a poultry farm with requirements collected from a client.

### How do I get set up? ###

* clone the project using `git clone git@bitbucket.org:nipun5perera/jp-poultry-une.git`
* import the project to Spring too suit
* create a database named `jppoultry`
* update `hibernate.hbm2ddl.auto` value in `jpaContext.xml` to `create`

\* For additional information please contact niroshan.kdt@gmail.com
